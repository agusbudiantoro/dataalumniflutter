import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:dataAlumni/network/feedInProfile.dart';
import 'package:dataAlumni/network/feed_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'feed_profile_event.dart';
part 'feed_profile_state.dart';

class FeedProfileBloc extends Bloc<FeedProfileEvent, FeedProfileState> {
  @override
  FeedProfileState get initialState => FeedProfileInitial();

  @override
  Stream<FeedProfileState> mapEventToState(
    FeedProfileEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    List<CloningPosting> data;
    int awal = 0;
    // if(state is FeedProfileInitial){
    //   yield* getFeedInProfile(data, tokens, idUser);
    if(event is PostProfileLiked){
      print("masukbloclike");
      print(state);
      yield* _postLiked(event.idPostingan, idUser, tokens, event.likeIndex);
    }
    if(event is UnProfileLiked){
      print("masukblocunlike");
      yield* _unLiked(event.idPostingan, idUser, tokens, event.likeIndex);
    } if(state is FeedProfileInitial){
      yield* getFeedInProfile(event.idUserProfil, data, tokens, idUser, awal);
    } else {
      yield* getFeedInProfileSeccond(event.idUserProfil, data, tokens, idUser, awal, state);
    }
    if(event is FeedProfileRefreshEvent){
      yield* getFeedInProfile(event.idUserRefresh,data, tokens, idUser, awal);
    }
    if(event is EditFeedGalery){
      print("sini masuk edit");
      yield* editFeedInProfile(event.gambar64, event.dataFeed, tokens, idUser);
    } if(event is EditFeedYtb){
      print("sini masuk edit ytb");
      yield* editFeedInProfileYtb(event.dataFeed, tokens, idUser);
    }
    if(event is SetStatus){
      yield* pushSetStatus(event.idPost, event.indexStatus, event.valueStatus, tokens);
    }
    if(event is DeletePosting){
      print("siniiiiiii");
      yield * deletePostingan(state,event.idPost, event.indexNow, tokens);
    }
  }
}

Stream<FeedProfileState> deletePostingan(data,id, ind, tokens) async* {
  yield DeleteWaiting();
  print("sini masuk edit 2");
  try {
    print("sini masuk edit 3");
    bool isi = await FeedInProfil.deletePosting(id, tokens);
    print('deletePosting');
    if(isi == true){
      print("sini delete");
      yield DeleteSukses();
    } else {
      yield DeleteError(errorMessage: "Error editing");
    }
  } catch (ex) {
    print('error2');
    yield DeleteError(errorMessage: ex.message.toString());
  }
}

Stream<FeedProfileState> pushSetStatus(idPost, idx,valueStat, tokens) async* {
  yield StatusWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");

    bool isi = await FeedInProfil.setStatusEdit(idPost, tokens);
    print('masuk');
    print(valueStat);
    print(isi);
    if(isi == true){
      // postLoaded.listPost[idx].status = (valueStat == 0) ? 1 :0;
      yield StatusSukses(indexStatus: idx);
    } else {
      yield StatusError(errorMessage: "Error editing");
    }
  } catch (ex) {
    print('error2');
    yield StatusError(errorMessage: ex.message.toString());
  }
}

Stream<FeedProfileState> editFeedInProfileYtb(data, tokens, idUser) async* {
  yield EditWaiting();
  print("sini masuk edit 2");
  try {
    print("sini masuk edit 3");
    bool isi = await FeedInProfil.editFeedYtb(data, tokens, idUser);
    print('masuk');
    print(isi);
    if(isi == true){
      yield EditSukses();
    } else {
      yield EditError(errorMessage: "Error editing");
    }
  } catch (ex) {
    print('error2');
    yield EditError(errorMessage: ex.message.toString());
  }
}

Stream<FeedProfileState> editFeedInProfile(gbr, data, tokens, idUser) async* {
  yield EditWaiting();
  print("sini masuk edit 2");
  try {
    print("sini masuk edit 3");
    bool isi = await FeedInProfil.editFeed(gbr, data, tokens, idUser);
    print('masuk');
    print(isi);
    if(isi == true){
      yield EditSukses();
    } else {
      yield EditError(errorMessage: "Error editing");
    }
  } catch (ex) {
    print('error2');
    yield EditError(errorMessage: ex.message.toString());
  }
}

Stream<FeedProfileState> getFeedInProfile(id, data,tokens, idUser, awal) async* {
  yield FeedProfilWaiting();
  try {
    if(id == null){
      data = await FeedInProfil.getFeedByIdUser(tokens, idUser, awal, 6);
    } else {
      data = await FeedInProfil.getFeedByIdUser(tokens, id, awal, 6);
    }
    print('masuk feed Profil');
    yield FeedSuccessLoad(listPost: data, hasReachMax: false, statusLike: false, statusUnLike: false);
  } catch (ex) {
    yield FeedProfilError(errorMessage: ex.message.toString());
  }
}

Stream<FeedProfileState> getFeedInProfileSeccond(id, data, tokens, idUser, awal, state) async* {
  try {
    FeedSuccessLoad postLoaded = state as FeedSuccessLoad;
  int acuan;
  print("inidata");
  print(postLoaded.counter);
  if(postLoaded.counter == null){
    acuan = awal+1;
  } else {
    acuan = postLoaded.counter+1;
  }
  print("acuan");
  print(acuan);
  if(id == null){
    data = await FeedInProfil.getFeedByIdUser(tokens,idUser,acuan, 6);
  } else {
    data = await FeedInProfil.getFeedByIdUser(tokens,id,acuan, 6);
  }
  print('data kosong?');
  print(data.isEmpty);
  print(data);
  yield (data.isEmpty) ? postLoaded.copyWith(hasReachMax: true) : FeedSuccessLoad(counter:acuan,listPost: postLoaded.listPost + data, hasReachMax: false, statusLike: false, statusUnLike: false);
  } catch (e) {
    print("error");
    print(e);
    yield FeedProfilError(errorMessage: e.message.toString(), statusState: false);
  }
  
}

Stream<FeedProfileState> _postLiked(idPosting, idUser, token, indexLike) async* {
  print("masukblok");
  try {
    // FeedSuccessLoad postLoaded = state as FeedSuccessLoad;
    bool data = await Feed.postLikeApi(idPosting,idUser,token);
    print('masuk');
    print(indexLike);
    print(data);
    if(data == true){
      print("masuktrue");
      yield FeedSuccessLoad(statusLike: true, indLike: indexLike, statusUnLike: false);
    } else {
      yield FeedProfilError(errorMessage: "gagal like", statusState: true);
    }
  } catch (ex) {
    yield FeedProfilError(errorMessage: ex.message.toString(), statusState: true);
  }
}

Stream<FeedProfileState> _unLiked(idPosting, idUser, token, indexLike) async* {
  print("masukblok");
  try {
    // FeedSuccessLoad postLoaded = state as FeedSuccessLoad;
    bool data = await Feed.postUnLikeApi(idPosting,idUser,token);
    print('masuk');
    print(data);
    if(data == true){
      print("masuktrue");
      yield FeedSuccessLoad( statusLike: false, indLike: indexLike, statusUnLike: true);
    } else {
      yield FeedProfilError(errorMessage: "gagal like", statusState: true);
    }
  } catch (ex) {
    yield FeedProfilError(errorMessage: ex.message.toString(), statusState: true);
  }
}
