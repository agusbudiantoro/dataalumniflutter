part of 'feed_profile_bloc.dart';

@immutable
abstract class FeedProfileState {
  
}

class FeedProfileInitial extends FeedProfileState {}

class FeedSuccessLoad extends FeedProfileState{
  List<CloningPosting> listPost;
  bool statusLike;
  bool statusUnLike;
  int indLike;
  bool hasReachMax;
  int counter;
  int indexDel;
  FeedSuccessLoad({this.listPost, this.indLike, this.statusLike, this.statusUnLike, this.hasReachMax, this.counter, this.indexDel});

  FeedSuccessLoad copyWith({List<CloningPosting> listPost, bool hasReachMax, int counter, int statusLike, int indLike, int statusUnLike, int indexDel}){
    return FeedSuccessLoad(
      listPost: listPost ?? this.listPost,
      hasReachMax: hasReachMax ?? this.hasReachMax,
      counter: counter ?? this.counter,
      statusLike: statusLike ?? this.statusLike,
      indLike: indLike ?? this.indLike,
      statusUnLike: statusUnLike?? this.statusUnLike,
      indexDel: indexDel?? indexDel
    );
  }
}
class FeedProfilWaiting extends FeedProfileState{}

class FeedProfilError extends FeedProfileState{
  bool statusState;
  final String errorMessage;
  FeedProfilError({this.errorMessage, this.statusState});
}

class EditWaiting extends FeedProfileState {}
class EditSukses extends FeedProfileState{}
class EditError extends FeedProfileState {
  final String errorMessage;
  EditError({
    this.errorMessage
  });
}

class StatusWaiting extends FeedProfileState{}
class StatusError extends FeedProfileState{
  final String errorMessage;
  StatusError({this.errorMessage});
}
class StatusSukses extends FeedProfileState {
  int indexStatus;
  StatusSukses({@required this.indexStatus});
}

class DeleteSukses extends FeedProfileState {
  
}
class DeleteError extends FeedProfileState{
  final String errorMessage;
  DeleteError({this.errorMessage});
}
 class DeleteWaiting extends FeedProfileState{
   
 }