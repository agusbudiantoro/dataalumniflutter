part of 'feed_profile_bloc.dart';

@immutable
class FeedProfileEvent {
  int idUserProfil;
  FeedProfileEvent({this.idUserProfil});
}

class GetFeed extends FeedProfileEvent{
}

class PostProfileLiked extends FeedProfileEvent{
  int idPostingan;
  int idUser;
  int likeIndex;

  PostProfileLiked({@required this.idPostingan, this.idUser, this.likeIndex});
}

class FeedProfileRefreshEvent extends FeedProfileEvent{
  int idUserRefresh;
  FeedProfileRefreshEvent({this.idUserRefresh});
}

class UnProfileLiked extends FeedProfileEvent{
  int idPostingan;
  int idUser;
  int likeIndex;

  UnProfileLiked({@required this.idPostingan, this.idUser, this.likeIndex});
}

class EditFeedGalery extends FeedProfileEvent{
  File gambar64;
  final CloningPosting dataFeed;

  EditFeedGalery({this.gambar64, @required this.dataFeed});

}

class EditFeedYtb extends FeedProfileEvent{
  final CloningPosting dataFeed;

  EditFeedYtb({@required this.dataFeed});

}

class SetStatus extends FeedProfileEvent{
  int idPost;
  int indexStatus;
  int valueStatus;
  SetStatus({@required this.idPost, @required this.indexStatus, @required this.valueStatus});
}

class DeletePosting extends FeedProfileEvent {
  int indexNow;
  int idPost;
  final CloningPosting dataFeed;

  DeletePosting({@required this.idPost,this.indexNow, this.dataFeed});
}