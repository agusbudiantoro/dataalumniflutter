import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'package:dataAlumni/network/menu_profile/addInformasiUser.dart';
import 'package:dataAlumni/network/menu_profile/editInformasiUser.dart';
import 'package:dataAlumni/network/menu_profile/getInformasiUser.dart';
import 'package:dataAlumni/network/menu_profile/hapusInformasiUser.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'informasi_user_event.dart';
part 'informasi_user_state.dart';

class InformasiUserBloc extends Bloc<InformasiUserEvent, InformasiUserState> {
  @override
  InformasiUserState get initialState => InformasiUserInitial();

  @override
  Stream<InformasiUserState> mapEventToState(
    InformasiUserEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    if(event is InformasiUserEvent){
      print("sini");
      print(event.urutan);
      yield* _getInformasi(event.idUser,tokens, event.urutan, idUser);
    } if(event is AddDataInformasiEvent){
      print("tambah");
      print(event.urutanInd);
      yield* _addInformasi(tokens, event.urutanInd, idUser, event.dataPendidikanOld, event.dataPendidikan, event.dataKarirOld, event.dataKarir, event.dataOrganisasiOld, event.dataOrganisasi);
    } if(event is EditDataInformasiEvent){
      print("edit");
      yield* _editInformasi(event.indexData,tokens, event.urutanInd, idUser, event.dataPendidikanOld, event.dataPendidikan, event.dataKarirOld, event.dataKarir, event.dataOrganisasiOld, event.dataOrganisasi);
    }
    if(event is HapusDataInformasiEvent){
      print("edit");
      print(event.indexData);
      yield* _hapusInformasi(event.indexData,tokens, event.urutanInd, idUser, event.dataPendidikanOld, event.dataPendidikan, event.dataKarirOld, event.dataKarir, event.dataOrganisasiOld, event.dataOrganisasi);
    }
  }
}

Stream<InformasiUserState> _hapusInformasi(ind,token, urutan, idUser, data, newData, data1, newData1, data2, newData2) async* {
  yield GetInformasiWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    print(ind);
    if(urutan == 0){
      List<ModelPendidikanUser> dataOld = data;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      print(newData.toString());
      bool isi = await HapusInformasionApi.hapusPendidikan(token, idUser, newData);
      print("coba");
      print(dataOld[ind]);
      if(isi == true){
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Pendidikan", dataPendidikan: dataOld..removeAt(ind));
      } else {
        yield GetInformasiError(errorMessage: "error");
      }
    } else if(urutan == 1){
      List<ModelKarir> dataOld = data1;
      print(urutan);
      print("sini data karir");
      print(dataOld.length);
      print(ind);
      print(newData1.toString());
      bool isi = await HapusInformasionApi.hapusKarir(token, idUser, newData1);
      print("coba");
      print(dataOld[ind]);
      if(isi == true){
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Karir", dataKarir: dataOld..removeAt(ind));
      } else {
        yield GetInformasiError(errorMessage: "error");
      }
    } else if(urutan == 2){
      List<ModelOrganisasi> dataOld = data2;
      print(urutan);
      print("sini data karir");
      print(dataOld.length);
      print(ind);
      print(newData2.toString());
      bool isi = await HapusInformasionApi.hapusOrganisasi(token, idUser, newData2);
      print("coba");
      print(dataOld[ind]);
      if(isi == true){
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Organisasi", dataOrganisasi: dataOld..removeAt(ind));
      } else {
        yield GetInformasiError(errorMessage: "error");
      }
    }
  } catch (ex) {
    print('error2');
    yield GetInformasiError(errorMessage: ex.message.toString());
  }
}

Stream<InformasiUserState> _editInformasi(ind,token, urutan, idUser, data, newData, data1, newData1, data2, newData2) async* {
  yield GetInformasiWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    if(urutan == 0){
      List<ModelPendidikanUser> dataOld = data;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      print(newData.toString());
      final isi = await EditInformasionApi.editPendidikan(token, idUser, newData);
      print("coba");
      dataOld[ind] = isi;
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Pendidikan", dataPendidikan: dataOld);
    } else if(urutan == 1){
      List<ModelKarir> dataOld = data1;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      final isi = await EditInformasionApi.editKarir(token, idUser, newData1);
      print("coba");
      dataOld[ind] = isi;
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Karir", dataKarir: dataOld);
    } else if(urutan == 2){
      List<ModelOrganisasi> dataOld = data2;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      final isi = await EditInformasionApi.editOrganisasi(token, idUser, newData2);
      print("coba");
      dataOld[ind] = isi;
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Organisasi", dataOrganisasi: dataOld);
    }
  } catch (ex) {
    print('error2');
    yield GetInformasiError(errorMessage: ex.message.toString());
  }
}

Stream<InformasiUserState> _addInformasi(token, urutan, idUser, data, newData, data1, newData1, data2, newData2) async* {
  yield GetInformasiWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    print(urutan);
    if(urutan == 0){
      List<ModelPendidikanUser> dataOld = data;
      var dataNonReversed =  dataOld.reversed.toList();
      print("sini data");
      final isi = await AddInformasionApi.addPendidikan(token,idUser,newData);
      var finalIsi = dataNonReversed..add(isi);
      var finalIsiReversed = finalIsi.reversed.toList();
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Pendidikan", dataPendidikan: finalIsiReversed);
    } else if(urutan == 1){
      print("masuk karir");
      print(data1);
      List<ModelKarir> dataOld = data1;
      var dataNonReversed =  dataOld.reversed.toList();
      print("sini data");
      print(data1.length);
      final isi1 = await AddInformasionApi.addKarir(token,idUser,newData1);
      var finalIsi = dataNonReversed..add(isi1);
      var finalIsiReversed = finalIsi.reversed.toList();
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Karir", dataKarir: finalIsiReversed);
    } else if(urutan == 2){
      print("masuk organisasi");
      print(data1);
      List<ModelOrganisasi> dataOld = data2;
      var dataNonReversed =  dataOld.reversed.toList();
      print("sini data");
      print(data2.length);
      final isi1 = await AddInformasionApi.addOrganisasi(token,idUser,newData2);
      var finalIsi = dataNonReversed..add(isi1);
      var finalIsiReversed = finalIsi.reversed.toList();
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Organisasi", dataOrganisasi: finalIsiReversed);
    }
  } catch (ex) {
    print('error2');
    yield GetInformasiError(errorMessage: ex.message.toString());
  }
}

Stream<InformasiUserState> _getInformasi(idInEvent,token, urutan, idUser) async* {
  yield GetInformasiWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    print(idInEvent);
    if(idInEvent == null){
      if(urutan == 0){
      List<ModelPendidikanUser> isi = await GetInformasionApi.getPendidikan(token,idUser);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Pendidikan", dataPendidikan: isi.reversed.toList());
    } else if(urutan == 1){
      List<ModelKarir> isi2 = await GetInformasionApi.getKarir(token,idUser);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Karir", dataKarir: isi2.reversed.toList());
    } else if(urutan == 2){
      List<ModelOrganisasi> isi3 = await GetInformasionApi.getOrganisasi(token,idUser);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Organisasi", dataOrganisasi: isi3.reversed.toList());
    }
    } else{
      if(urutan == 0){
      List<ModelPendidikanUser> isi = await GetInformasionApi.getPendidikan(token,idInEvent);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Pendidikan", dataPendidikan: isi.reversed.toList());
    } else if(urutan == 1){
      List<ModelKarir> isi2 = await GetInformasionApi.getKarir(token,idInEvent);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Karir", dataKarir: isi2.reversed.toList());
    } else if(urutan == 2){
      List<ModelOrganisasi> isi3 = await GetInformasionApi.getOrganisasi(token,idInEvent);
      yield GetInformasiSukses(indexInformasi: urutan, inisialIndex: "Organisasi", dataOrganisasi: isi3.reversed.toList());
    }
    }
    
  } catch (ex) {
    print('error2');
    yield GetInformasiError(errorMessage: ex.message.toString());
  }
}