part of 'informasi_user_bloc.dart';

@immutable
abstract class InformasiUserState {
  
}

class InformasiUserInitial extends InformasiUserState {}
class GetInformasiSukses extends InformasiUserState{
List <ModelPendidikanUser> dataPendidikan;
List <ModelKarir> dataKarir;
List <ModelOrganisasi> dataOrganisasi;
int indexInformasi;
String inisialIndex;
String statusAksi;
GetInformasiSukses({this.dataPendidikan, this.indexInformasi, this.inisialIndex, this.dataKarir, this.dataOrganisasi, this.statusAksi});
}
class GetInformasiWaiting extends InformasiUserState{}

class GetInformasiError extends InformasiUserState{
  final String errorMessage;
  GetInformasiError({this.errorMessage});
}