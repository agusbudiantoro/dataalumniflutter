part of 'informasi_user_bloc.dart';

@immutable
class InformasiUserEvent {
  int urutan;
  int idUser;
  InformasiUserEvent({this.urutan, this.idUser});
}

class GetPendidikan extends InformasiUserEvent {
  int id;
  GetPendidikan({@required this.id});
}

class AddDataInformasiEvent extends InformasiUserEvent {
  final ModelPendidikanUser dataPendidikan;
  List <ModelPendidikanUser> dataPendidikanOld;
  final ModelKarir dataKarir;
  List <ModelKarir> dataKarirOld;
  final ModelOrganisasi dataOrganisasi;
  List <ModelOrganisasi> dataOrganisasiOld;
  int urutanInd;
  String statusAksi;
  AddDataInformasiEvent(
      {this.dataOrganisasiOld,this.dataKarirOld,this.statusAksi, this.dataPendidikan, this.dataKarir, this.dataOrganisasi, this.dataPendidikanOld, @required this.urutanInd});
}

class EditDataInformasiEvent extends InformasiUserEvent {
  final ModelPendidikanUser dataPendidikan;
  List <ModelPendidikanUser> dataPendidikanOld;
  final ModelKarir dataKarir;
  List <ModelKarir> dataKarirOld;
  final ModelOrganisasi dataOrganisasi;
  List <ModelOrganisasi> dataOrganisasiOld;
  int urutanInd;
  String statusAksi;
  int indexData;
  EditDataInformasiEvent(
      {this.dataOrganisasiOld,this.dataKarirOld,this.indexData,this.statusAksi,this.dataPendidikan, this.dataKarir, this.dataOrganisasi, this.dataPendidikanOld, @required this.urutanInd});
}

class HapusDataInformasiEvent extends InformasiUserEvent {
  final ModelPendidikanUser dataPendidikan;
  List <ModelPendidikanUser> dataPendidikanOld;
  final ModelKarir dataKarir;
  List <ModelKarir> dataKarirOld;
  final ModelOrganisasi dataOrganisasi;
  List <ModelOrganisasi> dataOrganisasiOld;
  int urutanInd;
  String statusAksi;
  int indexData;
  HapusDataInformasiEvent(
      {this.dataOrganisasiOld,this.dataKarirOld,this.indexData,this.statusAksi,this.dataPendidikan, this.dataKarir, this.dataOrganisasi, this.dataPendidikanOld, @required this.urutanInd});
}