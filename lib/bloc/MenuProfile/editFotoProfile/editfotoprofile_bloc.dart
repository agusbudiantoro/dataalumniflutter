import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/network/menu_profile/editFoto.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'editfotoprofile_event.dart';
part 'editfotoprofile_state.dart';

class EditfotoprofileBloc
    extends Bloc<EditfotoprofileEvent, EditfotoprofileState> {
  @override
  EditfotoprofileState get initialState => EditfotoprofileInitial();

  @override
  Stream<EditfotoprofileState> mapEventToState(
    EditfotoprofileEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    int idAlumni = preferences.getInt("idAlumni");
    if (event is EditFotoEvent) {
      yield* _editFoto(tokens, idAlumni, event.gambar64);
    }
  }
}

Stream<EditfotoprofileState> _editFoto(token, id, gbr) async* {
  yield EditFotoWaiting();
  print("sini masuk edit 2");
  try {
    print("sini masuk edit 3");
    DataAlumni isi = await EditFotoApi.editFotoApi(token, id, gbr);
    print('deletePosting');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String gambar64 = await GetGambarBase64Alumni.getGambarBase64NonKompresAlumni(
        token, isi.foto);
        await preferences.setString("fotoProfil", gambar64);
    yield EditFotoProfilSukses();
  } catch (ex) {
    print('error2');
    yield EditFotoError(errorMessage: ex.message.toString());
  }
}
