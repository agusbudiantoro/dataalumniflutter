part of 'editfotoprofile_bloc.dart';

@immutable
abstract class EditfotoprofileState{
}

class EditfotoprofileInitial extends EditfotoprofileState {}
class EditFotoProfilSukses extends EditfotoprofileState{
  DataAlumni dataAlumni;
  EditFotoProfilSukses({this.dataAlumni});
}

class EditFotoWaiting extends EditfotoprofileState{}
class EditFotoError extends EditfotoprofileState{
  String errorMessage;

  EditFotoError({this.errorMessage});
}