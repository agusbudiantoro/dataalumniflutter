import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/menu_alumni/alumniApi.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'tambahalumni_event.dart';
part 'tambahalumni_state.dart';

class TambahalumniBloc extends Bloc<TambahalumniEvent, TambahalumniState> {
  @override
  TambahalumniState get initialState => TambahalumniInitial();

  @override
  Stream<TambahalumniState> mapEventToState(
    TambahalumniEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int idUser = preferences.getInt("iduser");
    var tokens = preferences.getString("token");
    if(event is TambahAlumniProfil){
      yield* _tambahData(event.data, idUser, tokens);
    } if(event is EditAlumniProfil){
      yield* _editData(event.dataEdit, idUser, tokens);
    }
  }
}

Stream<TambahalumniState> _tambahData(data, id, token) async* {
  yield TambahAlumniWaiting();
  try {
    bool isi = await AlumniApi.postDataAlumni(data, id, token);
    print('masuk feed Profil');
    print(isi);
    if(isi == true){
      yield TambahAlumniSukses();
    } else{
      yield TambahAlumniError(errorMessage: "gagal");
    }
  } catch (ex) {
    yield TambahAlumniError(errorMessage: ex.message.toString());
  }
}

Stream<TambahalumniState> _editData(data, id, token) async* {
  yield EditAlumniWaiting();
  try {
    bool isi = await AlumniApi.putDataAlumni(data, id, token);
    print('masuk feed Profil');
    print(isi);
    if(isi == true){
      yield EditAlumniSukses();
    } else{
      yield EditAlumniError(errorMessage: "gagal");
    }
  } catch (ex) {
    yield EditAlumniError(errorMessage: ex.message.toString());
  }
}