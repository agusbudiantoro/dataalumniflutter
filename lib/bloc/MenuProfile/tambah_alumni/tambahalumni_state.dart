part of 'tambahalumni_bloc.dart';

@immutable
abstract class TambahalumniState{
}

class TambahalumniInitial extends TambahalumniState {}

class TambahAlumniSukses extends TambahalumniState{}

class TambahAlumniWaiting extends TambahalumniState{}

class TambahAlumniError extends TambahalumniState{
  String errorMessage;
  TambahAlumniError({this.errorMessage});
}

class EditAlumniSukses extends TambahalumniState{}

class EditAlumniWaiting extends TambahalumniState{}

class EditAlumniError extends TambahalumniState{
  String errorMessage;
  EditAlumniError({this.errorMessage});
}