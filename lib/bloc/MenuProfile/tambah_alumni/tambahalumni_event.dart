part of 'tambahalumni_bloc.dart';

@immutable
class TambahalumniEvent{
  
}

class TambahAlumniProfil extends TambahalumniEvent{
  DataAlumni data;

  TambahAlumniProfil({@required this.data});
}

class EditAlumniProfil extends TambahalumniEvent{
  DataAlumni dataEdit;

  EditAlumniProfil({@required this.dataEdit});
}