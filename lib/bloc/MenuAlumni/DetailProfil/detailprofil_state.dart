part of 'detailprofil_bloc.dart';

@immutable
abstract class DetailprofilState{
}

class DetailprofilInitial extends DetailprofilState {}
class GetDetailSukses extends DetailprofilState {
  List <DataAlumni> listAlumni;

  GetDetailSukses({this.listAlumni});
}

class GetDataWaiting extends DetailprofilState {
  
}

class GetDataError extends DetailprofilState{
final String errorMessage;
GetDataError({this.errorMessage});  
}