import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/menu_alumni/alumniApi.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'detailprofil_event.dart';
part 'detailprofil_state.dart';

class DetailprofilBloc extends Bloc<DetailprofilEvent, DetailprofilState> {
  @override
  DetailprofilState get initialState => DetailprofilInitial();

  @override
  Stream<DetailprofilState> mapEventToState(
    DetailprofilEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int idUser = preferences.getInt("iduser");
    var tokens = preferences.getString("token");

    if(event is GetDetailProfilEvent){
      print("sini data profil");
      yield* _getDataAlumniById(idUser, tokens);
    }
  }
}

Stream<DetailprofilState> _getDataAlumniById(id, token) async* {
  yield GetDataWaiting();
  try {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List <DataAlumni> data = await AlumniApi.getDataAlumniById(id, token);
    print('masuk feed Profil');
    print(data.length);
    if(data.length != 0){
      await preferences.setString("fotoProfil", data[0].stringGambar);
      await preferences.setInt("idAlumni", data[0].id);
    } else {
      await preferences.setString("fotoProfil", IconOrang().gbrPeople);
    }
    yield GetDetailSukses(listAlumni: data);
  } catch (ex) {
    yield GetDataError(errorMessage: ex.message.toString());
  }
}