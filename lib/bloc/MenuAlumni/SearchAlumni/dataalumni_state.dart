part of 'dataalumni_bloc.dart';

@immutable
abstract class DataalumniState{
}

class DataalumniInitial extends DataalumniState {}

class GetDataSukses extends DataalumniState {
  List <DataAlumni> listAlumni;
  bool hasReachMax;
  int counter;

  GetDataSukses({this.listAlumni, this.hasReachMax, this.counter});

  GetDataSukses copyWith({List<DataAlumni> listAlumni, bool hasReachMax, int counter}){
    return GetDataSukses(
      listAlumni: listAlumni ?? this.listAlumni,
      hasReachMax: hasReachMax ?? this.hasReachMax,
      counter: counter ?? this.counter,
    );
  }
}
class GetDataLoading extends DataalumniState{}

class GetDataError extends DataalumniState {
  final String errorMessage;

  GetDataError({this.errorMessage});
}