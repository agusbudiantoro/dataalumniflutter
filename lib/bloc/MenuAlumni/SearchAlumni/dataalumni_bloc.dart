import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/menu_alumni/alumniApi.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'dataalumni_event.dart';
part 'dataalumni_state.dart';

class DataalumniBloc extends Bloc<DataalumniEvent, DataalumniState> {
  @override
  DataalumniState get initialState => DataalumniInitial();

  @override
  Stream<DataalumniState> mapEventToState(
    DataalumniEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    List <DataAlumni> data; 
    int awal =0;
    int awalByName =1;
    if(state is DataalumniInitial){
      yield* _getDataAllAlumni(tokens, data, awal);
    } else {
      yield* _getDataAllAlumniSeccond(state, tokens, data, awal);
    }
    if(event is AlumniRefreshEvent){
      yield* _getDataAllAlumni(tokens, data, awal);
    }
    if(event is SearchByNameEvent){
      yield* _getDataAlumniByName(event.nama, event.listAlumniByName, tokens,awalByName);
    }
  }
}

Stream<DataalumniState> _getDataAlumniByName(nama, dataList, tokens,awalByName) async* {
  yield GetDataLoading();
  try {
    List <DataAlumni> data = await AlumniApi.getDataAlumniByName(tokens, awalByName, 4, nama);
    print('masuk feed Profil');
    print(nama);
    print(data.length);
    yield GetDataSukses(listAlumni: data, hasReachMax: false);
  } catch (ex) {
    yield GetDataError(errorMessage: ex.message.toString());
  }
}

Stream<DataalumniState> _getDataAllAlumni(tokens, data, awal) async* {
  yield GetDataLoading();
  try {
    data = await AlumniApi.getDataAlumni(tokens, awal, 4);
    print('masuk feed Profil');
    yield GetDataSukses(listAlumni: data, hasReachMax: false);
  } catch (ex) {
    yield GetDataError(errorMessage: ex.message.toString());
  }
}

Stream<DataalumniState> _getDataAllAlumniSeccond(state, tokens, data, awal) async* {
  try {
    GetDataSukses postLoaded = state as GetDataSukses;
  int acuan;
  print("inidata");
  print(postLoaded.counter);
  if(postLoaded.counter == null){
    acuan = awal+1;
  } else {
    acuan = postLoaded.counter+1;
  }
  print("acuan");
  print(acuan);
  data = await AlumniApi.getDataAlumni(tokens, acuan, 4);
  print('data kosong?');
  print(data.isEmpty);
  print(data);
  yield (data.isEmpty) ? postLoaded.copyWith(hasReachMax: true) : GetDataSukses(counter:acuan,listAlumni: postLoaded.listAlumni + data, hasReachMax: false);
  } catch (e) {
    print("error");
    print(e);
    yield GetDataError(errorMessage: e.message.toString());
  }
  
}