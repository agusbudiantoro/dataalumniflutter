part of 'dataalumni_bloc.dart';

@immutable
class DataalumniEvent{
}
class AlumniRefreshEvent extends DataalumniEvent {
  
}
class SearchByNameEvent extends DataalumniEvent {
  String nama;
  List <DataAlumni> listAlumniByName;

  SearchByNameEvent({this.nama, this.listAlumniByName});
}