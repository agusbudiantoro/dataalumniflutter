// part of 'bloc_post_bloc.dart';

// @immutable
// abstract class PostState {}

// class PostInitial extends PostState {}

// class PostLoaded extends PostState {
//   List<FeedPosting> listPost;
//   bool hasReachMax;
//   int counter;
//   PostLoaded({
//     this.listPost, this.hasReachMax, this.counter
//   });

//   PostLoaded copyWith({List<FeedPosting> listPost, bool hasReachMax, int counter}){
//     return PostLoaded(
//       listPost: listPost ?? this.listPost,
//       hasReachMax: hasReachMax ?? this.hasReachMax,
//       counter: counter ?? this.counter
//     );
//   }
// }

// class PostError extends PostState {
//   final String errorMessage;
//   PostError({
//     this.errorMessage,
//   });
// }

// class PostWaiting extends PostState {}