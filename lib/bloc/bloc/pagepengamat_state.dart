part of 'pagepengamat_bloc.dart';

@immutable
abstract class PagepengamatState{
}

class PagepengamatInitial extends PagepengamatState {}

class PengamatSukses extends PagepengamatState {
  
}
class PengamatError extends PagepengamatState {
  String message;
  PengamatError({this.message});
}