import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'pagepengamat_event.dart';
part 'pagepengamat_state.dart';

class PagepengamatBloc extends Bloc<PagepengamatEvent, PagepengamatState> {
  @override
  PagepengamatState get initialState => PagepengamatInitial();

  @override
  Stream<PagepengamatState> mapEventToState(
    PagepengamatEvent event,
  ) async* {
    if(event is PostPage){
      yield* _postPage();
    }
  }
}

Stream<PagepengamatState> _postPage() async* {
  try {
    print('sukses menuju page');
    yield PengamatSukses();
  } catch (ex) {
    yield PengamatError(message: ex.message.toString());
  }
}