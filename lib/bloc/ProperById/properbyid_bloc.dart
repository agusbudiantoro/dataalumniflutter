import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/modeProper.dart';
import 'package:dataAlumni/network/menu_profile/dataProper.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'properbyid_event.dart';
part 'properbyid_state.dart';

class ProperbyidBloc extends Bloc<ProperbyidEvent, ProperbyidState> {
  
  @override
  ProperbyidState get initialState => ProperbyidInitial();

  @override
  Stream<ProperbyidState> mapEventToState(
    ProperbyidEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    if (event is GetProperById){
      yield* getDataProper(event.id);
    } if(event is AddProper){
      print("tambah");
      yield* _addInformasi(tokens, idUser, event.dataProperOld, event.dataProper);
    }if(event is EditProper){
      print("edit");
      yield* _editInformasi(event.indexData,tokens, event.urutanInd, idUser, event.dataProperOld, event.dataProper);
    }if(event is HapusProper){
      print("delete");
      yield* _hapusInformasi(event.indexData,tokens, event.urutanInd, idUser, event.dataProperOld, event.dataProper);
    }
  }
}

Stream<ProperbyidState> getDataProper(id) async* {
  yield GetProperWaiting();
  try {
    List <ModelProper> data = await GetDataProperApi.getProper(id);
    print('masuk data proper');
    print(data.length);
    yield GetProperSukses(listProper: data);
  } catch (ex) {
    yield GetProperError(message: ex.message.toString());
  }
}

Stream<ProperbyidState> _addInformasi(token, idUser, data, newData) async* {
  yield GetProperWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    print(newData);
      List<ModelProper> dataOld = data;
      print("sini data");
      var dataNonReversed =  dataOld.reversed.toList();
      print("sini data");
      final isi = await GetDataProperApi.addProper(token,idUser,newData);
      var finalIsi = dataNonReversed..add(isi);
      var finalIsiReversed = finalIsi.reversed.toList();
      yield GetProperSukses(listProper: finalIsiReversed);
  } catch (ex) {
    print('error2');
    yield GetProperError(message: ex.message.toString());
  }
}

Stream<ProperbyidState> _editInformasi(ind,token, urutan, idUser, data, newData) async* {
  yield GetProperWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    List<ModelProper> dataOld = data;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      print(newData.toString());
      final isi = await GetDataProperApi.editProper(token, idUser, newData);
      print("coba");
      dataOld[ind] = isi;
      yield GetProperSukses(indexInformasi: urutan, listProper: dataOld);
    
  } catch (ex) {
    print('error2');
    yield GetProperError(message: ex.message.toString());
  }
}

Stream<ProperbyidState> _hapusInformasi(ind,token, urutan, idUser, data, newData) async* {
  yield GetProperWaiting();
  print("sini masuk edit stattus 2");
  try {
    print("sini masuk edit status 3");
    print('masuk');
    print(ind);
    List<ModelProper> dataOld = data;
      print(urutan);
      print("sini data");
      print(dataOld.length);
      print(ind);
      print(newData.toString());
      bool isi = await GetDataProperApi.hapusProper(token, idUser, newData);
      print("coba");
      print(dataOld[ind]);
      if(isi == true){
      yield GetProperSukses(indexInformasi: urutan,listProper: dataOld..removeAt(ind));
      } else {
        yield GetProperError(message: "error");
      }
    
  } catch (ex) {
    print('error2');
    yield GetProperError(message: ex.message.toString());
  }
}