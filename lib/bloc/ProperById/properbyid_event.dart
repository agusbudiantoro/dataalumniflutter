part of 'properbyid_bloc.dart';

@immutable
class ProperbyidEvent {
}

class GetProperById extends ProperbyidEvent {
  int id;

  GetProperById({@required this.id});
}

class AddProper extends ProperbyidEvent {
  final ModelProper dataProper;
  List <ModelProper> dataProperOld;
  int urutanInd;
  String statusAksi;

  AddProper({this.dataProper, this.dataProperOld, this.urutanInd, this.statusAksi});
}

class EditProper extends ProperbyidEvent {
  final ModelProper dataProper;
  List <ModelProper> dataProperOld;
  int urutanInd;
  String statusAksi;
  int indexData;

  EditProper({this.indexData, this.dataProper, this.dataProperOld, this.urutanInd, this.statusAksi});
}

class HapusProper extends ProperbyidEvent {
  final ModelProper dataProper;
  List <ModelProper> dataProperOld;
  int urutanInd;
  String statusAksi;
  int indexData;

  HapusProper({this.indexData, this.dataProper, this.dataProperOld, this.urutanInd, this.statusAksi});
}