part of 'properbyid_bloc.dart';

@immutable
abstract class ProperbyidState{
}

class ProperbyidInitial extends ProperbyidState {}
class GetProperSukses extends ProperbyidState {
  List <ModelProper> listProper;
int indexInformasi;
String inisialIndex;
String statusAksi;
  GetProperSukses({this.listProper, this.indexInformasi, this.inisialIndex, this.statusAksi});
}
class GetProperWaiting extends ProperbyidState {
  
}
class GetProperError extends ProperbyidState {
  final String message;

  GetProperError({this.message});
}