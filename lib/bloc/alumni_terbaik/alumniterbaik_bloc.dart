import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/bloc/MenuAlumni/SearchAlumni/dataalumni_bloc.dart';
import 'package:dataAlumni/model/modelAlumniTerbaik.dart';
import 'package:dataAlumni/network/alumniTerbaikApi.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'alumniterbaik_event.dart';
part 'alumniterbaik_state.dart';

class AlumniterbaikBloc extends Bloc<AlumniterbaikEvent, AlumniterbaikState> {
  @override
  AlumniterbaikState get initialState => AlumniterbaikInitial();

  @override
  Stream<AlumniterbaikState> mapEventToState(
    AlumniterbaikEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    var refToken = preferences.getString("refToken");
    if(event is GetDataAlumniTerbaik){
      print("alumni terbaik");
      yield* _getListPost(tokens, refToken);
    }
  }
}

Stream<AlumniterbaikState> _getListPost(token, ref) async* {
  try {
    List<AlumiTerbaikModel> data = await AlumniTerbaikApi.getDataAlumniTerbaik(token, ref);
    print('masuk');
    print(data);
    yield GetAlumniTerbaikSukses(listPost: data);
  } catch (ex) {
    yield GetAlumniError(errorMessage: ex.message.toString());
  }
}