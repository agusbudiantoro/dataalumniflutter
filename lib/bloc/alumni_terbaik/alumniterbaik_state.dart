part of 'alumniterbaik_bloc.dart';

@immutable
abstract class AlumniterbaikState{
}

class AlumniterbaikInitial extends AlumniterbaikState {}
class GetAlumniTerbaikSukses extends AlumniterbaikState {
  List<AlumiTerbaikModel> listPost;

  GetAlumniTerbaikSukses({this.listPost});
}
class GetAlumniWaiting extends AlumniterbaikState {
  
}

class GetAlumniError extends AlumniterbaikState {
  final String errorMessage;
  GetAlumniError({
    this.errorMessage
  });
}