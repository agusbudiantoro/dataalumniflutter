part of 'childkomentar_bloc.dart';

@immutable
class ChildKomentarBlocEvent {
  int idKomentar;

  ChildKomentarBlocEvent({@required this.idKomentar});
}

class GetChildKomentarEvent extends ChildKomentarBlocEvent{
  int id;
  GetChildKomentarEvent({@required this.id});
}

class PostChildKomentarBlocEvent extends ChildKomentarBlocEvent{
  int idKomentar;
  String isiKomentar;

  PostChildKomentarBlocEvent({@required this.idKomentar, @required this.isiKomentar});
}