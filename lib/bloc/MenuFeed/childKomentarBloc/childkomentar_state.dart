part of 'childkomentar_bloc.dart';

@immutable
abstract class ChildKomentarBlocState {}

class GetChildKomentarBlocInitial extends ChildKomentarBlocState {}

// get data komentar
class GetChildKomentarLoadState extends ChildKomentarBlocState{
  List<KomentarModel> listChildKomentar;
  int idKomen;

  GetChildKomentarLoadState({this.idKomen, this.listChildKomentar});
}

class GetChildKomentarLoadErrorState extends ChildKomentarBlocState{
  final String errorMessage;
  GetChildKomentarLoadErrorState({this.errorMessage});
}
class GetChildKomentarLoadWaiting extends ChildKomentarBlocState{}

// post child komentar
class PostChildKomentarBlocStateSukses extends ChildKomentarBlocState{}
class PostChildKomentarBlocStateError extends ChildKomentarBlocState{
  final String errorMessage;
  PostChildKomentarBlocStateError({this.errorMessage});
}