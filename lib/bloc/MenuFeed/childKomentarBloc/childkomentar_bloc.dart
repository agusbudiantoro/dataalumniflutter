import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/feed/komentar_model.dart';
import 'package:dataAlumni/network/komentar_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'childkomentar_event.dart';
part 'childkomentar_state.dart';

class ChildKomentarBloc extends Bloc<ChildKomentarBlocEvent, ChildKomentarBlocState> {
  @override
  ChildKomentarBlocState get initialState => GetChildKomentarBlocInitial();

  @override
  Stream<ChildKomentarBlocState> mapEventToState(
    ChildKomentarBlocEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    var idUser = preferences.getInt("iduser");
    
    if(event is ChildKomentarBlocEvent){
      yield* _getChildKomentar(tokens, event.idKomentar);
    }
    if(event is PostChildKomentarBlocEvent){
      yield* _postChildKomentar(tokens, event.idKomentar, event.isiKomentar, idUser, state);
    }
  } 
}

// get childkomentar
Stream<ChildKomentarBlocState> _getChildKomentar(token, idKomen) async* {
  yield GetChildKomentarLoadWaiting();
  try {
    print(token);
    print(idKomen);
    List<KomentarModel> data = await KomentarApi.connectToApiChildKomen(token, idKomen);
    print("suksesget");
    print(data.length);
    yield GetChildKomentarLoadState(listChildKomentar: data, idKomen: idKomen);
  } catch (e) {
    print(e.toString());
    yield GetChildKomentarLoadErrorState(errorMessage: e.toString());
  }
}

// post komentar
Stream<ChildKomentarBlocState> _postChildKomentar(token, idKomen, isiKomen, idUser, state) async* {
  yield GetChildKomentarLoadWaiting();
  try {
    print(token);
    print(idKomen);
    GetChildKomentarLoadState postLoaded = state as GetChildKomentarLoadState;
    final data = await KomentarApi.connectToApiPostChildKomen(token, idKomen, isiKomen, idUser);
    print("suksespost");
    print(data);
    if(postLoaded.listChildKomentar.isNotEmpty){
      yield GetChildKomentarLoadState(listChildKomentar: postLoaded.listChildKomentar + data, idKomen: idKomen);
    } else {
      yield GetChildKomentarLoadState(listChildKomentar: data, idKomen: idKomen);
    }
  } catch (e) {
    print(e.toString());
    yield GetChildKomentarLoadErrorState(errorMessage: e.toString());
  }
}

