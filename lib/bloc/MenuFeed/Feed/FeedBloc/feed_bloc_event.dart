part of 'feed_bloc_bloc.dart';

@immutable
class FeedEvent {
  int idPostingan;
  int idUser;

  FeedEvent({this.idPostingan, this.idUser});
}


class GetListPost extends FeedEvent{

}

class GetListPostLimit extends FeedEvent {
  
}

class FeedRefresh extends FeedEvent{

}

class PostFeedGambar extends FeedEvent{
  File gambar;
  final CloningPosting dataPost;

  PostFeedGambar({@required this.dataPost, @required this.gambar});
}

class PostFeedYoutube extends FeedEvent{
  final CloningPosting dataPost;

  PostFeedYoutube({@required this.dataPost});
}

class PostLiked extends FeedEvent{
  int idPostingan;
  int idUser;
  int likeIndex;

  PostLiked({@required this.idPostingan, this.idUser, this.likeIndex});
}

class UnLiked extends FeedEvent{
  int idPostingan;
  int idUser;
  int likeIndex;

  UnLiked({@required this.idPostingan, this.idUser, this.likeIndex});
}