part of 'feed_bloc_bloc.dart';

@immutable
abstract class FeedState {}

class FeedInitial extends FeedState {}

class FeedLoaded extends FeedState {
  List<CloningPosting> listPost;
  bool hasReachMax;
  int counter;
  bool statusLike;
  bool statusUnLike;
  int indLike;
  FeedLoaded({
    this.listPost, this.hasReachMax, this.counter, this.statusLike, this.indLike, this.statusUnLike
  });

  FeedLoaded copyWith({List<CloningPosting> listPost, bool hasReachMax, int counter, int statusLike, int indLike, int statusUnLike}){
    return FeedLoaded(
      listPost: listPost ?? this.listPost,
      hasReachMax: hasReachMax ?? this.hasReachMax,
      counter: counter ?? this.counter,
      statusLike: statusLike ?? this.statusLike,
      indLike: indLike ?? this.indLike,
      statusUnLike: statusUnLike?? this.statusUnLike
    );
  }
}

class FeedError extends FeedState {
  final String errorMessage;
  bool statusState;
  FeedError({
    this.errorMessage, this.statusState
  });
}

class PostWaiting extends FeedState {}
class PostSukses extends FeedState{}
class PostError extends FeedState {
  final String errorMessage;
  PostError({
    this.errorMessage
  });
}

class PostLikeWaiting extends FeedState{}
class PostLikeSukses extends FeedState{
  final String suksessMessage;

  PostLikeSukses({this.suksessMessage});
}

class PostLikeError extends FeedState{
  final String errorMessage;
  PostLikeError({
    this.errorMessage,
  });
}