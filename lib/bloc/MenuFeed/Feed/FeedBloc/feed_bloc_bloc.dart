import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
// import 'package:dataAlumni/model/feed/feed_model.dart';
import 'package:dataAlumni/model/post.dart';
import 'package:dataAlumni/network/feed_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'feed_bloc_event.dart';
part 'feed_bloc_state.dart';

class FeedBloc extends Bloc<FeedEvent, FeedState> {
  @override
  FeedState get initialState => FeedInitial();

  @override
  Stream<FeedState> mapEventToState(FeedEvent event) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    List<CloningPosting> data;
    int awal =0;
    print("cobastate");
    print(state);
    if (state is FeedInitial) {
      yield* _getListPost(data, tokens, awal, idUser);
    }
    else {
      yield* _getListPostSecond(data, tokens, state, awal, idUser);
      
    }
    if(event is FeedRefresh){
      yield* _getListPost(data, tokens, awal, idUser);
    }
    if(event is PostLiked){
      yield* _postLiked(state,event.idPostingan, idUser, tokens, event.likeIndex);
    }
    if(event is UnLiked){
      print("masukblocunlike");
      yield* _unLiked(state,event.idPostingan, idUser, tokens, event.likeIndex);
    } if (event is PostFeedGambar){
      print("masuk gambar");
      print(event.dataPost);
      print(event.gambar);
      yield* _postFeed(event.gambar, event.dataPost, tokens, idUser);
    } if(event is PostFeedYoutube){
      yield* _postFeedYtb(event.dataPost, tokens, idUser);
    }
  }
}

Stream<FeedState> _postFeedYtb(data, tokens, idUser) async* {
  yield PostWaiting();
  try {
    bool isi = await Feed.postFeedYtb(data, tokens, idUser);
    print('masuk');
    print(isi);
    // yield PostSukses();
    if(isi == true){
      yield PostSukses();
    } else {
      yield PostError(errorMessage: "Error posting");
    }
  } catch (ex) {
    yield PostError(errorMessage: ex.message.toString());
  }
}

Stream<FeedState> _postFeed(gbr, data, tokens, idUser) async* {
  yield PostWaiting();
  try {
    bool isi = await Feed.postFeed(gbr, data, tokens, idUser);
    print('masuk');
    print(isi);
    // yield PostSukses();
    if(isi == true){
      print("sinimasuk");
      yield PostSukses();
    } else {
      yield PostError(errorMessage: "Error posting");
    }
  } catch (ex) {
    yield PostError(errorMessage: ex.message.toString());
  }
}

Stream<FeedState> _getListPost(data, token, awal, idUser) async* {
  yield PostWaiting();
  try {
    data = await Feed.connectToApi(token,awal, 2, idUser);
    print('masuk');
    print(data);
    yield FeedLoaded(listPost: data, hasReachMax: false, statusLike: false, statusUnLike: false);
  } catch (ex) {
    yield FeedError(errorMessage: ex.message.toString(), statusState: false);
  }
}

Stream<FeedState> _getListPostSecond(data, token, state, awal, idUser) async* {
  try {
    FeedLoaded postLoaded = state as FeedLoaded;
  int acuan;
  print("inidata");
  print(postLoaded.counter);
  if(postLoaded.counter == null){
    acuan = awal+1;
  } else {
    acuan = postLoaded.counter+1;
  }
  print("acuan");
  print(acuan);
  data = await Feed.connectToApi(token,acuan, 2, idUser);
  print('data kosong?');
  print(data.isEmpty);
  print(data);
  yield (data.isEmpty) ? postLoaded.copyWith(hasReachMax: true) : FeedLoaded(counter:acuan,listPost: postLoaded.listPost + data, hasReachMax: false, statusLike: false, statusUnLike: false);
  } catch (e) {
    print("error");
    print(e);
    yield FeedError(errorMessage: e.message.toString(), statusState: false);
  }
  
}

Stream<FeedState> _postLiked(state,idPosting, idUser, token, indexLike) async* {
  print("masukblok");
  try {
    FeedLoaded postLoaded = state as FeedLoaded;
    bool data = await Feed.postLikeApi(idPosting,idUser,token);
    print('masuk');
    print(indexLike);
    print(data);
    if(data == true){
      print("masuktrue");
      yield FeedLoaded(listPost: postLoaded.listPost, hasReachMax: postLoaded.hasReachMax, statusLike: true, indLike: indexLike, statusUnLike: false);
    } else {
      yield FeedError(errorMessage: "gagal like", statusState: true);
    }
  } catch (ex) {
    yield FeedError(errorMessage: ex.message.toString(), statusState: true);
  }
}

Stream<FeedState> _unLiked(state,idPosting, idUser, token, indexLike) async* {
  print("masukblok");
  try {
    FeedLoaded postLoaded = state as FeedLoaded;
    bool data = await Feed.postUnLikeApi(idPosting,idUser,token);
    print('masuk');
    print(data);
    if(data == true){
      print("masuktrue");
      yield FeedLoaded(listPost: postLoaded.listPost, hasReachMax: postLoaded.hasReachMax, statusLike: false, indLike: indexLike, statusUnLike: true);
    } else {
      yield FeedError(errorMessage: "gagal like", statusState: true);
    }
  } catch (ex) {
    yield FeedError(errorMessage: ex.message.toString(), statusState: true);
  }
}