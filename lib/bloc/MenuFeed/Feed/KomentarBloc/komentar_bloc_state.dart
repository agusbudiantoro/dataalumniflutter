part of 'komentar_bloc_bloc.dart';

@immutable
abstract class KomentarBlocState {}

class GetKomentarBlocInitial extends KomentarBlocState {}

// get data komentar
class GetKomentarLoadState extends KomentarBlocState{
  List<KomentarModel> listKomentar;
  List<KomentarModel> listChildKomentar;
  int idPosting;
  bool isExpanded;

  GetKomentarLoadState({this.listKomentar, this.idPosting, this.listChildKomentar, this.isExpanded});
}

class GetKomentarLoadErrorState extends KomentarBlocState{
  final String errorMessage;
  GetKomentarLoadErrorState({this.errorMessage});
}

class GetKomentarLoadWaiting extends KomentarBlocState{}


// post data komenatar
class PostKomentarSuccessLoadState extends KomentarBlocState{
  
}

class PostKomentarLoadErrorState extends KomentarBlocState{
  final String errorMessage;
  PostKomentarLoadErrorState({this.errorMessage});
}

class PostKomentarLoadWaiting extends KomentarBlocState{}

// get data child komentar
// class GetChildKomentarInitialState extends KomentarBlocState{}

// class GetChildKomentarLoadState extends KomentarBlocState {
// List<KomentarModel> listChildKomentar;

// GetChildKomentarLoadState({this.listChildKomentar});
// }

// class GetChildKomentarErrorState extends KomentarBlocState{

// final String errorMessage;
//   GetChildKomentarErrorState({this.errorMessage});
// }