import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/feed/komentar_model.dart';
import 'package:dataAlumni/network/komentar_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'komentar_bloc_event.dart';
part 'komentar_bloc_state.dart';

class KomentarBloc extends Bloc<KomentarBlocEvent, KomentarBlocState> {
  @override
  KomentarBlocState get initialState => GetKomentarBlocInitial();

  @override
  Stream<KomentarBlocState> mapEventToState(
    KomentarBlocEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    var idUser = preferences.getInt("iduser");
    var gbr = preferences.getString("fotoProfil");
    if(state is GetKomentarBlocInitial){
      yield* _getKomentar(tokens, idUser, event.idPosting);
    }
    if(event is PostKomentarEvent){
      yield* _postKomentar(tokens, idUser, event.idPostingan, event.isiKomentar, state);
    }
    // if(event is GetChildKomentarEvent){
    //   yield* _getChildKomentar(tokens, event.idKomentar, state);
    // }
  } 
}

// get komentar
Stream<KomentarBlocState> _getKomentar(token, idUser, idPost) async* {
  yield GetKomentarLoadWaiting();
  try {
    print(idUser);
    print(idPost);
    List<KomentarModel> data = await KomentarApi.connectToApi(token, idPost);
    var data1 = data.reversed.toList();
    yield GetKomentarLoadState(listKomentar: data1, idPosting: idPost, isExpanded: true);
  } catch (e) {
    print(e.toString());
    yield GetKomentarLoadErrorState(errorMessage: e.toString());
  }
}

// post komentar
Stream<KomentarBlocState> _postKomentar(tokens, idUser, idPostingan, isiKomentar, state) async* {
  yield GetKomentarLoadWaiting();
  try {
    GetKomentarLoadState postLoaded = state as GetKomentarLoadState;
    
    final data = await KomentarApi.connectToApiPostKomen(tokens, idUser, idPostingan, isiKomentar);
    
    if(postLoaded.listKomentar.isNotEmpty){
      yield GetKomentarLoadState(listKomentar: postLoaded.listKomentar + data, idPosting: idPostingan);  
    } else {
      yield GetKomentarLoadState(listKomentar: data, idPosting: idPostingan, isExpanded: true);
    }
    } catch (e) {
    print(e.toString());
    yield GetKomentarLoadErrorState(errorMessage: e.toString());
  }
}

// get childkomentar
// Stream<KomentarBlocState> _getChildKomentar(token, idKomen, state) async* {
//   // yield GetKomentarLoadWaiting();
//   try {
//     GetKomentarLoadState postLoaded = state as GetKomentarLoadState;
//     print(token);
//     print(idKomen);
//     List<KomentarModel> data = await KomentarApi.connectToApiChildKomen(token, idKomen);
//     print("sukses");
//     print(data.length);
//     yield GetKomentarLoadState(listChildKomentar: data, listKomentar: postLoaded.listKomentar, idPosting: idKomen);
//   } catch (e) {
//     print(e.toString());
//     yield GetKomentarLoadErrorState(errorMessage: e.toString());
//   }
// }