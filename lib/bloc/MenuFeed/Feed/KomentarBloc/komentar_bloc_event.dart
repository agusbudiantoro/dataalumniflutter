part of 'komentar_bloc_bloc.dart';

@immutable
class KomentarBlocEvent {
  int idPosting;

  KomentarBlocEvent({@required this.idPosting});
}

class GetKomentarEvent extends KomentarBlocEvent{
  int id;
  GetKomentarEvent({@required this.id});
}

class PostKomentarEvent extends KomentarBlocEvent{
  int idPostingan;
  String isiKomentar;
  PostKomentarEvent({@required this.idPostingan, @required this.isiKomentar});
}

// class GetChildKomentarEvent extends KomentarBlocEvent{
//   int idKomentar;
  
//   GetChildKomentarEvent({@required this.idKomentar});
// }