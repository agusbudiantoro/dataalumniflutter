import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/agenda/modelPengumuman.dart';
import 'package:dataAlumni/network/menu_agenda/pengumumanApi.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'pengumuman_event.dart';
part 'pengumuman_state.dart';

class PengumumanBloc extends Bloc<PengumumanEvent, PengumumanState> {
  @override
  PengumumanState get initialState => PengumumanInitial();

  @override
  Stream<PengumumanState> mapEventToState(
    PengumumanEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    int idUser = preferences.getInt("iduser");
    List<ModelPengumuman> data;
    int awal =0;
    if(state is PengumumanInitial){
      print("masuk pengumuman");
      yield* _getListPost(data, tokens, awal, idUser);
    }else {
      yield* _getListPostSecond(data, tokens, state, awal, idUser);
    }
    if(event is RefreshPengumuman){
      yield* _getListPost(data, tokens, awal, idUser);
    }
  }
}

Stream<PengumumanState> _getListPost(data, token, awal, idUser) async* {
  try {
    data = await PengumumanApi.connectToApi(token,awal, 2, idUser);
    print('masuk');
    print(data);
    yield GetPengumumanSukses(listPost: data, hasReachMax: false);
  } catch (ex) {
    yield GetPengumumanError(errorMessage: ex.message.toString());
  }
}

Stream<PengumumanState> _getListPostSecond(data, token, state, awal, idUser) async* {
  try {
    GetPengumumanSukses postLoaded = state as GetPengumumanSukses;
  int acuan;
  print("inidata");
  print(postLoaded.counter);
  if(postLoaded.counter == null){
    acuan = awal+1;
  } else {
    acuan = postLoaded.counter+1;
  }
  print("acuan");
  print(acuan);
  data = await PengumumanApi.connectToApi(token,acuan, 2, idUser);
  print('data kosong?');
  print(data.isEmpty);
  print(data);
  yield (data.isEmpty) ? postLoaded.copyWith(hasReachMax: true) : GetPengumumanSukses(counter:acuan,listPost: postLoaded.listPost + data, hasReachMax: false);
  } catch (e) {
    print("error");
    print(e);
    yield GetPengumumanError(errorMessage: e.message.toString());
  }
  
}
