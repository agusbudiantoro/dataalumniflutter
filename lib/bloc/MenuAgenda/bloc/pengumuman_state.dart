part of 'pengumuman_bloc.dart';

@immutable
abstract class PengumumanState{
}

class PengumumanInitial extends PengumumanState {}

class GetPengumumanSukses extends PengumumanState{
  List<ModelPengumuman> listPost;
  bool hasReachMax;
  int counter;
  GetPengumumanSukses({
    this.listPost, this.hasReachMax, this.counter
  });

  GetPengumumanSukses copyWith({List<ModelPengumuman> listPost, bool hasReachMax, int counter, int statusLike, int indLike, int statusUnLike}){
    return GetPengumumanSukses(
      listPost: listPost ?? this.listPost,
      hasReachMax: hasReachMax ?? this.hasReachMax,
      counter: counter ?? this.counter,
    );
  }
}
class GetPengumumanWaiting extends PengumumanState{}
class GetPengumumanError extends PengumumanState{
  final String errorMessage;
  GetPengumumanError({
    this.errorMessage
  });
}