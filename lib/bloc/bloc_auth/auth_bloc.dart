import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dataAlumni/model/cek_token.dart';
import 'package:dataAlumni/model/login_model.dart';
import 'package:dataAlumni/model/registModel.dart';
import 'package:meta/meta.dart';
import 'package:dataAlumni/network/api_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  @override
  AuthState get initialState => AuthInitial();

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var tokens = preferences.getString("token");
    var refToken = preferences.getString("refToken");
    var idUser = preferences.getInt("iduser");
    if (event is LoginEvent) {
      yield* _loginAction(event.loginModelRequest);
    }
    if (event is CekTokenEvent) {
      yield* _cekToken(tokens,refToken);
    }
    if (event is RegisterEvent) {
      yield* _registAction(event.registerModelRequest);
    }
  }
}

Stream<AuthState> _registAction(RegisterModel request) async* {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  ApiProvider _apiProvider = ApiProvider();
  yield LoginWaiting();
  try {
    RegisterModel data = await _apiProvider.regist(request);
    print("woy3");
    print(preferences.getString("token"));
    yield RegistSuccess(registModel: data);
  } catch (ex) {
    print(ex.toString()) ; 
    yield RegistError(errorMessage: ex.message.toString());
  }
}

Stream<AuthState> _loginAction(LoginModel request) async* {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  ApiProvider _apiProvider = ApiProvider();
  yield LoginWaiting();
  try {
    LoginModel data = await _apiProvider.login(request);
    await preferences.setInt("iduser", data.iduser);
    await preferences.setString("token", data.accessToken);
    await preferences.setString("namaUser", data.namauser);
    await preferences.setString("namaLemdik", data.lemdik);
    await preferences.setString("email", data.email);
    await preferences.setString("refToken", data.refreshToken);
    print("woy3");
    print(preferences.getString("token"));
    yield LoginSuccess(loginModel: data);
  } catch (ex) {
    print(ex.toString()) ; 
    yield LoginError(errorMessage: ex.message.toString());
  }
}

Stream<AuthState> _cekToken(token, refToken) async* {
  print(token);
  print("cektoken1");
  print("cektoken5");
  ApiProvider _apiProvider = ApiProvider();
  yield CekTokenWaiting();
  await Future.delayed(Duration(seconds: 3));
  try {
    bool data = await ApiProvider.refreshToken(refToken);
    print("cektoken2");
    print(data);
    if(data == true){
      yield CekTokenSukses(cekToken: data);
    } else{
      yield CekTokenError(errorMessage: "error");
    }
  } catch (ex) {
    print("cektokenerror");
    print(ex.toString()) ; 
    yield CekTokenError(errorMessage: ex.message.toString());
  }
}