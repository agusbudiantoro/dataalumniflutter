part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class LoginSuccess extends AuthState {
  final LoginModel loginModel;
  LoginSuccess({@required this.loginModel});
}

class LoginError extends AuthState {
  final String errorMessage;
  LoginError({
    this.errorMessage,
  });
}

class LoginWaiting extends AuthState {}

class CekTokenInitial extends AuthState {}

class CekTokenSukses extends AuthState {
  bool cekToken;
  CekTokenSukses({@required this.cekToken});
}

class CekTokenError extends AuthState{
  final String errorMessage;
  CekTokenError({this.errorMessage});
}

class CekTokenWaiting extends AuthState{}

class RegistSuccess extends AuthState {
  final RegisterModel registModel;
  RegistSuccess({@required this.registModel});
}

class RegistError extends AuthState {
  final String errorMessage;
  RegistError({
    this.errorMessage,
  });
}

class RegistWaiting extends AuthState {}