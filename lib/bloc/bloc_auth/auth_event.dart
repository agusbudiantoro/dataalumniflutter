part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class LoginEvent extends AuthEvent {
  final LoginModel loginModelRequest;
  LoginEvent({@required this.loginModelRequest});
  
}

class CekTokenEvent extends AuthEvent {
}

class RegisterEvent extends AuthEvent{
  final RegisterModel registerModelRequest;
  RegisterEvent({@required this.registerModelRequest});
}