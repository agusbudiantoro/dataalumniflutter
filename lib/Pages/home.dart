import 'package:dataAlumni/Pages/Widget/BottomNavigation2.dart';
import 'package:dataAlumni/bloc/MenuAgenda/bloc/pengumuman_bloc.dart';
import 'package:dataAlumni/bloc/MenuAlumni/DetailProfil/detailprofil_bloc.dart';
import 'package:dataAlumni/bloc/MenuAlumni/SearchAlumni/dataalumni_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/childKomentarBloc/childkomentar_bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/InformasiUser/informasi_user_bloc.dart';
// import 'package:dataAlumni/bloc/PostBloc/bloc_post_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:like_button/like_button.dart';
import 'package:dataAlumni/Pages/Widget/BottomNavigation.dart';

class Home extends StatefulWidget {
  final int indexTo;

  Home({this.indexTo});
  @override
  _HomeState createState() => _HomeState(indexTo: this.indexTo);
}

class _HomeState extends State<Home> {
  final int indexTo;

  _HomeState({this.indexTo});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiBlocProvider(
        providers: [
          BlocProvider<KomentarBloc>(
            create: (context) => KomentarBloc(),
          ),
          BlocProvider<ChildKomentarBloc>(
            create: (context) => ChildKomentarBloc(),
          ),
          BlocProvider<FeedBloc>(
            create: (BuildContext context) => FeedBloc()..add(FeedEvent()),
          ),
          BlocProvider<FeedProfileBloc>(
            create: (BuildContext context) => FeedProfileBloc()..add(FeedProfileEvent()),
          ),
          BlocProvider<PengumumanBloc>(
            create: (BuildContext context) => PengumumanBloc(),
          ),
          BlocProvider<DataalumniBloc>(
            create: (BuildContext context) => DataalumniBloc()..add(DataalumniEvent())
          ),
          BlocProvider<DetailprofilBloc>(
            create: (BuildContext context) => DetailprofilBloc()..add(GetDetailProfilEvent()),
          )
        ],
        child: T1BottomNavigation(indexTo: indexTo),
      )
    );
  }
}