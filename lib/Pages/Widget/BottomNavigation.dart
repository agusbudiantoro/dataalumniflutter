// import 'package:dataAlumni/Pages/Page_MenuBottom/page_agenda.dart';
// import 'package:dataAlumni/Pages/Page_MenuBottom/page_alumni.dart';
// import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_feed.dart';
// import 'package:dataAlumni/Pages/Page_MenuBottom/page_profile.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/widgets.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:dataAlumni/util/DbExtension.dart';
// import 'package:dataAlumni/util/BottomNavigationUt.dart';
// import 'package:dataAlumni/util/Constant.dart';
// import 'package:dataAlumni/util/Images.dart';
// import 'package:dataAlumni/util/Colors.dart';
// import 'package:dataAlumni/util/LblString.dart';
// import 'package:dataAlumni/util/Widget/WidgetNavigationButton.dart';

// class T3BottomNavigation extends StatefulWidget {
//   static var tag = "/T3BottomNavigation";

//   @override
//   T3BottomNavigationState createState() => T3BottomNavigationState();
// }

// class T3BottomNavigationState extends State<T3BottomNavigation> {
//   int _pageBottom = 0;
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     changeStatusColor(t3_app_background);

//     return Scaffold(
//         bottomNavigationBar: CurvedNavigationBar(
//           backgroundColor: food_colorBlueAccent,
//           color: t3_white,
//           initialIndex: _pageBottom,
//           items: <Widget>[
//             SvgPicture.asset(
//               t3_ic_home,
//               height: 24,
//               width: 24,
//               fit: BoxFit.none,
//             ),
//             SvgPicture.asset(
//               t3_ic_msg,
//               height: 24,
//               width: 24,
//               fit: BoxFit.none,
//             ),
            
//             SvgPicture.asset(
//               t3_notification,
//               height: 24,
//               width: 24,
//               fit: BoxFit.none,
//             ),
//             SvgPicture.asset(
//               t3_ic_user,
//               height: 24,
//               width: 24,
//               fit: BoxFit.none,
//             ),
//           ],
//           onTap: onTabTapp,
//         ),
//         body: bodyItem(_pageBottom));
//   }

//   void onTabTapp(int index) {
//     setState(() {
//       _pageBottom = index;
//     });
//   }

//   Widget bodyItem(int i) {
//     print(i);
//     if (i == 0) {
//       return FeedPage();
//     } else if (i == 1) {
//       return Alumni();
//     } else if (i == 2) {
//       return Agenda();
//     } else if (i == 3) {
//       return Profil();
//     }
//   }
// }
