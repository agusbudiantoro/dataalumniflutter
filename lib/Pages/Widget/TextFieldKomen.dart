// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:dataAlumni/bloc/Feed/KomentarBloc/komentar_bloc_bloc.dart';
// import 'package:dataAlumni/util/Colors.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:fluttertoast/fluttertoast.dart';

// class TextFieldKomen extends StatefulWidget {
//   final String foto;
//   final int idPostinganWidget;
//   final int idKomentarWidget;

//   TextFieldKomen(
//       {this.foto, this.idPostinganWidget, this.idKomentarWidget});
//   @override
//   _TextFieldKomenState createState() => _TextFieldKomenState(
//       foto: this.foto,
//       idPostinganWidget: this.idPostinganWidget,
//       idKomentarWidget: this.idKomentarWidget,);
// }

// class _TextFieldKomenState extends State<TextFieldKomen> {
//   final TextEditingController _isiKomentar = TextEditingController();
//   _TextFieldKomenState(
//       {this.foto, this.idPostinganWidget, this.idKomentarWidget});
//   final String foto;
//   final int idPostinganWidget;
//   final int idKomentarWidget;

//   @override
//   void initState() {
//     print("inifoto");
//     print(_isiKomentar.text);
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final _komentarBloc = KomentarBloc();
//     return Transform.translate(
//       offset: Offset(0.0, -1 * MediaQuery.of(context).viewInsets.bottom),
//       child: Container(
//         height: 85.0,
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.only(
//             topLeft: Radius.circular(30.0),
//             topRight: Radius.circular(30.0),
//           ),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black12,
//               offset: Offset(0, -2),
//               blurRadius: 6.0,
//             ),
//           ],
//           color: Colors.white,
//         ),
//         child: Padding(
//           padding: EdgeInsets.all(12.0),
//           child: TextField(
//             controller: _isiKomentar,
//             decoration: InputDecoration(
//               border: InputBorder.none,
//               enabledBorder: OutlineInputBorder(
//                 borderRadius: BorderRadius.circular(30.0),
//                 borderSide: BorderSide(color: Colors.grey),
//               ),
//               focusedBorder: OutlineInputBorder(
//                 borderRadius: BorderRadius.circular(30.0),
//                 borderSide: BorderSide(color: Colors.grey),
//               ),
//               contentPadding: EdgeInsets.all(20.0),
//               hintText: 'Add a comment',
//               prefixIcon: Container(
//                 margin: EdgeInsets.all(4.0),
//                 width: 48.0,
//                 height: 48.0,
//                 decoration: BoxDecoration(
//                   shape: BoxShape.circle,
//                   boxShadow: [
//                     BoxShadow(
//                       color: Colors.black45,
//                       offset: Offset(0, 2),
//                       blurRadius: 6.0,
//                     ),
//                   ],
//                 ),
//                 child: CircleAvatar(
//                   child: ClipOval(
//                     child: CachedNetworkImage(
//                       height: 48.0,
//                       width: 48.0,
//                       imageUrl: foto,
//                       fit: BoxFit.cover,
//                     ),
//                   ),
//                 ),
//               ),
//               suffixIcon: Container(
//                 margin: EdgeInsets.only(right: 4.0),
//                 width: 20.0,
//                 child: BlocListener<KomentarBloc, KomentarBlocState>(
//                             bloc: _komentarBloc,
//                             listener: (context, state) {
//                               if (state is GetKomentarLoadErrorState) {
//                                 Fluttertoast.showToast(msg: state.errorMessage);
//                                 Fluttertoast.showToast(
//                                     msg: "Memuat data Gagal");
//                               }
//                               if (state is GetKomentarLoadState) {
//                                 Fluttertoast.showToast(msg: "Success Memuat");
//                                 // Navigator.push(
//                                 //     context,
//                                 //     MaterialPageRoute(
//                                 //       builder: (context) => Feed_Komentar(
//                                 //         data: data.listPost[index],
//                                 //         ytUrl: _controllersN[index],
//                                 //         listKomentar: state.listKomentar,
//                                 //         myIdKomentator: state.idPosting,
//                                 //       ),
//                                 //     ));
//                               }
//                               if (state is GetKomentarLoadWaiting) {
//                                 Fluttertoast.showToast(msg: "Loading data");
//                               }
//                             },
//                             child: Container(
//                               child:
//                                   BlocBuilder<KomentarBloc, KomentarBlocState>(
//                                 bloc: _komentarBloc,
//                                 builder: (context, state) {
//                                   if (state is GetKomentarLoadWaiting) {
//                                     return CircularProgressIndicator(
//                                       backgroundColor: Colors.white,
//                                     );
//                                   }
//                                   if (state is GetKomentarLoadErrorState) {
//                                     return buildFlatButtonSendKomen(_komentarBloc);
//                                   }
//                                   if (state is GetKomentarLoadState) {
//                                     return buildFlatButtonSendKomen(_komentarBloc);
//                                   }
//                                   return buildFlatButtonSendKomen(_komentarBloc);
//                                 },
//                               ),
//                             ),
//                           )
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   FlatButton buildFlatButtonSendKomen(KomentarBloc bloc) {
//     return FlatButton(
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(25.0),
//       ),
//       color: Colors.white,
//       onPressed: () {
//         bloc..add(PostKomentarEvent(idPostingan: idPostinganWidget, isiKomentar: _isiKomentar.text));
//       },
//       child: Icon(
//         Icons.send,
//         size: 25.0,
//         color: food_colorPrimaryDark,
//       ),
//     );
//   }
// }
