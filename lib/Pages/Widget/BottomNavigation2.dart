import 'dart:io';

import 'package:dataAlumni/Pages/Page_MenuBottom/page_agenda/page_agenda.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_alumni/page_alumni.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_feed.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedGalery.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedYoutube.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/page_profile.dart';
import 'package:dataAlumni/Pages/Widget/spinCircle.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/util/model/bottomMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/WidgetNavigationButton.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:image_picker/image_picker.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';

class T1BottomNavigation extends StatefulWidget {
  static var tag = "/T1BottomNavigation";
  final int indexTo;

  T1BottomNavigation({this.indexTo});

  @override
  T1BottomNavigationState createState() =>
      T1BottomNavigationState(indexTo: this.indexTo);
}

class T1BottomNavigationState extends State<T1BottomNavigation> {
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();
  var isSelected = 1;
  FeedBloc bloc;
  File imageFile;
  final int indexTo;
  T1BottomNavigationState({this.indexTo});
  GlobalKey _one = GlobalKey();
  int statusPagePost;
  // GlobalKey _two = GlobalKey();
  // GlobalKey _three = GlobalKey();
  // GlobalKey _four = GlobalKey();
  // GlobalKey _five = GlobalKey();
  BuildContext myContext;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (indexTo == null) {
      isSelected = 1;
    } else {
      isSelected = indexTo;
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(milliseconds: 200),
          () => ShowCaseWidget.of(myContext).startShowCase([_one]));
    });
    navigationPage();
  }

  void navigationPage() {
    print("status post page");
    print(statusPagePost);
    if (statusPagePost == 2) {
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AddYoutube(),
        ),
      );
    }
  }

  _openGalery(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFile = picture;
    });
    Navigator.pop(context);
    if (imageFile != null) {
      return Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PostGalery(imageFile: imageFile)));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }
  }

  _openKamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = picture;
    });
    // Navigator.of(context).pop();
    Navigator.pop(context);
    if (imageFile != null) {
      return Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PostGalery(imageFile: imageFile)));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }
  }

  Widget tabItem(var pos, var icon) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isSelected = pos;
        });
      },
      child: Container(
        width: 45,
        height: 45,
        alignment: Alignment.center,
        decoration: isSelected == pos
            ? BoxDecoration(shape: BoxShape.circle, color: Colors.white)
            : BoxDecoration(),
        child: SvgPicture.asset(
          icon,
          width: 20,
          height: 20,
          color: isSelected == pos ? Colors.blueAccent : t1_textColorSecondary,
        ),
      ),
    );
  }

  Future<void> _showAlert(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Row(
                    children: [
                      GestureDetector(
                          child: Row(
                            children: [
                              Icon(Icons.add_photo_alternate,
                                  color: Colors.black),
                              SizedBox(width: 5),
                              Text("Galery")
                            ],
                          ),
                          onTap: () => _openGalery(context)),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                          child: Row(
                            children: [
                              Icon(Icons.add_a_photo,
                                  color: Colors.black),
                              SizedBox(width: 5),
                              Text("Tambah Foto")
                            ],
                          ),
                          onTap: () {
                            _openKamera(context);
                            // Navigator.pop(context);
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => AddYoutube()));
                          })
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                          child: Row(
                            children: [
                              Icon(Icons.play_circle_fill_rounded,
                                  color: Colors.black),
                              SizedBox(width: 5),
                              Text("Tambah Youtube Video")
                            ],
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddYoutube()));
                          })
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // final primaryColor = Theme.of(context).primaryColor;
    return Scaffold(
      body: bodyItem(context),
    //   floatingActionButton: floatingButton(primaryColor),
      bottomNavigationBar: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 5),
            height: 60,
            decoration: BoxDecoration(
              color: t1_white,
              boxShadow: [
                BoxShadow(
                    color: shadow_color,
                    blurRadius: 10,
                    spreadRadius: 2,
                    offset: Offset(0, 3.0))
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
      tabItem(1, t1_home),
                  tabItem(2, t1_file),
                  Container(width: 45, height: 45),
                  tabItem(3, t1_notification),
                  tabItem(4, t1_user),
                ],
              ),
            ),
          ),
          Container(
            child: FloatingActionButton(
              backgroundColor: Colors.blueAccent,
              onPressed: () {
                return _showAlert(context);
              },
              child: Icon(
                Icons.add_to_photos_rounded,
                color: t1_white,
              ),
            ),
          )
        ],
      ),
    );
  }

  // void _showSnackBar(BuildContext context, String message) {
  //   Scaffold.of(context).showSnackBar(SnackBar(
  //     content: Text(message),
  //     duration: const Duration(milliseconds: 1000),
  //   ));
  // }

  // Widget floatingButton(primaryColor) {
  //   return Builder(
  //     builder: (context) => FabCircularMenu(
  //       key: fabKey,
  //       // Cannot be `Alignment.center`
  //       alignment: Alignment.bottomRight,
  //       ringColor: Colors.white.withAlpha(25),
  //       ringDiameter: 500.0,
  //       ringWidth: 150.0,
  //       fabSize: 64.0,
  //       fabElevation: 8.0,
  //       fabIconBorder: CircleBorder(),
  //       // Also can use specific color based on wether
  //       // the menu is open or not:
  //       // fabOpenColor: Colors.white
  //       // fabCloseColor: Colors.white
  //       // These properties take precedence over fabColor
  //       fabColor: Colors.white,
  //       fabOpenIcon: Icon(Icons.menu, color: primaryColor),
  //       fabCloseIcon: Icon(Icons.close, color: primaryColor),
  //       fabMargin: const EdgeInsets.all(16.0),
  //       animationDuration: const Duration(milliseconds: 800),
  //       animationCurve: Curves.easeInOutCirc,
  //       onDisplayChange: (isOpen) {
  //         _showSnackBar(context, "The menu is ${isOpen ? "open" : "closed"}");
  //         // fabKey.currentState.
  //         // if (statusPagePost == 2) {
  //         //   Navigator.pop(context);
  //         //   Navigator.push(
  //         //     context,
  //         //     MaterialPageRoute(
  //         //       builder: (context) => AddYoutube(),
  //         //     ),
  //         //   );
  //         // }
  //       },
  //       children: <Widget>[
  //         RawMaterialButton(
  //           onPressed: () {
  //             // setState(() {
  //             //   statusPagePost = 2;
  //             // });
  //             Navigator.pop(context);
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(
  //               builder: (context) => AddYoutube(),
  //             ),
  //           );
  //           },
  //           shape: CircleBorder(),
  //           padding: const EdgeInsets.all(24.0),
  //           child: Icon(Icons.looks_one, color: Colors.white),
  //         ),
  //         RawMaterialButton(
  //           onPressed: () {
  //             _showSnackBar(context, "You pressed 2");
  //           },
  //           shape: CircleBorder(),
  //           padding: const EdgeInsets.all(24.0),
  //           child: Icon(Icons.looks_two, color: Colors.white),
  //         ),
  //         RawMaterialButton(
  //           onPressed: () {
  //             _showSnackBar(context, "You pressed 3");
  //           },
  //           shape: CircleBorder(),
  //           padding: const EdgeInsets.all(24.0),
  //           child: Icon(Icons.looks_3, color: Colors.white),
  //         ),
  //         RawMaterialButton(
  //           onPressed: () {
  //             _showSnackBar(
  //                 context, "You pressed 4. This one closes the menu on tap");
              
  //           },
  //           shape: CircleBorder(),
  //           padding: const EdgeInsets.all(24.0),
  //           child: Icon(Icons.looks_4, color: Colors.white),
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget bodyItem(BuildContext context) {
    // return SpinCircleBottomBarHolder(
    //     bottomNavigationBar: SCBottomBarDetails(
    //         circleColors: [Colors.white, Colors.blue, Colors.blueAccent],
    //         iconTheme: IconThemeData(color: Colors.black),
    //         activeIconTheme: IconThemeData(color: Colors.blueAccent),
    //         actionButtonDetails: SCActionButtonDetails(
    //             color: Colors.blueAccent, icon: Icon(Icons.add), elevation: 0),
    //         backgroundColor: Colors.white,
    //         bnbHeight: 70,
    //         items: <SCBottomBarItem>[
    //           SCBottomBarItem(
    //               icon: Icons.home,
    //               onPressed: () {
    //                 setState(() {
    //                   isSelected = 1;
    //                 });
    //               }),
    //           SCBottomBarItem(
    //               icon: Icons.list,
    //               onPressed: () {
    //                 setState(() {
    //                   isSelected = 2;
    //                 });
    //               }),
    //           SCBottomBarItem(
    //               icon: Icons.notifications,
    //               onPressed: () {
    //                 setState(() {
    //                   isSelected = 3;
    //                 });
    //               }),
    //           SCBottomBarItem(
    //               icon: Icons.person,
    //               onPressed: () {
    //                 setState(() {
    //                   isSelected = 4;
    //                 });
    //               })
    //         ],
    //         circleItems: <SCItem>[
    //           SCItem(
    //               icon: Icon(Icons.add_photo_alternate),
    //               onPressed: () {
    //                 _openGalery(context);
    //               }),
    //           SCItem(
    //               icon: Icon(Icons.play_circle_fill_rounded),
    //               onPressed: () {
    //                 setState(() {
    //                   statusPagePost = 2;
    //                 });
    //                 print(statusPagePost);
    //                 return AddYoutube();
    //                 // navigationPage.call();
    //                 // Navigator.of(context).pop();
    //                 // // Navigator.of(context).pop(false);
    //                 // return Navigator.push(context,
    //                 //     MaterialPageRoute(builder: (context) => AddYoutube()));
    //               }),
    //           SCItem(
    //               icon: Icon(Icons.add_a_photo),
    //               onPressed: () {
    //                 _openKamera(context);
    //               }),
    //         ]),
    //     child: (isSelected == 1)
    //         ? FeedPage()
    //         : (isSelected == 2)
    //             ? Alumni()
    //             : (isSelected == 3)
    //                 ? Agenda()
    //                 : (isSelected == 4)
    //                     ? T10Profile()
    //                     : Container());
      if (isSelected == 1) {
        // Navigator.of(context).pop();
        // Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => FeedPage()));
        return FeedPage();
      } else if (isSelected == 2) {
        return Alumni();
      } else if (isSelected == 3) {
        return Agenda();
      } else if (isSelected == 4) {
        return T10Profile();
      }
    }
}
