import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:dataAlumni/model/feed/feed_model.dart';
import 'package:flutter/material.dart';

class ShowCaption extends StatefulWidget {
  final CloningPosting data;
  final int indexKe;

  ShowCaption({this.data, this.indexKe});
  @override
  _ShowCaptionState createState() =>
      _ShowCaptionState(data: this.data, indexKe: this.indexKe);
}

class _ShowCaptionState extends State<ShowCaption> {
  final CloningPosting data;
  final int indexKe;
  _ShowCaptionState({this.data, this.indexKe});
  bool show = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 15),
          child: show
              ? Text(
                  data.caption,
                  textAlign: TextAlign.justify,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Text(
                        (data.caption.length < 50)
                            ? data.caption
                            : data.caption.substring(0, 50) + '...',
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
        ),
        (data.caption.length < 50)
            ? Text("")
            : Container(
                margin: EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      show = !show;
                    });
                    print(show);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      show
                          ? Text(
                              "Less",
                              style: TextStyle(color: Colors.blue),
                            )
                          : Text("More", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                ),
              ),
      ],
    );
  }
}
