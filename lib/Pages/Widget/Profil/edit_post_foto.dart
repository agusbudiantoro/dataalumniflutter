import 'dart:io';

import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toggle_switch/toggle_switch.dart';

class PostGaleryProfil extends StatefulWidget {
  final CloningPosting dataList;
  final int indexTo;
  PostGaleryProfil({@required this.dataList, @required this.indexTo});
  @override
  _PostGaleryProfilState createState() =>
      _PostGaleryProfilState(dataList: this.dataList, indexTo: this.indexTo);
}

class _PostGaleryProfilState extends State<PostGaleryProfil> {
  final TextEditingController _caption = TextEditingController();
  final TextEditingController _judul = TextEditingController();
  final TextEditingController _subjudul = TextEditingController();
  File imageFiles;
  final int indexTo;
  final CloningPosting dataList;
  _PostGaleryProfilState({this.dataList, this.indexTo});

  @override
  void initState() {
    super.initState();
    _caption.text = dataList.caption;
    _judul.text = dataList.judul;
    _subjudul.text = dataList.subjudul;
  }

  _openGalery() async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFiles = picture;
    });
    // Navigator.of(context).pop();
    // if (imageFile != null) {
    //   return Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => PostGaleryProfil(dataList: dataList)));
    // } else {
    //   Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    // }
  }

  @override
  Widget build(BuildContext context) {
    final _postBloc = FeedProfileBloc();
    return Scaffold(
      appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Text('Edit Postingan'),
          leading: Container(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back_ios, color: Colors.white),
            ),
          ),
          actions: <Widget>[
            BlocListener<FeedProfileBloc, FeedProfileState>(
              bloc: _postBloc,
              listener: (context, state) {
                if(state is DeleteSukses){
                  Fluttertoast.showToast(msg: "Success Delete");
                  Navigator.of(context).pop(true);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(indexTo: 4)));
                }
                if (state is EditSukses) {
                  Fluttertoast.showToast(msg: "Success posting");
                  Navigator.of(context).pop(true);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(indexTo: 4)));
                }
                if (state is EditError) {
                  Fluttertoast.showToast(msg: "Posting Gagal");
                }
                if (state is StatusSukses) {
                  setState(() {
                    if (dataList.status == 0) {
                      dataList.status = 1;
                    } else {
                      dataList.status = 0;
                    }
                  });
                  Fluttertoast.showToast(msg: "Success edit status");
                  print("sini woy");
                }
                if (state is EditWaiting) {
                  return CircularProgressIndicator();
                }
                if(state is DeleteWaiting){
                  Fluttertoast.showToast(msg: "Loading...");
                }
              },
              child: BlocBuilder<FeedProfileBloc, FeedProfileState>(
                bloc: _postBloc,
                builder: (context, state) {
                  if (state is EditSukses) {
                    return buildIconButtonPost(_postBloc);
                  }
                  if (state is EditError) {
                    Fluttertoast.showToast(msg: "koneksi gagal");
                  }
                  if (state is EditWaiting) {
                    return CircularProgressIndicator();
                  }
                  return buildIconButtonPost(_postBloc);
                },
              ),
            )
          ]),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                          height: MediaQuery.of(context).size.height / 7,
                          child: (imageFiles == null)
                              ? Image.memory(
                                  dataList.gambarBase64,
                                  width: 110,
                                  height: 50,
                                )
                              : Image.file(
                                  imageFiles,
                                  width: 110,
                                  height: 50,
                                )),
                      (imageFiles == null)
                          ? RaisedButton(
                              onPressed: () {
                                _openGalery();
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                // decoration: const BoxDecoration(
                                //   color: Colors.white,
                                //   borderRadius: BorderRadius.all(
                                //     Radius.circular(20.0),
                                //   ),
                                // ),
                                child: Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.all(5.0),
                                        child: Icon(
                                            Icons.create_new_folder_outlined,
                                            color: Colors.grey)),
                                    Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 50.0, minHeight: 30.0),
                                      alignment: Alignment.center,
                                      child: const Text(
                                        'Pilih File',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : RaisedButton(
                              onPressed: () {
                                setState(() {
                                  imageFiles = null;
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                // decoration: const BoxDecoration(
                                //   color: Colors.white,
                                //   borderRadius: BorderRadius.all(
                                //     Radius.circular(20.0),
                                //   ),
                                // ),
                                child: Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.all(5.0),
                                        child: Icon(Icons.delete,
                                            color: Colors.red)),
                                    Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 50.0, minHeight: 30.0),
                                      alignment: Alignment.center,
                                      child: const Text(
                                        'Hapus Foto',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
                Padding(padding: EdgeInsets.all(6.0)),
                Flexible(
                    flex: 2,
                    child: TextField(
                      controller: _caption,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(hintText: "Caption ...."),
                    )),
              ],
            ),
            Flexible(
                flex: 2,
                child: TextField(
                  controller: _judul,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(hintText: "Judul ...."),
                )),
            Flexible(
                flex: 2,
                child: TextField(
                  controller: _subjudul,
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(hintText: "Subjudul ...."),
                )),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                      flex: 1,
                      child: Text(
                        "Status Posting",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                  Flexible(
                    flex: 2,
                    child: ToggleSwitch(
                      minWidth: 90.0,
                      initialLabelIndex: dataList.status,
                      cornerRadius: 20.0,
                      activeFgColor: Colors.white,
                      inactiveBgColor: Colors.grey,
                      inactiveFgColor: Colors.white,
                      labels: ['Hidden', 'Posting'],
                      icons: [
                        Icons.stop_screen_share,
                        Icons.mobile_screen_share_outlined
                      ],
                      activeBgColors: [Colors.pink, Colors.blue],
                      onToggle: (index) {
                        print('switched to: $index');
                        _postBloc
                          ..add(SetStatus(
                              idPost: dataList.id,
                              indexStatus: indexTo,
                              valueStatus: index));
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showAlertHapus(context, _postBloc);
        },
        child: Icon(Icons.delete),
        backgroundColor: Colors.red,
      ),
    );
  }

  IconButton buildIconButtonPost(FeedProfileBloc _postBloc) {
    return IconButton(
      icon: Icon(Icons.send),
      onPressed: () {
        print("page");
        CloningPosting postData = CloningPosting(
            caption: _caption.text.toString(),
            judul: _judul.text.toString(),
            subjudul: _subjudul.text.toString(),
            id: dataList.id);
        _postBloc.add(EditFeedGalery(dataFeed: postData, gambar64: imageFiles));
        Container(width: 50, child: CircularProgressIndicator());
      },
    );
  }

  Future<void> _showAlertHapus(BuildContext context, _postBloc) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentHapus(context, _postBloc),
          );
        });
  }

  dialogContentHapus(BuildContext context, _postBloc) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                margin: EdgeInsets.all(10),
                child: Text("Apakah sudah yakin ingin menghapus postingan?"),
              )
            ]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    _showAlertLoading(context);
                    return _postBloc..add(DeletePosting(idPost: dataList.id));
                  },
                  child: Row(
                    children: [
                      Icon(Icons.delete, color: Colors.redAccent),
                      Text("Hapus")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showAlertLoading(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentLoadingHapus(context),
          );
        });
  }

  dialogContentLoadingHapus(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Center(child: Container(child: CircularProgressIndicator()),)
          ],
        ),
      ),
    );
  }
}
