import 'dart:typed_data';

import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/Pages/Widget/Profil/edit_post_foto.dart';
import 'package:dataAlumni/Pages/Widget/Profil/edit_post_ytb.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:like_button/like_button.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class ShowDialogInProfil extends StatefulWidget {
  final Uint8List gambar64;
  final CloningPosting data;
  final int indexTo;

  ShowDialogInProfil(
      {@required this.data, this.gambar64, @required this.indexTo});
  @override
  _ShowDialogInProfilState createState() => _ShowDialogInProfilState(
      data: this.data, gambar64: this.gambar64, indexTo: this.indexTo);
}

class _ShowDialogInProfilState extends State<ShowDialogInProfil> {
  final Uint8List gambar64;
  final CloningPosting data;
  final bloc = FeedProfileBloc();
  final int indexTo;

  _ShowDialogInProfilState(
      {@required this.data, this.gambar64, @required this.indexTo});

  @override
  Widget build(BuildContext context) {
    return dialogContent(context);
  }

  dialogContent(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          (data.url == null || data.url == "")
              ? Container(
                  margin: EdgeInsets.only(top: 0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Image.memory(
                    gambar64,
                    cacheHeight: 250,
                    cacheWidth: 450,
                  ))
              : Container(
                  margin: EdgeInsets.only(top: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: YoutubePlayer(
                    // key: ObjectKey(_controller),
                    controller: new YoutubePlayerController(
                        initialVideoId: YoutubePlayer.convertUrlToId(data.url),
                        flags: YoutubePlayerFlags(autoPlay: false)),
                    actionsPadding: const EdgeInsets.only(left: 16.0),
                    bottomActions: [
                      CurrentPosition(),
                      const SizedBox(width: 10.0),
                      ProgressBar(isExpanded: true),
                      const SizedBox(width: 10.0),
                      RemainingDuration(),
                      FullScreenButton(),
                    ],
                  )),
          Container(
            margin: EdgeInsets.only(left: 15, top: 20),
            child: Row(
              children: <Widget>[
                BlocListener<FeedProfileBloc, FeedProfileState>(
                  bloc: bloc,
                  listener: (context, state) {
                    print(state);
                    print("cobastate");
                    print(data.love);
                    if (state is FeedSuccessLoad) {
                      if (state.statusLike == true) {
                        setState(() {
                          data.cekLove = true;
                          data.love += 1;
                          state.indLike = null;
                        });
                        print("berhasil true");
                      } else if (state.statusUnLike == true) {
                        print("gagal");
                        setState(() {
                          print(data.love);
                          data.cekLove = false;
                          data.love -= 1;
                          state.indLike = null;
                        });
                      }
                    }
                    if (state is FeedProfilError) {
                      Fluttertoast.showToast(msg: "koneksi gagal");
                    }
                  },
                  child: Container(
                    child: buildLikeButton(),
                  ),
                ),
                SizedBox(width: 10),
                InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Feed_Komentar(
                              data: data, gambar64: data.gambarBase64),
                        ),
                      );
                    },
                    child: Icon(Icons.comment, color: Colors.grey)),
                SizedBox(width: 4),
                (data.jumkomentar == null)
                    ? Text(
                        "0",
                        style: TextStyle(color: Colors.grey),
                      )
                    : Text(
                        data.jumkomentar.toString(),
                        style: TextStyle(color: Colors.grey),
                      ),
                SizedBox(width: 10),
                InkWell(
                    onTap: () {
                      if (data.url == null || data.url == "") {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                PostGaleryProfil(dataList: data, indexTo: indexTo),
                          ),
                        );
                      } else {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                PostYtbProfil(dataList: data, indexTo: indexTo),
                          ),
                        );
                      }
                    },
                    child: Icon(Icons.edit_outlined, color: Colors.grey)),
                SizedBox(width: 4),
              ],
            ),
          ),
          SizedBox(height: 10),
          // Container(
          //   margin: EdgeInsets.only(left: 20, right: 15, top: 10),
          //   child: show
          //       ? Text(
          //           data.caption,
          //           textAlign: TextAlign.justify,
          //         )
          //       : Row(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           children: [
          //             Flexible(
          //               flex: 2,
          //               child: Text(
          //                 (data.caption.length < 50)
          //                     ? data.caption
          //                     : data.caption.substring(0, 50) + "....",
          //                 textAlign: TextAlign.justify,
          //               ),
          //             ),
          //           ],
          //         ),
          // ),
          // (data.caption.length < 50)
          //     ? Text("")
          //     : Container(
          //         margin: EdgeInsets.only(right: 15, bottom: 20),
          //         child: InkWell(
          //           onTap: () {
          //             setState(() {
          //               show = !show;
          //             });
          //             print(show);
          //           },
          //           child: Row(
          //             mainAxisAlignment: MainAxisAlignment.end,
          //             children: <Widget>[
          //               show
          //                   ? Text(
          //                       "Less",
          //                       style: TextStyle(color: Colors.blue),
          //                     )
          //                   : Text("More", style: TextStyle(color: Colors.blue))
          //             ],
          //           ),
          //         ),
          //       ),
        ],
      ),
    );
  }

  LikeButton buildLikeButton() {
    return LikeButton(
      isLiked: data.cekLove,
      // size of the button
      // the total number of count
      likeCount: data.love,
      // to handle the count behaviour
      countBuilder: (int count, bool isLiked, String text) {
        // color of the count
        final ColorSwatch<int> color =
            isLiked ? Colors.pinkAccent : Colors.grey;
        Widget result;
        if (count == 0) {
          result = Text(
            '0',
            style: TextStyle(color: color),
          );
        } else
          result = Text(
            count >= 1000 ? (count / 1000.0).toStringAsFixed(1) + 'k' : text,
            style: TextStyle(color: color),
          );
        return result;
      },
      // position of the count you want to show(i.e. TOP,BOTTOM,RIGHT,LEFT)
      countPostion: CountPostion.right,
      // Animation for the count
      // only the last digit will change
      onTap: onLikeButtonTapped,
    );
  }

  buildButtonLike(isLiked) {
    if (isLiked == false) {
      bloc..add(PostProfileLiked(idPostingan: data.id, likeIndex: indexTo));
    } else if (isLiked == true) {
      bloc..add(UnProfileLiked(idPostingan: data.id, likeIndex: indexTo));
    }
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    /// send your request here
    final bool success = await buildButtonLike(isLiked);

    /// if failed, you can do nothing
    // return success? !isLiked:isLiked;
    return !isLiked;
  }
}
