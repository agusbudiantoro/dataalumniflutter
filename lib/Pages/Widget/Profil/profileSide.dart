import 'dart:convert';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/page_olahInformasi.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/page_olahproper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator/T10.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../login.dart';

class T10Drawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return T10DrawerState();
  }
}

class T10DrawerState extends State<T10Drawer> {
  String namaUser;
  String email;
  String namaLemdik;
  int myUserId;
  var selectedItem = -1;
  Uint8List dataFoto;

  @override
  void initState() {
    super.initState();
    ambilGambat();
    getFromSharedPreferences();
  }

  ambilGambat() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    dataFoto = base64Decode(preferences.getString("fotoProfil"));
  }

  _exitAkun(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt("iduser", null);
    await preferences.setString("token", "");
    await preferences.setString("namaUser", "kosong");
    await preferences.setString("namaLemdik", "kosong");
    await preferences.setString("email", "kosong");
    await preferences.setString("refToken", "");
    await preferences.setString("fotoProfil", "");
    Navigator.of(context).pop(true);
    return Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => FoodSignIn(tanda:true)),
    );
  }

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      email = prefs.getString("email");
      namaUser = prefs.getString("namaUser");
      namaLemdik = prefs.getString("namaLemdik");
      myUserId = prefs.getInt("iduser");
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.85,
      height: MediaQuery.of(context).size.height,
      child: Drawer(
        elevation: 8,
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: t10_white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        backgroundImage:
                            MemoryImage(dataFoto),
                        radius: MediaQuery.of(context).size.width * 0.15,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      text("$namaUser",
                          fontFamily: fontBold, textColor: Colors.black),
                      // text("$email", textColor: Colors.black),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                olahPendidikan(4),
                mView(),
                olahKarir(5),
                mView(),
                olahOrganisasi(5),
                mView(),
                olahProper(5),
                mView(),
                getDrawerItem(theme10_lbl_logout, 3),
                mView(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget olahProper(int pos) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => OlahProper(idUserKu: myUserId,)),
        );
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: selectedItem == pos ? t10_colorPrimary_light : t10_white,
          child: Row(
            children: <Widget>[
              Container(
                width: 4,
                height: MediaQuery.of(context).size.width * 0.13,
                color: selectedItem == pos ? t10_colorPrimary : t10_white,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: text("Olah Proper",
                    textColor: selectedItem == pos
                        ? t10_textColorPrimary
                        : t10_textColorSecondary,
                    fontSize: textSizeLargeMedium,
                    fontFamily: fontMedium),
              )
            ],
          )),
    );
  }

  Widget olahOrganisasi(int pos) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => T1Listing(posInd: 2)),
        );
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: selectedItem == pos ? t10_colorPrimary_light : t10_white,
          child: Row(
            children: <Widget>[
              Container(
                width: 4,
                height: MediaQuery.of(context).size.width * 0.13,
                color: selectedItem == pos ? t10_colorPrimary : t10_white,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: text("Olah Organisasi",
                    textColor: selectedItem == pos
                        ? t10_textColorPrimary
                        : t10_textColorSecondary,
                    fontSize: textSizeLargeMedium,
                    fontFamily: fontMedium),
              )
            ],
          )),
    );
  }

  Widget olahKarir(int pos) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => T1Listing(posInd: 1,)),
        );
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: selectedItem == pos ? t10_colorPrimary_light : t10_white,
          child: Row(
            children: <Widget>[
              Container(
                width: 4,
                height: MediaQuery.of(context).size.width * 0.13,
                color: selectedItem == pos ? t10_colorPrimary : t10_white,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: text("Olah Karir",
                    textColor: selectedItem == pos
                        ? t10_textColorPrimary
                        : t10_textColorSecondary,
                    fontSize: textSizeLargeMedium,
                    fontFamily: fontMedium),
              )
            ],
          )),
    );
  }

  Widget olahPendidikan(int pos) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => T1Listing(posInd: 0,)),
        );
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: selectedItem == pos ? t10_colorPrimary_light : t10_white,
          child: Row(
            children: <Widget>[
              Container(
                width: 4,
                height: MediaQuery.of(context).size.width * 0.13,
                color: selectedItem == pos ? t10_colorPrimary : t10_white,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: text("Olah Pendidikan",
                    textColor: selectedItem == pos
                        ? t10_textColorPrimary
                        : t10_textColorSecondary,
                    fontSize: textSizeLargeMedium,
                    fontFamily: fontMedium),
              )
            ],
          )),
    );
  }

  Widget getDrawerItem(String name, int pos) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _exitAkun(context);
        });
      },
      child: Container(
          width: MediaQuery.of(context).size.width,
          color: selectedItem == pos ? t10_colorPrimary_light : t10_white,
          child: Row(
            children: <Widget>[
              Container(
                width: 4,
                height: MediaQuery.of(context).size.width * 0.13,
                color: selectedItem == pos ? t10_colorPrimary : t10_white,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: text(name,
                    textColor: selectedItem == pos
                        ? t10_textColorPrimary
                        : t10_textColorSecondary,
                    fontSize: textSizeLargeMedium,
                    fontFamily: fontMedium),
              )
            ],
          )),
    );
  }

  Widget mView() {
    return Divider(
      color: t10_view_color,
      height: 1,
    );
  }
}
