import 'dart:io';

import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuProfile/tambah_alumni/tambahalumni_bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class PostAlumni extends StatefulWidget {

  @override
  _PostAlumniState createState() => _PostAlumniState();
}

class _PostAlumniState extends State<PostAlumni> {
  bool statusSend = false;
  final TextEditingController _agama = TextEditingController();
  final TextEditingController _alamatKantor = TextEditingController();
  final TextEditingController _asalInstansi = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _expertise = TextEditingController();
  final TextEditingController _gol = TextEditingController();
  final TextEditingController _jabatan = TextEditingController();
  final TextEditingController _jenisKelamin = TextEditingController();
  final TextEditingController _nama = TextEditingController();
  final TextEditingController _noTelp = TextEditingController();
  final TextEditingController _nomorId = TextEditingController();
  final TextEditingController _nomorKra = TextEditingController();
  final TextEditingController _organisasi = TextEditingController();
  final TextEditingController _pangkat = TextEditingController();
  final TextEditingController _tempatLahir = TextEditingController();
  List _listGender = ["Pria", "Wanita"];
  String _valGender;
  @override
  Widget build(BuildContext context) {
    final _postBloc = TambahalumniBloc();
    return Scaffold(
        appBar: GradientAppBar(
            backgroundColorStart: Color(0xFF398AE5),
            backgroundColorEnd: Color(0xFF61A4F1),
            title: Text('Tambah Data Alumni'),
            leading: Container(
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => Home()),
                  // );
                },
                child: Icon(Icons.arrow_back_ios, color: Colors.white),
              ),
            ),
            actions: <Widget>[
              BlocListener<TambahalumniBloc, TambahalumniState>(
                bloc: _postBloc,
                listener: (context, state) {
                  print("inistate");
                  print(state);
                  if (state is TambahAlumniSukses) {
                    Fluttertoast.showToast(msg: "Success Tambah Data");
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home(indexTo: 4)));
                  }
                  if (state is TambahAlumniError) {
                    Fluttertoast.showToast(msg: state.errorMessage);
                  }
                  if (state is TambahAlumniWaiting) {
                    return Container(
                      child: Container(width: 50,child: CircularProgressIndicator()),
                    );
                  }
                },
                // child: (statusSend == false) ? buildIconButtonPost(_postBloc) : Container(width: 50,child: CircularProgressIndicator()),
                child: BlocBuilder<TambahalumniBloc, TambahalumniState>(
                  bloc: _postBloc,
                  builder: (context, state) {
                    if (state is TambahAlumniError) {
                      Fluttertoast.showToast(msg: "koneksi gagal");
                    }
                    if(state is TambahAlumniWaiting){
                      return Container(width: 50,child: CircularProgressIndicator());
                    }
                    return buildIconButtonPost(_postBloc);
                  },
                ),
              )
            ]),
        body: Container(
          padding: EdgeInsets.all(20),
                      child: ListView(
              children: [
                Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _agama,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Agama ...",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Agama",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _nama,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Nama ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Nama",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 2,
                    child: DropdownButton(
                      hint: Text("Jenis Kelamin"),
                      value: _valGender,
                      items: _listGender.map((value) {
                        return DropdownMenuItem(
                          child: Text(value),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _valGender = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height:10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _noTelp,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Nomor Telpon ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Nomor Telpon",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _nomorId,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "NIP/NRP/NIK ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "NIP/NRP/NIK",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _nomorKra,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Nomor KRA ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Nomor KRA",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _alamatKantor,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Alamat Kantor ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Alamat Kantor",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _asalInstansi,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Asal Instansi ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Asal Instansi",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _email,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Email ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Email",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _expertise,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Expertise ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Expertise",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _gol,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Gol ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Gol",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _jabatan,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Jabatan ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Jabatan",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),              
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _pangkat,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Pangkat ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Pangkat",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _tempatLahir,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                            hintText: "Tempat Lahir ....",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            hintStyle: TextStyle(color: Colors.black),
                            labelText: "Tempat Lahir",
                            labelStyle: TextStyle(color: Colors.black)),
                      )),
                ],
              ),
              ],
            ),
        ));
  }

  IconButton buildIconButtonPost(TambahalumniBloc _postBloc) {
    return IconButton(
      icon: Icon(Icons.send),
      onPressed: () {
        print("page");
        DataAlumni postData =
            DataAlumni(agama: _agama.text.toString(), alamatKantor: _alamatKantor.text.toString(), asalInstansi: _asalInstansi.text.toString(), email: _email.text.toString(), expertise: _expertise.text.toString(), gol: _gol.text.toString(), jabatan: _jabatan.text.toString(), jenisKelamin: _valGender.toString(), nama: _nama.text.toString(), noTelp: _noTelp.text.toString(), nomorId: _nomorId.text.toString(), nomorKra: _nomorKra.text.toString(),organisasi: _organisasi.text.toString(), pangkat: _pangkat.text.toString(), tempatLahir: _tempatLahir.text.toString());
        _postBloc.add(TambahAlumniProfil(data: postData));
        Container(width: 50,child: CircularProgressIndicator());
      },
    );
  }
}
