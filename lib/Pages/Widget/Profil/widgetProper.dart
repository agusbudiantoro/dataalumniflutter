import 'package:dataAlumni/bloc/ProperById/properbyid_bloc.dart';
import 'package:dataAlumni/model/modeProper.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListProper extends StatefulWidget {
  final int idUserByDataAlumni;

  ListProper({this.idUserByDataAlumni});
  @override
  _ListProperState createState() => _ListProperState();
}

class _ListProperState extends State<ListProper> {
  final ProperbyidBloc blocProper = ProperbyidBloc();
  int idUser;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFromSharedPreferences();
  }

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      idUser = prefs.getInt("iduser");
    });
    if(widget.idUserByDataAlumni == null){
      blocProper.add(GetProperById(id: idUser));
    } else {
      blocProper.add(GetProperById(id: widget.idUserByDataAlumni));
    }
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocListener<ProperbyidBloc, ProperbyidState>(
        bloc: blocProper,
        listener: (context, state) {
          if (state is GetProperSukses) {
            print("sukses ambil data proper");

            // print(listDataProperNow.length);
            print("sukses ambil data proper2");
          }
        },
        child: Container(
            child: BlocBuilder<ProperbyidBloc, ProperbyidState>(
          bloc: blocProper,
          builder: (context, state) {
            if (state is GetProperSukses) {
              // Fluttertoast.showToast(msg: "ambil data proper sukses");
              print("sukses ambil data proper builder");
              // print(listDataProperNow.length);
              print(state.listProper.length);
              return _container(state);
            }
            if(state is GetProperWaiting){
              return _loadingList();
            }
            return _loadingList();
          },
        )),
      ),
    );
  }

  Widget _loadingList() {
    return Column(
      children: <Widget>[
        // TopBar(t1_Listing, true),
        Container(
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: 2,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                // return Text(model.listProper[index].id.toString());
                return Shimmer.fromColors(
                  baseColor: Colors.grey[400],
                  highlightColor: Colors.grey[100],
                  child: scrollProperLoading()
                );
              }),
        )
      ],
    );
  }

  Widget scrollProperLoading() {
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Container(
          decoration: boxDecoration(radius: 10, showShadow: true),
          child: Column(
            children: [
              Stack(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(16), child: _columnLoading()),
                  Container(
                    width: 4,
                    height: 35,
                    margin: EdgeInsets.only(top: 16),
                    // color: t1TextColorPrimary,
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  Widget _columnLoading() {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Judul"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text("",
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Abstrak"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text("",
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Kata Kunci"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text("",
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Evidence:"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text("",
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Tahun"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text("",
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
      ],
    );
  }

  Widget _container(GetProperSukses model) {
    // return Container(
    return Column(
      children: <Widget>[
        // TopBar(t1_Listing, true),
        Container(
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: model.listProper.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                // return Text(model.listProper[index].id.toString());
                return scrollProper(context, model, index);
              }),
        )
      ],
    );
    // );
  }

  Widget scrollProper(BuildContext context, GetProperSukses model, pos) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Container(
          decoration: boxDecoration(radius: 10, showShadow: true),
          child: Column(
            children: [
              Stack(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(16), child: _column(model, pos)),
                  Container(
                    width: 4,
                    height: 35,
                    margin: EdgeInsets.only(top: 16),
                    color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  Widget _column(GetProperSukses model, pos) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Judul"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.listProper[pos].judul.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Abstrak"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.listProper[pos].abstrak.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Kata Kunci"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.listProper[pos].kataKunci.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Evidence:"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.listProper[pos].evidence.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Tahun"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.listProper[pos].tahun.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
      ],
    );
  }
}
