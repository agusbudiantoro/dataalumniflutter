import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/InformasiUser/informasi_user_bloc.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/Widget/widgetCardOpen.dart';
import 'package:dataAlumni/util/model/informasiUser.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nb_utils/nb_utils.dart';

class CardOpen extends StatefulWidget {
  final ModelInfo nama;
  final int indexTo;
  final int myId;
  CardOpen({@required this.nama, @required this.indexTo,this.myId});
  @override
  _CardOpenState createState() =>
      _CardOpenState(nama: this.nama, indexTo: this.indexTo, myId: this.myId);
}

class _CardOpenState extends State<CardOpen> {
  final int indexTo;
  final int myId;
  final _bloc = InformasiUserBloc();
  final ModelInfo nama;
  _CardOpenState({@required this.nama, @required this.indexTo, this.myId});
  final _foldingCellKey = GlobalKey<SimpleFoldingCellState>();
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      child: SimpleFoldingCell(
          key: _foldingCellKey,
          frontWidget: _buildFrontWidget(),
          innerTopWidget: _buildInnerTopWidget(),
          innerBottomWidget: BlocBuilder<InformasiUserBloc, InformasiUserState>(
            bloc: _bloc,
            builder: (context, state) {
              if (state is GetInformasiSukses) {
                return _buildInnerBottomWidget(width, state);
              }
              if (state is GetInformasiWaiting) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
              if (state is GetInformasiError) {
                Fluttertoast.showToast(msg: "gagal");
              }
              if (state is InformasiUserInitial) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
            },
          ),
          cellSize: Size(MediaQuery.of(context).size.width, 225),
          padding: EdgeInsets.all(15),
          animationDuration: Duration(milliseconds: 300),
          borderRadius: 10,
          onOpen: () => _bloc.add(InformasiUserEvent(idUser: myId,urutan: indexTo)),
          onClose: () => print('cell closed')),
    );
  }

  Widget _buildFrontWidget() {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            text(nama.name,
                fontSize: 20.0,
                textColor: Colors.white,
                fontFamily: 'OpenSans'),
            FlatButton(
              onPressed: () {
                _foldingCellKey?.currentState?.toggleFold();
              },
              child: text(
                "Open",
                textColor: Colors.white,
              ),
              color: Colors.indigoAccent,
              splashColor: Colors.white.withOpacity(0.5),
            )
          ],
        ));
  }

  Widget _buildInnerTopWidget() {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(nama.name,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'OpenSans',
                    fontSize: 20.0,
                    fontWeight: FontWeight.w800)),
            FlatButton(
              onPressed: () => _foldingCellKey?.currentState?.toggleFold(),
              child: text(
                "Close",
              ),
              textColor: Colors.white,
              color: Colors.indigoAccent,
              splashColor: Colors.white.withOpacity(0.5),
            ),
          ],
        ));
  }

  Widget _buildInnerBottomWidget(width, GetInformasiSukses data) {
    return Container(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: (data.indexInformasi == 0)
              ? data.dataPendidikan.length
              : (data.indexInformasi == 1)
                  ? data.dataKarir.length
                  : (data.indexInformasi == 2)
                      ? data.dataOrganisasi.length
                      : 0,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
                color: Color(0xFFecf2f9),
                alignment: Alignment.topLeft,
                child: Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Column(
                      children: [
                        SizedBox(
                          width: width,
                          height: 30,
                          child: Container(
                            color: Colors.black,
                            child: (data.indexInformasi == 0)
                                ? Center(
                                    child: Text(data.inisialIndex.toString(), style: TextStyle(color: Colors.white),))
                                : (data.indexInformasi == 1)
                                    ? Center(
                                        child:
                                            Text(data.inisialIndex.toString(),style: TextStyle(color: Colors.white)))
                                    : (data.indexInformasi == 2)
                                        ? Center(
                                            child: Text(
                                                data.inisialIndex.toString(),style: TextStyle(color: Colors.white)))
                                        : Text(""),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                    child: Center(
                                        child: (data.indexInformasi == 0)
                                            ? Text("Nama Univ")
                                            : (data.indexInformasi == 1)
                                                ? Text("Posisi")
                                                : (data.indexInformasi == 2)
                                                    ? Text("Bidang Organisasi")
                                                    : Text(""))),
                              ),
                              Container(
                                height: width * 0.1,
                                width: 0.5,
                                color: t10_view_color,
                                margin: EdgeInsets.only(
                                    left: spacing_standard_new,
                                    right: spacing_standard_new),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                    child: (data.indexInformasi == 0)
                                        ? Text(
                                            data.dataPendidikan[index].namauniv)
                                        : (data.indexInformasi == 1)
                                            ? Text(data.dataKarir[index].posisi)
                                            : (data.indexInformasi == 2)
                                                ? Text(data
                                                    .dataOrganisasi[index]
                                                    .bidangOrganisasi)
                                                : Text("")),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                    child: Center(
                                        child: (data.indexInformasi == 0)
                                            ? Text("Jenjang")
                                            : (data.indexInformasi == 1)
                                                ? Text("Tempat")
                                                : (data.indexInformasi == 2)
                                                    ? Text("Nama Organisasi")
                                                    : Text(""))),
                              ),
                              Container(
                                height: width * 0.1,
                                width: 0.5,
                                color: t10_view_color,
                                margin: EdgeInsets.only(
                                    left: spacing_standard_new,
                                    right: spacing_standard_new),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                    child: (data.indexInformasi == 0)
                                        ? Text(
                                            data.dataPendidikan[index].jenjang)
                                        : (data.indexInformasi == 1)
                                            ? Text(data.dataKarir[index].tempat)
                                            : (data.indexInformasi == 2)
                                                ? Text(data
                                                    .dataOrganisasi[index]
                                                    .namaOrganisasi)
                                                : Text("")),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  child: Center(
                                      child: (data.indexInformasi == 0)
                                          ? Text("Lokasi")
                                          : (data.indexInformasi == 1)
                                              ? Text("Tahun")
                                              : (data.indexInformasi == 2)
                                                  ? Text("Jabatan")
                                                  : Text("")),
                                ),
                              ),
                              Container(
                                height: width * 0.1,
                                width: 0.5,
                                color: t10_view_color,
                                margin: EdgeInsets.only(
                                    left: spacing_standard_new,
                                    right: spacing_standard_new),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                    child: (data.indexInformasi == 0)
                                        ? Text(
                                            data.dataPendidikan[index].lokasi)
                                        : (data.indexInformasi == 1)
                                            ? Text(data.dataKarir[index].tahun)
                                            : (data.indexInformasi == 2)
                                                ? Text(data
                                                    .dataOrganisasi[index]
                                                    .jabatan)
                                                : Text("")),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(5),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                    child: Center(
                                        child: (data.indexInformasi == 0)
                                            ? Text("Tahun")
                                            : (data.indexInformasi == 2)
                                                ? Text("Tahun")
                                                : Text(""))),
                              ),
                              Container(
                                height: width * 0.1,
                                width: 0.5,
                                color: t10_view_color,
                                margin: EdgeInsets.only(
                                    left: spacing_standard_new,
                                    right: spacing_standard_new),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                    child: (data.indexInformasi == 0)
                                        ? Text(data.dataPendidikan[index].tahun)
                                        : (data.indexInformasi == 2)
                                            ? Text(data
                                                .dataOrganisasi[index].tahun)
                                            : Text("")),
                              ),
                            ],
                          ),
                        )
                      ],
                    )));
          }),
    );
  }
}
