// import 'dart:html';

import 'dart:convert';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/childKomentarBloc/childkomentar_bloc.dart';
import 'package:dataAlumni/model/feed/komentar_model.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/Widget/boxKomen.dart';
import 'package:dataAlumni/util/model/Dashed.dart';
import 'package:dataAlumni/util/model/Social_model.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Komen_Widget extends StatefulWidget {
  final KomentarModel model;
  final int pos;
  final String tanggalPost;
  final VoidCallback balesan;

  Komen_Widget({this.model, this.pos, this.tanggalPost, this.balesan});
  @override
  _Komen_WidgetState createState() => _Komen_WidgetState(
      model: this.model,
      pos: this.pos,
      tanggalPost: this.tanggalPost,
      balesan: this.balesan);
}

class _Komen_WidgetState extends State<Komen_Widget> {
  _Komen_WidgetState({this.model, this.pos, this.tanggalPost, this.balesan});
  final KomentarModel model;
  final int pos;
  final String tanggalPost;
  final VoidCallback balesan;
  final _bloc = ChildKomentarBloc();
  var updateOn;
  final TextEditingController _isiKomentar = TextEditingController();
  String komentarNama;
  Uint8List dataFoto;

  List<Feeds> feed1 = [
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user1.png',
      name: 'John Doe',
      feedImage: '$BaseUrl/images/food/food_ic_popular2.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user2.png',
      name: 'Carry Milton',
      feedImage: '$BaseUrl/images/food/food_ic_popular3.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user3.png',
      name: 'Jhonny Smith',
      feedImage: '$BaseUrl/images/food/food_ic_popular1.jpg',
    ),
  ];

  @override
  void initState() {
    super.initState();
    ambilGambat();
  }

  ambilGambat() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    dataFoto = base64Decode(preferences.getString("fotoProfil"));
  }

  void replyFungsi(id, nama) {
    setState(() {
      reply = !reply;
      komentarNama = "$nama";
      print(komentarNama);
      if (reply == true) {
        showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            builder: (context) => SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: TextFieldKomen(id, nama),
              ),
            ),
          );
      }
    });

    print(reply);
  }

  bool reply = false;
  bool showBalasan = false;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    double c_width = MediaQuery.of(context).size.width * 0.7;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 1.0),
      child: Container(
        margin: EdgeInsets.only(bottom: spacing_standard_new),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    DashedCircle(
                      dashes: 0,
                      child: Padding(
                          padding: EdgeInsets.only(
                              bottom: 10.0, left: 10.0, right: 10.0, top: 0),
                          child: CircleAvatar(
                            radius: 23.0,
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: (model.fotoBase64 == null) ? CachedNetworkImageProvider(social_ic_user2) : MemoryImage(model.fotoBase64),
                                ),
                              ),
                            ),
                          )),
                      color: social_colorPrimary,
                    ),
                    SizedBox(
                      width: spacing_middle,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        text(model.namakomentator.toString(),
                            fontFamily: fontMedium),
                        Container(
                          width: c_width,
                          child: Text(
                            model.isiKomentar,
                            textAlign: TextAlign.justify,
                          ),
                        ),
                        Container(
                          child: Row(
                            children: [
                              text(
                                tanggalPost.toString(),
                                textColor: t3_textColorSecondary,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              InkWell(
                                onTap: () {
                                  return replyFungsi(model.id ,model.namakomentator);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    reply
                                        ? text(
                                            "Tutup",
                                            textColor: food_colorPrimaryDark,
                                          )
                                        : text("Balas",
                                            textColor: food_colorPrimaryDark)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Center(child: buildInkWellLihatBalasan()),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            (showBalasan == true) ? blocBuilderChildKomentar() : Text(""),
          ],
        ),
      ),
    );
  }

  InkWell buildInkWellLihatBalasan() {
    return InkWell(
      onTap: () {
        setState(() {
          showBalasan = !showBalasan;
          if (showBalasan == true) {
            _bloc
              ..add(ChildKomentarBlocEvent(
                idKomentar: model.id,
              ));
          }
        });
        print(showBalasan);
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          showBalasan
              ? text(
                  "Tutup Balasan",
                  textColor: food_colorPrimaryDark,
                )
              : text("Lihat Balasan", textColor: food_colorPrimaryDark)
        ],
      ),
    );
  }

  BlocBuilder<ChildKomentarBloc, ChildKomentarBlocState>
      blocBuilderChildKomentar() {
    return BlocBuilder<ChildKomentarBloc, ChildKomentarBlocState>(
      bloc: _bloc,
      builder: (context, state) {
        if (state is GetChildKomentarBlocInitial) {
          print("stateinisial");
          print(state);
          return CircularProgressIndicator();
        }
        if (state is GetChildKomentarLoadState) {
          print("statesuksessl");
          if (state.listChildKomentar.length != 0) {
            return listKomenBalasan(state);
          } else {
            return Container(
              child: Text("Tidak Ada Balasan"),
            );
          }
          // return listKomenBalasan(state);
        }
        if (state is GetChildKomentarLoadErrorState) {
          Fluttertoast.showToast(msg: state.errorMessage);
          Fluttertoast.showToast(msg: "Memuat data Gagal");
        }
        print("else");
        print(state);
        return CircularProgressIndicator();
      },
    );
  }

  Widget listKomenBalasan(GetChildKomentarLoadState state) {
    // controllerr.addListener(autoScroll);
    // return Expanded(
    //   child: SingleChildScrollView(
    //     // controller: listController,
    //     child: Container(
    //       decoration: boxDecorationKomen(showShadow: true),
    //       padding: EdgeInsets.all(spacing_standard_new),
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: state.listChildKomentar.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          DateTime todayDate =
              DateTime.parse(state.listChildKomentar[index].updatedOn);
          var updateOn = formatDate(todayDate.toLocal(),
              [yyyy, '/', mm, '/', dd, ' ', hh, ':', nn, ':', ss, ' ', am]);
          return listKomenBalasanPrint(updateOn, index, state);
        });
    //     ),
    //   ),
    // );
  }

  Widget listKomenBalasanPrint(tgl, indexKe, GetChildKomentarLoadState isi) {
    double c_widthChild = MediaQuery.of(context).size.width * 0.5;
    return Container(
      margin: EdgeInsets.only(bottom: spacing_standard_new),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              DashedCircle(
                dashes: 0,
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: CircleAvatar(
                      radius: 15.0,
                      child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: MemoryImage(isi.listChildKomentar[indexKe].fotoBase64),
                          ),
                        ),
                      ),
                    )),
                color: social_colorPrimary,
              ),
              SizedBox(
                width: spacing_middle,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  text(isi.listChildKomentar[indexKe].namakomentator.toString(),
                      fontFamily: fontMedium),
                  Container(
                    width: c_widthChild,
                    child: Text(
                      isi.listChildKomentar[indexKe].isiKomentar,
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        text(
                          tgl.toString(),
                          textColor: t3_textColorSecondary,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget TextFieldKomen(id, nama) {
    return Transform.translate(
      offset: Offset(0.0, -1 * MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        height: 85.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0, -2),
              blurRadius: 6.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: TextField(
            autofocus: true,
            controller: _isiKomentar,
            decoration: InputDecoration(
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.grey),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.grey),
              ),
              contentPadding: EdgeInsets.all(20.0),
              hintText: 'Balas ke $komentarNama',
              prefixIcon: Container(
                margin: EdgeInsets.all(4.0),
                width: 48.0,
                height: 48.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(0, 2),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: (dataFoto != null)
                    ? CircleAvatar(
                        child: ClipOval(
                          child: Image.memory(
                            dataFoto,
                            fit: BoxFit.cover,
                            width: 48.0,
                            height: 48.0,
                          ),
                        ),
                      )
                    : CircleAvatar(
                        child: ClipOval(
                          child: CachedNetworkImage(
                            height: 48.0,
                            width: 48.0,
                            imageUrl: feed1[1].profileImg,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
              ),
              suffixIcon: Container(
                  margin: EdgeInsets.only(right: 4.0),
                  width: 20.0,
                  child:
                      BlocListener<ChildKomentarBloc, ChildKomentarBlocState>(
                    bloc: _bloc,
                    listener: (context, state) {
                      if (state is GetChildKomentarLoadErrorState) {
                        Fluttertoast.showToast(msg: state.errorMessage);
                        Fluttertoast.showToast(msg: "Memuat data Gagal");
                      }
                      if (state is GetChildKomentarLoadState) {
                        print("suksesset");
                        setState(() {
                         showBalasan = true;
                         reply=false;
                         _isiKomentar.clear();
                        });
                        Fluttertoast.showToast(msg: "Success Memuat");
                      }
                      if (state is GetChildKomentarLoadWaiting) {
                        print("suksesset2");
                        setState(() {
                         showBalasan = true;
                         reply=false;
                         _isiKomentar.clear();
                        });
                        Fluttertoast.showToast(msg: "Loading data");
                      }
                    },
                    child: Container(
                      child: buildFlatButtonSendKomen(_bloc, id),
                    ),
                  )),
            ),
          ),
        ),
      ),
    );
  }

  FlatButton buildFlatButtonSendKomen(ChildKomentarBloc bloc, id) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      color: Colors.white,
      onPressed: () {
        Navigator.pop(context);
        bloc
          ..add(PostChildKomentarBlocEvent(
              idKomentar: id, isiKomentar: _isiKomentar.text));
      },
      child: Icon(
        Icons.send,
        size: 25.0,
        color: food_colorPrimaryDark,
      ),
    );
  }
}
