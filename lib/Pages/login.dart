import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Animation/FadeAnimation.dart';
import 'package:clippy_flutter/arc.dart';
import 'package:dataAlumni/Pages/register.dart';
import 'package:flutter/widgets.dart';
import 'package:dataAlumni/bloc/bloc_auth/auth_bloc.dart';
import 'package:dataAlumni/model/login_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:dataAlumni/network/api_provider.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import 'FoodCreateAccount.dart';

class FoodSignIn extends StatefulWidget {
  final bool tanda;
  static String tag = '/FoodSignIn';

  const FoodSignIn({
    this.tanda,
    Key key,
  }) : super(key: key);
  @override
  FoodSignInState createState() => FoodSignInState(tanda: this.tanda);
}

class FoodSignInState extends State<FoodSignIn> {
  bool _isLoading = false;
  bool _secure = true;
  final bool tanda;
  String showPass = "Show Password";
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _passWord = TextEditingController();
  FoodSignInState({this.tanda});
  void _toggle() {
    setState(() {
      if (_secure) {
        _secure = false;
        showPass = "Hide Password";
      } else {
        _secure = true;
        showPass = "Show Password";
      }
    });
  }

  @override
  void initState() {
    super.initState();
    statusTanda();
  }

  void statusTanda() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("tanda", tanda);
    print("sini woyy");
    print(tanda);
  }

  @override
  Widget build(BuildContext context) {
    final _loginBlocBloc = AuthBloc();
    // String username = _userName.text.toString();
    // String password = _passWord.text.toString();

    changeStatusColor(Colors.transparent);

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    // Widget mOption(var color, var icon, var value, var iconColor, valueColor) {
    //   return InkWell(
    //     onTap: () {
    //       //
    //     },
    //     child: Container(
    //       width: width,
    //       padding: EdgeInsets.all(12.0),
    //       margin: EdgeInsets.only(bottom: spacing_standard_new),
    //       decoration: boxDecoration(bgColor: color, radius: 50),
    //       child: RichText(
    //         textAlign: TextAlign.center,
    //         text: TextSpan(
    //           children: [
    //             WidgetSpan(
    //                 child: Padding(
    //                     padding: const EdgeInsets.only(right: spacing_standard),
    //                     child: SvgPicture.asset(icon,
    //                         color: iconColor, width: 18, height: 18))),
    //             TextSpan(
    //                 text: value,
    //                 style:
    //                     TextStyle(fontSize: textSizeMedium, color: valueColor)),
    //           ],
    //         ),
    //       ),
    //     ),
    //   );
    // }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: <Widget>[
          Image(
              image: AssetImage("images/dataAlumni/2910377.png"),
              height: width * 0.6,
              fit: BoxFit.fill,
              width: width),
          new Stack(
            children: [
              Positioned(
                left: 150,
                top: 0,
                width: width * 0.2,
                height: height * 0.1,
                child: FadeAnimation(
                    2,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  "images/dataAlumni/circleLeft.png"))),
                    )),
              ),
              Positioned(
                right: -10,
                top: 27,
                width: width * 0.2,
                height: height * 0.1,
                child: FadeAnimation(
                    2.3,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  "images/dataAlumni/circleRight.png"))),
                    )),
              ),
              Positioned(
                left: 10,
                top: 170,
                width: width * 0.07,
                height: height * 0.1,
                child: FadeAnimation(
                    2.5,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage("images/dataAlumni/bubble.png"))),
                    )),
              ),
              Positioned(
                left: 109,
                top: 179,
                width: width * 0.21,
                height: height * 0.05,
                child: FadeAnimation(
                    2.8,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image:
                                  AssetImage("images/dataAlumni/laptop.png"))),
                    )),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: width * 0.5),
            child: Stack(
              children: <Widget>[
                Arc(
                  arcType: ArcType.CONVEX,
                  edge: Edge.TOP,
                  height: (MediaQuery.of(context).size.width) / 10,
                  child: new Container(
                      height: (MediaQuery.of(context).size.height),
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color(0xFF73AEF5),
                              Color(0xFF61A4F1),
                              Color(0xFF478DE0),
                              Color(0xFF398AE5),
                            ],
                            stops: [0.1, 0.4, 0.7, 0.9],
                          ),
                        ),
                      )),
                ),
                // Align(
                //   alignment: Alignment.topCenter,
                //   child: FadeAnimation(
                //       1.5,
                //       Container(
                //         transform: Matrix4.translationValues(0.0, -20.0, 0.0),
                //         alignment: Alignment.center,
                //         decoration: BoxDecoration(
                //             shape: BoxShape.circle, color: food_white),
                //         width: width * 0.13,
                //         height: width * 0.13,
                //         child: Icon(Icons.arrow_forward,
                //             color: food_textColorPrimary),
                //       )),
                // ),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.all(spacing_standard_new),
                  child: Column(
                    children: <Widget>[
                      // SizedBox(height: width * 0.1),
                      FadeAnimation(
                        2.8,
                        text("LOGIN",
                            textColor: food_white,
                            fontFamily: fontBold,
                            fontSize: 30.0),
                      ),
                      FadeAnimation(
                        3.0,
                        text("SILANRI",
                            textColor: food_white,
                            fontFamily: fontBold,
                            fontSize: 20.0),
                      ),
                      FadeAnimation(
                        3.2,
                        text("(Sinergi Alumi LAN untuk Negeri)",
                            textColor: food_white,
                            fontFamily: fontBold,
                            fontSize: 15.0),
                      ),
                      // SizedBox(height: width * 0.12),
                      Padding(
                        padding: EdgeInsets.only(
                            bottom: 30.0, left: 30.0, right: 30.0, top: 20.0),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color.fromRGBO(0, 0, 100, 1),
                                        blurRadius: 20.0,
                                        offset: Offset(0, 10))
                                  ]),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.grey[100]))),
                                    child: TextField(
                                      controller: _userName,
                                      decoration: InputDecoration(
                                          suffixIcon: Icon(
                                            Icons.people,
                                            color: Colors.pink[200],
                                          ),
                                          border: InputBorder.none,
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Color(0xFF73AEF5),
                                            ),
                                          ),
                                          hintText: "Username / NIP",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[400])),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: TextField(
                                      controller: _passWord,
                                      obscureText: _secure,
                                      decoration: InputDecoration(
                                        suffixIcon: Icon(Icons.security,
                                            color: Colors.blue[200]),
                                        border: InputBorder.none,
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Color(0xFF73AEF5),
                                          ),
                                        ),
                                        hintText: "Password",
                                        hintStyle:
                                            TextStyle(color: Colors.grey[400]),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        FlatButton(
                                          onPressed: _toggle,
                                          child: new Row(
                                            children: [
                                              Text(
                                                showPass,
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                              SizedBox(width: width * 0.02),
                                              Icon(
                                                Icons.remove_red_eye,
                                                color: Colors.grey,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            BlocListener<AuthBloc, AuthState>(
                              bloc: _loginBlocBloc,
                              listener: (context, state) {
                                if (state is LoginError) {
                                  Fluttertoast.showToast(
                                      msg: state.errorMessage);
                                  Fluttertoast.showToast(
                                      msg: state.errorMessage);
                                }
                                if (state is LoginSuccess) {
                                  Fluttertoast.showToast(msg: "Success Login");
                                  Navigator.of(context).pop(true);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Home()));
                                }
                                if (state is LoginWaiting) {
                                  Fluttertoast.showToast(msg: "Login Waiting");
                                }
                              },
                              child: Container(
                                child: BlocBuilder<AuthBloc, AuthState>(
                                  bloc: _loginBlocBloc,
                                  builder: (context, state) {
                                    if (state is LoginWaiting) {
                                      return CircularProgressIndicator(
                                        backgroundColor: Colors.white,
                                      );
                                    }
                                    if (state is LoginError) {
                                      return _buttonLogin(_loginBlocBloc);
                                    }
                                    if (state is LoginSuccess) {
                                      return _buttonLogin(_loginBlocBloc);
                                    }
                                    return _buttonLogin(_loginBlocBloc);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      // SizedBox(height: width * 0.08),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                              height: 0.5,
                              color: food_white,
                              width: width * 0.06,
                              margin: EdgeInsets.only(right: spacing_standard)),
                          text("@2020 pusdatin lan all right reserved",
                              textColor: food_white,
                              textAllCaps: true,
                              fontSize: textSizeSMedium),
                          Container(
                              height: 0.5,
                              color: food_white,
                              width: width * 0.07,
                              margin: EdgeInsets.only(left: spacing_standard)),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buttonLogin(AuthBloc bloc) {
    return Container(
        child: Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          flex: 1,
          child: RaisedButton(
            onPressed: () {
              LoginModel loginRequestData = LoginModel(
                  username: _userName.text.toString(),
                  password: _passWord.text.toString());
              bloc.add(LoginEvent(
                loginModelRequest: loginRequestData,
              ));
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(80.0),
            ),
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(80.0),
                ),
              ),
              child: Container(
                constraints:
                    const BoxConstraints(minWidth: 150.0, minHeight: 36.0),
                alignment: Alignment.center,
                child: const Text(
                  'Masuk',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(width:10),
        Flexible(
          flex: 1,
          child: RaisedButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Register()));
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(80.0),
            ),
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(80.0),
                ),
              ),
              child: Container(
                constraints:
                    const BoxConstraints(minWidth: 150.0, minHeight: 36.0),
                alignment: Alignment.center,
                child: const Text(
                  'Registrasi',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    ));
  }
}
