import 'dart:io';
import 'dart:ui';

import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedGalery.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuAgenda/bloc/pengumuman_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:dataAlumni/util/constant.dart';
import 'package:image_picker/image_picker.dart';

class PagePengumuman extends StatefulWidget {
  @override
  _PagePengumumanState createState() => _PagePengumumanState();
}

class _PagePengumumanState extends State<PagePengumuman> {
  ScrollController controller = ScrollController();
  final bloc = PengumumanBloc();
  File imageFile;

  void onScroll() {
    // double maxScroll = controller.position.maxScrollExtent;
    // double currentScroll = controller.position.pixels;

    // if (currentScroll == maxScroll) {
    return bloc.add(GetPengumumanSecondEvent());
    // }
  }

  void onScrollHabis() {
    Fluttertoast.showToast(msg: "data habis");
  }

  bool show = false;
  List<Feeds> feed1 = [
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user1.png',
      name: 'John Doe',
      feedImage: '$BaseUrl/images/food/food_ic_popular2.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user2.png',
      name: 'Carry Milton',
      feedImage: '$BaseUrl/images/food/food_ic_popular3.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user3.png',
      name: 'Jhonny Smith',
      feedImage: '$BaseUrl/images/food/food_ic_popular1.jpg',
    ),
  ];
  bool isActive;
  @override
  void initState() {
    super.initState();
    isActive = true;
    print(isActive);
    init();
    print("sini");
    bloc..add(PengumumanEvent());
  }

  init() async {
    await Future.delayed(
      Duration(seconds: 3),
    );
    setState(() {
      isActive = false;
    });
  }

//   You can set the initial value of the count.
  final int likeCount = 0;

//   Set thr Size of the Button/Icons.
  final double buttonSize = 30.0;

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return bloc.add(RefreshPengumuman());
  }

  _openKamera(BuildContext context) async {
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState(() {
      imageFile = picture;
    });
    Navigator.of(context).pop();
    if (imageFile != null) {
      return Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PostGalery(imageFile: imageFile)));
    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    }
  }

  @override
  Widget build(BuildContext context) {
    // bloc = BlocProvider.of<PengumumanBloc>(context);

    double c_width = MediaQuery.of(context).size.width * 0.8;
    // controller.addListener(onScroll);
    return SafeArea(
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Text('Pengumuman'),
          leading: Container(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back, color: Colors.white),
            ),
          ),
        ),
        body: BlocListener<PengumumanBloc, PengumumanState>(
          bloc: bloc,
          listener: (context, state) {
            print(state);
            print("cobastate");
            if (state is GetPengumumanSukses) {
              print("berhasil true");
              // Fluttertoast.showToast(msg: "");
            }
            if (state is GetPengumumanError) {
              Fluttertoast.showToast(msg: "koneksi gagal");
            }
            // Fluttertoast.showToast(msg: "ini gagal");
            // return loadingStartPage();
          },
          child: Container(
            child: BlocBuilder<PengumumanBloc, PengumumanState>(
              bloc: bloc,
              builder: (context, state) {
                if (state is PengumumanInitial) {
                  return loadingStartPage();
                }
                if (state is GetPengumumanSukses) {
                  print("load");
                  return buildListView(state);
                }
                if (state is GetPengumumanError) {
                  Fluttertoast.showToast(msg: "koneksi gagal");
                  return loadingStartPage();
                }
                return loadingStartPage();
              },
            ),
          ),
        ),
      ),
    );
  }

  LazyLoadScrollView buildListView(GetPengumumanSukses data) {
    // controller.addListener(onScroll);
    return LazyLoadScrollView(
      child: RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.builder(
          itemCount: (data.hasReachMax)
              ? data.listPost.length
              : data.listPost.length + 1,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) =>
              (index < data.listPost.length)
                  ? Container(
                      // height: (data.listPost[index].showCaption == false) ? 500 : null,
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                          //                   <--- left side
                          color: Colors.grey,
                        ),
                      )),
                      margin: EdgeInsets.only(
                        top: 15,
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [Text("judul")],
                            ),
                          ),
                          Container(
                            height: 300,
                              margin: EdgeInsets.only(top: 15),
                              decoration: BoxDecoration(
                                border: Border(
                                  top: BorderSide(
                                    //                   <--- left side
                                    color: Colors.grey,
                                  ),
                                ),
                                // borderRadius: BorderRadius.circular(15),
                              ),
                              child: Image.memory(
                                data.listPost[index].gambarBase64,
                                // cacheHeight: 300,
                              )),
                          SizedBox(height: 10),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("Tanggal Pelaksanaan :"),
                                SizedBox(width: 10),
                                Text("10-sep-2020")
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          buildColumnDeskripsi(data, index),
                          // ShowCaption(data: data.listPost[index]),
                        ],
                      ),
                    )
                  : Container(
                      child: Center(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
        ),
      ),
      onEndOfPage: (data.hasReachMax == false) ? onScroll : onScrollHabis,
    );
  }

  Column buildColumnDeskripsi(GetPengumumanSukses data, int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 15),
          child: data.listPost[index].showDeskripsi
              ? Text(
                  data.listPost[index].deskripsi,
                  textAlign: TextAlign.justify,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Text(
                        (data.listPost[index].deskripsi.length < 50)
                            ? data.listPost[index].deskripsi
                            : data.listPost[index].deskripsi.substring(0, 50) +
                                '...',
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
        ),
        (data.listPost[index].deskripsi.length < 50)
            ? Text("")
            : Container(
                margin: EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      data.listPost[index].showDeskripsi =
                          !data.listPost[index].showDeskripsi;
                    });
                    print(data.listPost[index].showDeskripsi);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      data.listPost[index].showDeskripsi
                          ? Text(
                              "Less",
                              style: TextStyle(color: Colors.blue),
                            )
                          : Text("More", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  LazyLoadScrollView loadingStartPage() {
    return LazyLoadScrollView(
      onEndOfPage: () {},
      child: RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.builder(
          itemCount: feed1.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return SizedBox(
              child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      //                   <--- left side
                      color: Colors.black,
                      width: 1.0,
                    ),
                  )),
                  margin: EdgeInsets.only(
                    top: 15,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                  left: 10,
                                ),
                                height: 8,
                                color: Colors.grey,
                              ),
                            ],
                          ),
                          Container(
                            height: 10,
                            margin: EdgeInsets.only(right: 10),
                            color: Colors.grey,
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        height: 200,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    /// send your request here
    // final bool success= await sendRequest();

    /// if failed, you can do nothing
    // return success? !isLiked:isLiked;

    return !isLiked;
  }
}

class Feeds {
  String profileImg;
  String name;
  String feedImage;

  Feeds({this.profileImg, this.name, this.feedImage});
}
