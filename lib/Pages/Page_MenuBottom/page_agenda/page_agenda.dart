import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_agenda/page_jadwal.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_agenda/page_pengumuman.dart';
import 'package:dataAlumni/util/constant.dart';
import 'package:dataAlumni/model/models.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/DataGenerator.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/model/agenda.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';

class Agenda extends StatefulWidget {
  static var tag = "/T2Cards";

  @override
  _AgendaState createState() => _AgendaState();
}

class _AgendaState extends State<Agenda> {
  List<T2Favourite> mFavouriteList;
  GlobalKey _two = GlobalKey();
  GlobalKey _three = GlobalKey();
  // GlobalKey _four = GlobalKey();
  // GlobalKey _five = GlobalKey();
  BuildContext myContext;
  bool statusShow;

  @override
  void initState() {
    super.initState();
    mFavouriteList = getFavourites();
    getTanda();
  }

  void getTanda() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      statusShow = prefs.getBool("tandaShowCase2");
    });
    if(statusShow == null){
      WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(milliseconds: 200),
          () => ShowCaseWidget.of(myContext).startShowCase([_two, _three]));
    });
    } else{
      
    }
    print("sini woyy22");
    print(statusShow);
    print(prefs.getBool("tandaShowCase2"));
  }

  void statusTanda() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("tandaShowCase2", false);
    print("sini woyy");
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return ShowCaseWidget(
      onComplete: (index, key) {
        print('onComplete: $index, $key');
        if(index == 1){
          statusTanda();
        }
      },
      builder: Builder(
        builder: (context) {
          myContext = context;
          return SafeArea(
            child: Scaffold(
              appBar: GradientAppBar(
                backgroundColorStart: Color(0xFF398AE5),
                backgroundColorEnd: Color(0xFF61A4F1),
                title: Center(child: Text('Daftar Agenda')),
                // leading: Container(
                //   child: InkWell(
                //     onTap: () {
                //       Navigator.pop(context);
                //     },
                //   ),
                // ),
              ),
              body: Container(
                margin: EdgeInsets.only(top: 20),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Showcase(
                      //   key: _two,
                      //   title: "Jadwal",
                      //   description: "Untuk mengetahui semua jadwal kegiatan",
                      //   child: InkWell(
                      //     onTap: () {
                      //       Navigator.push(
                      //           context,
                      //           MaterialPageRoute(
                      //               builder: (context) => CalenderHomePage()));
                      //     },
                      //     child: Container(
                      //         height: 150,
                      //         margin: EdgeInsets.only(
                      //             left: 16, right: 16, bottom: 16),
                      //         decoration: BoxDecoration(
                      //             gradient: new LinearGradient(
                      //           begin: Alignment.topCenter,
                      //           end: Alignment.bottomCenter,
                      //           colors: [
                      //             Color(0xFF73AEF5),
                      //             Color(0xFF61A4F1),
                      //             Color(0xFF478DE0),
                      //             Color(0xFF398AE5),
                      //           ],
                      //           stops: [0.1, 0.4, 0.7, 0.9],
                      //         )),
                      //         child: Stack(
                      //           children: <Widget>[
                      //             Opacity(
                      //               opacity: 0.2,
                      //               child: Container(
                      //                 decoration: BoxDecoration(
                      //                     image: DecorationImage(
                      //                         image: AssetImage(
                      //                             "images/dataAlumni/" +
                      //                                 InfoAgenda().list[0].gbr +
                      //                                 ".jpg"),
                      //                         fit: BoxFit.fill)),
                      //               ),
                      //             ),
                      //             Column(
                      //               mainAxisAlignment: MainAxisAlignment.center,
                      //               children: [
                      //                 Row(
                      //                   mainAxisAlignment:
                      //                       MainAxisAlignment.center,
                      //                   children: [
                      //                     Container(
                      //                       child: Text(
                      //                         InfoAgenda().list[0].name,
                      //                         style: TextStyle(
                      //                             color: Colors.white,
                      //                             fontSize: 20,
                      //                             fontWeight: FontWeight.bold),
                      //                       ),
                      //                     ),
                      //                   ],
                      //                 ),
                      //                 SizedBox(height: 5),
                      //                 Container(
                      //                   child: Icon(Icons.calendar_today,
                      //                       color: Colors.white),
                      //                 )
                      //               ],
                      //             )
                      //           ],
                      //         )),
                      //   ),
                      // ),
                      Showcase(
                        key: _three,
                        title: "Pengumuman",
                        description: "Untuk mengetahui pengumuman terbaru",
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PagePengumuman()));
                          },
                          child: Container(
                              height: 150,
                              margin: EdgeInsets.only(
                                  left: 16, right: 16, bottom: 16),
                              decoration: BoxDecoration(
                                  gradient: new LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color(0xFF73AEF5),
                                  Color(0xFF61A4F1),
                                  Color(0xFF478DE0),
                                  Color(0xFF398AE5),
                                ],
                                stops: [0.1, 0.4, 0.7, 0.9],
                              )),
                              child: Stack(
                                children: <Widget>[
                                  Opacity(
                                    opacity: 0.2,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "images/dataAlumni/" +
                                                      InfoAgenda().list[1].gbr +
                                                      ".jpg"),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            child: Text(
                                              InfoAgenda().list[1].name,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 5),
                                      Container(
                                        child: Icon(Icons.speaker_notes,
                                            color: Colors.white),
                                      )
                                    ],
                                  )
                                ],
                              )),
                        ),
                      )
                      // Expanded(
                      //   child: ListView.builder(
                      //     scrollDirection: Axis.vertical,
                      //     itemCount: 2,
                      //     itemBuilder: (context, index) {
                      //       return InkWell(
                      //         onTap: () {
                      //           if(InfoAgenda().list[index].name == "Jadwal"){
                      //             Navigator.push(context, MaterialPageRoute(
                      //                             builder: (context) => CalenderHomePage()));
                      //           } else {
                      //             Navigator.push(context, MaterialPageRoute(
                      //                             builder: (context) => PagePengumuman()));
                      //           }
                      //         },
                      //         child: Container(
                      //             height: 150,
                      //             margin:
                      //                 EdgeInsets.only(left: 16, right: 16, bottom: 16),
                      //             decoration: BoxDecoration(
                      //                 gradient: new LinearGradient(
                      //               begin: Alignment.topCenter,
                      //               end: Alignment.bottomCenter,
                      //               colors: [
                      //                 Color(0xFF73AEF5),
                      //                 Color(0xFF61A4F1),
                      //                 Color(0xFF478DE0),
                      //                 Color(0xFF398AE5),
                      //               ],
                      //               stops: [0.1, 0.4, 0.7, 0.9],
                      //             )),
                      //             child: Stack(
                      //               children: <Widget>[
                      //                 Opacity(
                      //                   opacity: 0.2,
                      //                   child: Container(
                      //                     decoration: BoxDecoration(
                      //                         image: DecorationImage(
                      //                             image: AssetImage(
                      //                                 "images/dataAlumni/" +
                      //                                     InfoAgenda().list[index].gbr +
                      //                                     ".jpg"),
                      //                             fit: BoxFit.fill)),
                      //                   ),
                      //                 ),
                      //                 Column(
                      //                   mainAxisAlignment: MainAxisAlignment.center,
                      //                   children: [
                      //                     Row(
                      //                       mainAxisAlignment: MainAxisAlignment.center,
                      //                       children: [
                      //                         Container(
                      //                           child: Text(
                      //                             InfoAgenda().list[index].name,
                      //                             style: TextStyle(
                      //                                 color: Colors.white,
                      //                                 fontSize: 20,
                      //                                 fontWeight: FontWeight.bold),
                      //                           ),
                      //                         )
                      //                       ],
                      //                     )
                      //                   ],
                      //                 )
                      //               ],
                      //             )),
                      //       );
                      //     },
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
