import 'dart:io';

import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/editFotoProfile/editfotoprofile_bloc.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toggle_switch/toggle_switch.dart';

class EditFoto extends StatefulWidget {
  final DataAlumni dataList;
  EditFoto({this.dataList});
  @override
  _EditFotoState createState() =>
      _EditFotoState(dataList: this.dataList);
}

class _EditFotoState extends State<EditFoto> {
  final TextEditingController _caption = TextEditingController();
  final TextEditingController _judul = TextEditingController();
  final TextEditingController _subjudul = TextEditingController();
  File imageFiles;
  final int indexTo;
  final DataAlumni dataList;
  _EditFotoState({this.dataList, this.indexTo});

  @override
  void initState() {
    super.initState();
  }

  _openGalery() async {
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageFiles = picture;
    });
    // Navigator.of(context).pop();
    // if (imageFile != null) {
    //   return Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => PostGaleryProfil(dataList: dataList)));
    // } else {
    //   Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    // }
  }

  @override
  Widget build(BuildContext context) {
    final _postBloc = EditfotoprofileBloc();
    return Scaffold(
      appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Text('Edit Postingan'),
          leading: Container(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back_ios, color: Colors.white),
            ),
          ),
          actions: <Widget>[
            BlocListener<EditfotoprofileBloc, EditfotoprofileState>(
              bloc: _postBloc,
              listener: (context, state) {
                if(state is EditFotoProfilSukses){
                  Fluttertoast.showToast(msg: "Success Edit Foto");
                  Navigator.of(context).pop(true);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Home(indexTo: 4)));
                }
                if (state is EditFotoError) {
                  Fluttertoast.showToast(msg: "Edit Gagal");
                }
                if (state is EditFotoWaiting) {
                  return CircularProgressIndicator();
                }
              },
              child: BlocBuilder<EditfotoprofileBloc, EditfotoprofileState>(
                bloc: _postBloc,
                builder: (context, state) {
                  if (state is EditFotoProfilSukses) {
                    return buildIconButtonPost(_postBloc);
                  }
                  if (state is EditFotoError) {
                    Fluttertoast.showToast(msg: "koneksi gagal");
                  }
                  if (state is EditFotoWaiting) {
                    return CircularProgressIndicator();
                  }
                  return buildIconButtonPost(_postBloc);
                },
              ),
            )
          ]),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                  flex: 1,
                  child: Column(
                    children: [
                      Container(
                          height: MediaQuery.of(context).size.height / 7,
                          child: (imageFiles == null)
                              ? Image.memory(
                                  dataList.gambarBase64,
                                  width: 110,
                                  height: 50,
                                )
                              : Image.file(
                                  imageFiles,
                                  width: 110,
                                  height: 50,
                                )),
                      (imageFiles == null)
                          ? RaisedButton(
                              onPressed: () {
                                _openGalery();
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                // decoration: const BoxDecoration(
                                //   color: Colors.white,
                                //   borderRadius: BorderRadius.all(
                                //     Radius.circular(20.0),
                                //   ),
                                // ),
                                child: Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.all(5.0),
                                        child: Icon(
                                            Icons.create_new_folder_outlined,
                                            color: Colors.grey)),
                                    Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 50.0, minHeight: 30.0),
                                      alignment: Alignment.center,
                                      child: const Text(
                                        'Pilih File',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : RaisedButton(
                              onPressed: () {
                                setState(() {
                                  imageFiles = null;
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              padding: const EdgeInsets.all(0.0),
                              child: Ink(
                                // decoration: const BoxDecoration(
                                //   color: Colors.white,
                                //   borderRadius: BorderRadius.all(
                                //     Radius.circular(20.0),
                                //   ),
                                // ),
                                child: Row(
                                  children: [
                                    Container(
                                        padding: EdgeInsets.all(5.0),
                                        child: Icon(Icons.delete,
                                            color: Colors.red)),
                                    Container(
                                      constraints: const BoxConstraints(
                                          minWidth: 50.0, minHeight: 30.0),
                                      alignment: Alignment.center,
                                      child: const Text(
                                        'Hapus Foto',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  IconButton buildIconButtonPost(EditfotoprofileBloc _postBloc) {
    return IconButton(
      icon: Icon(Icons.send),
      onPressed: () {
        print("page");
        
        _postBloc.add(EditFotoEvent(gambar64: imageFiles));
        Container(width: 50, child: CircularProgressIndicator());
      },
    );
  }
}
