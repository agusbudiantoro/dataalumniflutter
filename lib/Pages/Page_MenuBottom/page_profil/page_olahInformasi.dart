import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/InformasiUser/informasi_user_bloc.dart';
import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dataAlumni/util/model/informasiUser.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator/listingTheme1.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class T1Listing extends StatefulWidget {
  final int posInd;
  static var tag = "/T1Listing";

  T1Listing({@required this.posInd});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return T1ListingState(posInd: this.posInd);
  }
}

class T1ListingState extends State<T1Listing> {
  // for pendidikan
  final TextEditingController _namaUniv = TextEditingController();
  final TextEditingController _jenjang = TextEditingController();
  final TextEditingController _lokasi = TextEditingController();
  final TextEditingController _tahun = TextEditingController();
  final TextEditingController _namaUnivEd = TextEditingController();
  final TextEditingController _jenjangEd = TextEditingController();
  final TextEditingController _lokasiEd = TextEditingController();
  final TextEditingController _tahunEd = TextEditingController();
  // end pendidikan

  // for karir
  final TextEditingController _posisi = TextEditingController();
  final TextEditingController _tempat = TextEditingController();
  final TextEditingController _tahun1 = TextEditingController();
  final TextEditingController _posisiEd = TextEditingController();
  final TextEditingController _tempatEd = TextEditingController();
  final TextEditingController _tahun1Ed = TextEditingController();
  // end karir

  // for Organisasi
  final TextEditingController _namaOrg = TextEditingController();
  final TextEditingController _bidang = TextEditingController();
  final TextEditingController _jabatan = TextEditingController();
  final TextEditingController _tahun2 = TextEditingController();
  final TextEditingController _namaOrgEd = TextEditingController();
  final TextEditingController _bidangEd = TextEditingController();
  final TextEditingController _jabatanEd = TextEditingController();
  final TextEditingController _tahun2Ed = TextEditingController();
  // end Organisasi
  int idData;
  int indexData;

  List<T1Model> mListings;
  final _bloc = InformasiUserBloc();
  final int posInd;
  T1ListingState({@required this.posInd});
  List<ModelPendidikanUser> dataInPendidikan;
  List<ModelKarir> dataInKarir;
  List<ModelOrganisasi> dataInOrganisasi;

  @override
  void initState() {
    super.initState();
    mListings = getListings();
    _bloc.add(InformasiUserEvent(urutan: posInd));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            (widget.posInd == 2)
                ? 'Kelola Organisasi'
                : (widget.posInd == 1)
                    ? 'Kelola Karir'
                    : (widget.posInd == 0)
                        ? 'Kelola Pendidikan'
                        : "Error",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            InkWell(
              onTap: () {
                _showAlert(context, _bloc);
              },
              child: Row(
                children: [
                  Center(
                      child: Text(
                    'Tambah',
                    style: TextStyle(color: Colors.black),
                  )),
                  Center(
                    child: Icon(
                      Icons.add_box_outlined,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        body: BlocListener<InformasiUserBloc, InformasiUserState>(
          bloc: _bloc,
          listener: (context, state) {
            if (state is GetInformasiSukses) {
              setState(() {
                dataInPendidikan = state.dataPendidikan;
                dataInKarir = state.dataKarir;
                dataInOrganisasi = state.dataOrganisasi;
              });
              return buildContainer(state);
            }
            if (state is GetInformasiError) {
              Fluttertoast.showToast(msg: "gagal");
              return Center(
                  child: Container(child: CircularProgressIndicator()));
            }
          },
          child: BlocBuilder<InformasiUserBloc, InformasiUserState>(
            bloc: _bloc,
            builder: (context, state) {
              if (state is GetInformasiSukses) {
                return buildContainer(state);
              }
              if (state is GetInformasiWaiting) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
              if (state is GetInformasiError) {
                Fluttertoast.showToast(msg: "gagal");
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
              if (state is InformasiUserInitial) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
            },
          ),
        ));
  }

  Container buildContainer(GetInformasiSukses data) {
    return Container(
      child: Column(
        children: <Widget>[
          // TopBar(t1_Listing, true),
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: (posInd == 0)
                    ? data.dataPendidikan.length
                    : (posInd == 1)
                        ? data.dataKarir.length
                        : (posInd == 2)
                            ? data.dataOrganisasi.length
                            : 0,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return T1ListItem(context, data, index);
                }),
          )
        ],
      ),
    );
  }

  Future<void> _showAlertHapus(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentHapus(context),
          );
        });
  }

  dialogContentHapus(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                margin: EdgeInsets.all(10),
                child: Text("Apakah sudah yakin ingin menghapus data?"),
              )
            ]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    if (widget.posInd == 0) {
                      ModelPendidikanUser dataPost = ModelPendidikanUser(
                          id: idData,
                          namauniv: _namaUnivEd.text.toString(),
                          jenjang: _jenjangEd.text.toString(),
                          lokasi: _lokasiEd.text.toString(),
                          tahun: _tahunEd.text.toString());
                      _bloc
                        ..add(HapusDataInformasiEvent(
                            indexData: indexData,
                            dataPendidikan: dataPost,
                            urutanInd: posInd,
                            dataPendidikanOld: dataInPendidikan));
                      Navigator.of(context).pop();
                    } else if(widget.posInd == 1){
                      ModelKarir dataPost = ModelKarir(
                          id: idData);
                      _bloc
                        ..add(HapusDataInformasiEvent(
                            indexData: indexData,
                            dataKarir: dataPost,
                            urutanInd: posInd,
                            dataKarirOld: dataInKarir));
                      Navigator.of(context).pop();
                    } else if(widget.posInd == 2){
                      ModelOrganisasi dataPost = ModelOrganisasi(
                          id: idData);
                      _bloc
                        ..add(HapusDataInformasiEvent(
                            indexData: indexData,
                            dataOrganisasi: dataPost,
                            urutanInd: posInd,
                            dataOrganisasiOld: dataInOrganisasi));
                      Navigator.of(context).pop();
                    }
                  },
                  child: Row(
                    children: [
                      Icon(Icons.delete, color: Colors.redAccent),
                      Text("Hapus")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showAlertEdit(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentEdit(context),
          );
        });
  }

  dialogContentEdit(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
                mainAxisSize: MainAxisSize.min,
                children: (widget.posInd == 0)
                    ? <Widget>[
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _namaUnivEd,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration: InputDecoration(
                                  hintText: "Nama Universitas ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _jenjangEd,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Jenjang ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _lokasiEd,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Lokasi ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _tahunEd,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Tahun ...."),
                            )),
                      ]
                    : (widget.posInd == 1)
                        ? <Widget>[
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _posisiEd,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Posisi ...."),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _tempatEd,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Tempat ...."),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _tahun1Ed,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Tahun ...."),
                                )),
                          ]
                        : (widget.posInd == 2)
                            ? <Widget>[
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _namaOrgEd,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Nama Organisasi ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _bidangEd,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Bidang Organisasi ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _jabatanEd,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Jabatan ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _tahun2Ed,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Tahun ...."),
                                    )),
                              ]
                            : <Widget>[Text("Error")]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    if (widget.posInd == 0) {
                      ModelPendidikanUser dataPost = ModelPendidikanUser(
                          id: idData,
                          namauniv: _namaUnivEd.text.toString(),
                          jenjang: _jenjangEd.text.toString(),
                          lokasi: _lokasiEd.text.toString(),
                          tahun: _tahunEd.text.toString());
                      _bloc
                        ..add(EditDataInformasiEvent(
                            indexData: indexData,
                            dataPendidikan: dataPost,
                            urutanInd: posInd,
                            dataPendidikanOld: dataInPendidikan));
                      Navigator.of(context).pop();
                    } else if (widget.posInd == 1) {
                      ModelKarir dataPost = ModelKarir(
                          id: idData,
                          posisi: _posisiEd.text.toString(),
                          tempat: _tempatEd.text.toString(),
                          tahun: _tahun1Ed.text.toString());
                      _bloc
                        ..add(EditDataInformasiEvent(
                            indexData: indexData,
                            dataKarir: dataPost,
                            urutanInd: posInd,
                            dataKarirOld: dataInKarir));
                      Navigator.of(context).pop();
                    } else if (widget.posInd == 2) {
                      ModelOrganisasi dataPost = ModelOrganisasi(
                          id: idData,
                          namaOrganisasi: _namaOrgEd.text.toString(),
                          bidangOrganisasi: _bidangEd.text.toString(),
                          jabatan: _jabatanEd.text.toString(),
                          tahun: _tahun2Ed.text.toString());
                      _bloc
                        ..add(EditDataInformasiEvent(
                            indexData: indexData,
                            dataOrganisasi: dataPost,
                            urutanInd: posInd,
                            dataOrganisasiOld: dataInOrganisasi));
                      Navigator.of(context).pop();
                    }
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.save,
                        color: Colors.blueAccent,
                      ),
                      Text("Simpan")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showAlert(BuildContext context, InformasiUserBloc _bloc) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContent(context, _bloc),
          );
        });
  }

  dialogContent(BuildContext context, InformasiUserBloc _bloc) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
                mainAxisSize: MainAxisSize.min,
                children: (widget.posInd == 0)
                    ? <Widget>[
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _namaUniv,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration: InputDecoration(
                                  hintText: "Nama Universitas ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _jenjang,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Jenjang ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _lokasi,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Lokasi ...."),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Flexible(
                            flex: 2,
                            child: TextField(
                              controller: _tahun,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: 5,
                              decoration:
                                  InputDecoration(hintText: "Tahun ...."),
                            )),
                      ]
                    : (widget.posInd == 1)
                        ? <Widget>[
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _posisi,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Posisi ...."),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _tempat,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Tempat ...."),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            Flexible(
                                flex: 2,
                                child: TextField(
                                  controller: _tahun1,
                                  keyboardType: TextInputType.multiline,
                                  minLines: 1,
                                  maxLines: 5,
                                  decoration:
                                      InputDecoration(hintText: "Tahun ...."),
                                )),
                          ]
                        : (widget.posInd == 2)
                            ? <Widget>[
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _namaOrg,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Nama Organisasi ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _bidang,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Bidang Organisasi ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _jabatan,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Jabatan ...."),
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Flexible(
                                    flex: 2,
                                    child: TextField(
                                      controller: _tahun2,
                                      keyboardType: TextInputType.multiline,
                                      minLines: 1,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                          hintText: "Tahun ...."),
                                    )),
                              ]
                            : <Widget>[Text("Error")]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    print(widget.posInd);
                    if (widget.posInd == 0) {
                      ModelPendidikanUser dataPost = ModelPendidikanUser(
                          namauniv: _namaUniv.text.toString(),
                          jenjang: _jenjang.text.toString(),
                          lokasi: _lokasi.text.toString(),
                          tahun: _tahun.text.toString());
                      _bloc
                        ..add(AddDataInformasiEvent(
                            dataPendidikan: dataPost,
                            dataPendidikanOld: dataInPendidikan,
                            urutanInd: posInd));
                      Navigator.of(context).pop();
                    } else if (widget.posInd == 1) {
                      print("masuk 1");
                      ModelKarir dataPost = ModelKarir(
                          posisi: _posisi.text.toString(),
                          tempat: _tempat.text.toString(),
                          tahun: _tahun1.text.toString());
                      _bloc
                        ..add(AddDataInformasiEvent(
                            dataKarir: dataPost,
                            dataKarirOld: dataInKarir,
                            urutanInd: posInd));
                      Navigator.of(context).pop();
                    } else if(widget.posInd == 2){
                      print("masuk 2");
                      ModelOrganisasi dataPost = ModelOrganisasi(
                          namaOrganisasi: _namaOrg.text.toString(),
                          bidangOrganisasi: _bidang.text.toString(),
                          jabatan: _jabatan.text.toString(),
                          tahun: _tahun2.text.toString()
                          );
                      _bloc
                        ..add(AddDataInformasiEvent(
                            dataOrganisasi: dataPost,
                            dataOrganisasiOld: dataInOrganisasi,
                            urutanInd: posInd));
                      Navigator.of(context).pop();
                    }
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.save,
                        color: Colors.blueAccent,
                      ),
                      Text("Simpan")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget T1ListItem(BuildContext context, GetInformasiSukses model, int pos) {
    var width = MediaQuery.of(context).size.width;
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Container(
          decoration: boxDecoration(radius: 10, showShadow: true),
          child: Column(
            children: [
              Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(16),
                    child: (widget.posInd == 2)
                        ? widgetOrganisasi(model, pos)
                        : (widget.posInd == 1)
                            ? widgetKarir(model, pos)
                            : (widget.posInd == 0)
                                ? widgetPendidikan(model, pos)
                                : Text("Error"),
                  ),
                  Container(
                    width: 4,
                    height: 35,
                    margin: EdgeInsets.only(top: 16),
                    color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          _showAlertEdit(context);
                          setState(() {
                            if (widget.posInd == 0) {
                              _namaUnivEd.text =
                                  model.dataPendidikan[pos].namauniv;
                              _jenjangEd.text =
                                  model.dataPendidikan[pos].jenjang;
                              _lokasiEd.text = model.dataPendidikan[pos].lokasi;
                              _tahunEd.text = model.dataPendidikan[pos].tahun;
                              idData = model.dataPendidikan[pos].id;
                            } else if (widget.posInd == 1) {
                              _posisiEd.text = model.dataKarir[pos].posisi;
                              _tempatEd.text = model.dataKarir[pos].tempat;
                              _tahun1Ed.text = model.dataKarir[pos].tahun;
                              idData = model.dataKarir[pos].id;
                            } else if(widget.posInd == 2){
                              _namaOrgEd.text = model.dataOrganisasi[pos].namaOrganisasi;
                              _bidangEd.text = model.dataOrganisasi[pos].bidangOrganisasi;
                              _jabatanEd.text = model.dataOrganisasi[pos].jabatan;
                              _tahun2Ed.text = model.dataOrganisasi[pos].tahun;
                              idData = model.dataOrganisasi[pos].id;
                            }
                            indexData = pos;
                          });
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.edit,
                              color: Colors.orange,
                            ),
                            Text("Edit")
                          ],
                        ),
                      ),
                      SizedBox(width: 50),
                      InkWell(
                        onTap: () {
                          _showAlertHapus(context);
                          setState(() {
                            if(widget.posInd == 0){
                              idData = model.dataPendidikan[pos].id;
                            } else if(widget.posInd == 1){
                              idData = model.dataKarir[pos].id;
                            } else if(widget.posInd == 2){
                              idData = model.dataOrganisasi[pos].id;
                            }
                            indexData = pos;
                          });
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.delete_forever,
                              color: Colors.red,
                            ),
                            Text("Hapus")
                          ],
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ));
  }

  Widget widgetPendidikan(GetInformasiSukses data, int ind) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(child: Text("Nama Universitas : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataPendidikan[ind].namauniv.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Jenjang                   : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataPendidikan[ind].jenjang.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Lokasi                      : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataPendidikan[ind].lokasi.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Tahun                       : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataPendidikan[ind].tahun.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
      ],
    );
  }

  Widget widgetKarir(GetInformasiSukses data, int ind) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(child: Text("Posisi              : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataKarir[ind].posisi.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Tempat           : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataKarir[ind].tempat.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Tahun              : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataKarir[ind].tahun.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }

  Widget widgetOrganisasi(GetInformasiSukses data, int ind) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(child: Text("Nama Organisasi      : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataOrganisasi[ind].namaOrganisasi.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Bidang Organisasi    : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataOrganisasi[ind].bidangOrganisasi.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Jabatan                      : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataOrganisasi[ind].jabatan.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Container(child: Text("Tahun                          : ")),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.dataOrganisasi[ind].tahun.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
      ],
    );
  }
}
