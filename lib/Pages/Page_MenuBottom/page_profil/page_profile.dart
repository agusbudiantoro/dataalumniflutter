import 'dart:convert';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/feed_profil.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/page_edit_foto.dart';
import 'package:dataAlumni/Pages/Widget/Profil/WidgetCard.dart';
import 'package:dataAlumni/Pages/Widget/Profil/feed_dialog.dart';
import 'package:dataAlumni/Pages/Widget/Profil/profileSide.dart';
import 'package:dataAlumni/Pages/Widget/Profil/widgetProper.dart';
import 'package:dataAlumni/Pages/login.dart';
import 'package:dataAlumni/bloc/MenuAlumni/DetailProfil/detailprofil_bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/bloc/ProperById/properbyid_bloc.dart';
import 'package:dataAlumni/model/T4Models.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/model/modeProper.dart';
import 'package:dataAlumni/Pages/Widget/Profil/addDataAlumni.dart';
import 'package:dataAlumni/Pages/Widget/Profil/editDataAlumni.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/DataGenerator/T4DataGenerator.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/Widget/widgetCardOpen.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:dataAlumni/util/model/informasiUser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/feed_profil.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'package:flutter/material.dart';
import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator/T10.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class T10Profile extends StatefulWidget {
  static String tag = '/T10Profile';
  List<ModelProper> listDataProperNow;

  T10Profile({this.listDataProperNow});
  @override
  T10ProfileState createState() => T10ProfileState();
}

class T10ProfileState extends State<T10Profile> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _foldingCellKey = GlobalKey<SimpleFoldingCellState>();
  List<T10Images> mList;
  FeedProfileBloc bloc;
  DetailprofilBloc blocDetailProfil;
  final ProperbyidBloc blocProper = ProperbyidBloc();
  String namaUser;
  String namaLemdik;
  String email;
  int idUser;
  TabController _tabController;
  int initialIndex = 0;
  bool statusSCroll = false;
  bool statusScrollHabis = false;
  int removeIndex;
  ScrollController controller = ScrollController();
  ScrollController controller2 = ScrollController();
  Uint8List fileFoto = base64Decode(IconOrang().gbrPeople);

  @override
  void initState() {
    super.initState();
    mList = getProfile();
    getFromSharedPreferences();
    _tabController = TabController(vsync: this, length: 2);
    print("siniuser");
    print(idUser);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      idUser = prefs.getInt("iduser");
      email = prefs.getString("email");
      namaUser = prefs.getString("namaUser");
      namaLemdik = prefs.getString("namaLemdik");
    });
    // return blocProper.add(GetProperById(id: idUser));
  }

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    bloc.add(FeedProfileRefreshEvent());
    blocDetailProfil.add(GetDetailProfilEvent());
  }

  void onScroll() {
    double maxScroll = controller.position.maxScrollExtent;
    double currentScroll = controller.position.pixels;

    if (currentScroll == maxScroll &&
        statusSCroll == false &&
        initialIndex == 0) {
      return bloc.add(FeedProfileEvent());
    }
    if (currentScroll == maxScroll &&
        statusSCroll == true &&
        initialIndex == 0 && statusScrollHabis == false) {
      Fluttertoast.showToast(msg: "data habis");
      setState(() {
        statusScrollHabis = true;
      });
    }
    if(currentScroll == maxScroll &&
        statusSCroll == true &&
        initialIndex == 0 && statusScrollHabis == true){
          Container();
        }
  }

  // void onScrollHabis() {
  //   Fluttertoast.showToast(msg: "data habis");
  //   setState(() {
  //     statusSCroll = true;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<FeedProfileBloc>(context);
    changeStatusColor(t10_white);
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: t10_white,
      body: BlocListener<FeedProfileBloc, FeedProfileState>(
        bloc: bloc,
        listener: (context, state) {
          print(state);
          print("cobastate");
          if (state is FeedSuccessLoad) {
            if (state.hasReachMax == true) {
              setState(() {
                statusSCroll = true;
              });
            }
            // Fluttertoast.showToast(msg: "koneksi ssuksesssssssssssss");
            return buildSafeAreaBody(width, state);
          }
          if (state is FeedProfilError) {
            Fluttertoast.showToast(msg: "koneksi gagal");
          }
          return loadingPage(width);
        },
        child: Container(
          child: BlocBuilder<FeedProfileBloc, FeedProfileState>(
            builder: (context, state) {
              if (state is FeedProfileInitial) {
                return loadingPage(width);
              }
              if (state is FeedSuccessLoad) {
                print("lihat has");
                print(state.hasReachMax);
                // Fluttertoast.showToast(msg: "koneksi sukses");
                return buildSafeAreaBody(width, state);
              }
              if (state is FeedProfilError) {
                Fluttertoast.showToast(msg: "koneksi gagal");
                return loadingPage(width);
              }
              if (state is FeedProfilWaiting) {
                // Fluttertoast.showToast(msg: "Loading");
                return loadingPage(width);
              }
            },
          ),
        ),
      ),
      drawer: T10Drawer(),
    );
  }

  RefreshIndicator loadingPage(double width) {
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            topBar(context),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(spacing_middle),
                                bottomRight:
                                    Radius.circular(spacing_standard_new)),
                            child: Image.memory(
                              fileFoto,
                              fit: BoxFit.cover,
                              height: width * 0.35,
                              // cacheHeight: 300,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: spacing_standard_new,
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Center(
                                child: text("$namaUser",
                                    fontFamily: fontMedium,
                                    fontSize: textSizeLargeMedium),
                              ),
                              SizedBox(
                                height: spacing_control,
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                              // Row(
                              //   children: <Widget>[
                              //     Column(
                              //       crossAxisAlignment:
                              //           CrossAxisAlignment.start,
                              //       children: <Widget>[
                              //         text(theme10_lbl_150,
                              //             fontFamily: fontMedium),
                              //         text(theme10_lbl_design,
                              //             textColor: t10_textColorSecondary),
                              //       ],
                              //     ),
                              //     Container(
                              //       height: width * 0.1,
                              //       width: 0.5,
                              //       color: t10_view_color,
                              //       margin: EdgeInsets.only(
                              //           left: spacing_standard_new,
                              //           right: spacing_standard_new),
                              //     ),
                              //     Column(
                              //       crossAxisAlignment:
                              //           CrossAxisAlignment.start,
                              //       children: <Widget>[
                              //         text(theme10_lbl_2K,
                              //             fontFamily: fontMedium),
                              //         text(theme10_lbl_followers,
                              //             textColor: t10_textColorSecondary),
                              //       ],
                              //     )
                              //   ],
                              // )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.all(spacing_standard_new),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          text("Lemdik",
                              fontFamily: fontMedium,
                              fontSize: textSizeLargeMedium),
                          text("$namaLemdik",
                              textColor: t10_textColorSecondary,
                              isLongText: true),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              // text(theme10_photos + " and " + theme10_videos,
                              //     fontFamily: fontMedium,
                              //     fontSize: textSizeLargeMedium),
                              // RichText(
                              //   text: TextSpan(
                              //     children: [
                              //       TextSpan(
                              //           text: theme10_view_all,
                              //           style: TextStyle(
                              //               fontSize: textSizeMedium,
                              //               fontFamily: fontMedium,
                              //               color: t10_textColorSecondary)),
                              //       WidgetSpan(
                              //         child: Padding(
                              //           padding: EdgeInsets.only(left: 0),
                              //           child: Icon(
                              //             Icons.keyboard_arrow_right,
                              //             color: t10_textColorPrimary,
                              //             size: 18,
                              //           ),
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2,
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2,
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2,
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2,
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                              child: Shimmer.fromColors(
                            baseColor: Colors.grey[400],
                            highlightColor: Colors.grey[100],
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2,
                              margin: EdgeInsets.only(top: 15),
                              height: 10,
                              color: Colors.grey,
                            ),
                          )),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          // ListPosts(tabController: _tabController),
                          // Text(data.listPost[0].stringGambar),
                          GridView.builder(
                            reverse: true,
                            scrollDirection: Axis.vertical,
                            itemCount: 6,
                            shrinkWrap: true,
                            physics: AlwaysScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Shimmer.fromColors(
                                baseColor: Colors.grey[400],
                                highlightColor: Colors.grey[100],
                                child: ClipRRect(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(spacing_middle)),
                                    child: Container(
                                      color: Colors.black,
                                    )),
                              );
                            },
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 10,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildSafeAreaBody(double width, FeedSuccessLoad data) {
    controller.addListener(onScroll);
    // return LazyLoadScrollView(
    //   onEndOfPage: (data.hasReachMax == false) ? onScroll : onScrollHabis,
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            topBar(context),
            Expanded(
              child: SingleChildScrollView(
                controller: controller,
                // physics: NeverScrollableScrollPhysics(),
                child: BlocListener<DetailprofilBloc, DetailprofilState>(
                  bloc: blocDetailProfil,
                  listener: (context, state) {
                    if (state is GetDetailSukses) {
                      Fluttertoast.showToast(msg: "Sukses ambil data profil");
                    }
                  },
                  child: Container(
                    child: BlocBuilder<DetailprofilBloc, DetailprofilState>(
                      bloc: blocDetailProfil,
                      builder: (context, state) {
                        if (state is GetDetailSukses) {
                          print("sini");
                          print(state.listAlumni);
                          print(state.listAlumni.length);
                          if (state.listAlumni.length != 0) {
                            return listData(width, state, data);
                          } else {
                            return listDataNull(width, data);
                          }
                        }
                        if (state is GetDataWaiting) {
                          return loadingPageKedua(width);
                        }
                        if (state is GetDataError) {
                          Fluttertoast.showToast(msg: "errorr");
                        }
                        return loadingPageKedua(width);
                      },
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    // );
  }

  Widget loadingPageKedua(double width) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: spacing_standard_new,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(spacing_middle),
                    bottomRight: Radius.circular(spacing_standard_new)),
                child: Image.memory(
                  fileFoto,
                  fit: BoxFit.cover,
                  height: width * 0.35,
                  // cacheHeight: 300,
                ),
              ),
            ),
            SizedBox(
              width: spacing_standard_new,
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: text("$namaUser",
                        fontFamily: fontMedium, fontSize: textSizeLargeMedium),
                  ),
                  SizedBox(
                    height: spacing_standard,
                  ),
                  // Row(
                  //   children: <Widget>[
                  //     Column(
                  //       crossAxisAlignment:
                  //           CrossAxisAlignment.start,
                  //       children: <Widget>[
                  //         text(theme10_lbl_150,
                  //             fontFamily: fontMedium),
                  //         text(theme10_lbl_design,
                  //             textColor: t10_textColorSecondary),
                  //       ],
                  //     ),
                  //     Container(
                  //       height: width * 0.1,
                  //       width: 0.5,
                  //       color: t10_view_color,
                  //       margin: EdgeInsets.only(
                  //           left: spacing_standard_new,
                  //           right: spacing_standard_new),
                  //     ),
                  //     Column(
                  //       crossAxisAlignment:
                  //           CrossAxisAlignment.start,
                  //       children: <Widget>[
                  //         text(theme10_lbl_2K,
                  //             fontFamily: fontMedium),
                  //         text(theme10_lbl_followers,
                  //             textColor: t10_textColorSecondary),
                  //       ],
                  //     )
                  //   ],
                  // )
                ],
              ),
            )
          ],
        ),
        Container(
          margin: EdgeInsets.all(spacing_standard_new),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              text("Lemdik",
                  fontFamily: fontMedium, fontSize: textSizeLargeMedium),
              text("$namaLemdik",
                  textColor: t10_textColorSecondary, isLongText: true),
              SizedBox(
                height: spacing_standard_new,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // text(theme10_photos + " and " + theme10_videos,
                  //     fontFamily: fontMedium,
                  //     fontSize: textSizeLargeMedium),
                  // RichText(
                  //   text: TextSpan(
                  //     children: [
                  //       TextSpan(
                  //           text: theme10_view_all,
                  //           style: TextStyle(
                  //               fontSize: textSizeMedium,
                  //               fontFamily: fontMedium,
                  //               color: t10_textColorSecondary)),
                  //       WidgetSpan(
                  //         child: Padding(
                  //           padding: EdgeInsets.only(left: 0),
                  //           child: Icon(
                  //             Icons.keyboard_arrow_right,
                  //             color: t10_textColorPrimary,
                  //             size: 18,
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                  child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  width: MediaQuery.of(context).size.width / 2,
                  margin: EdgeInsets.only(top: 15),
                  height: 10,
                  color: Colors.grey,
                ),
              )),
              SizedBox(
                height: spacing_standard_new,
              ),
              // ListPosts(tabController: _tabController),
              // Text(data.listPost[0].stringGambar),
              GridView.builder(
                reverse: true,
                scrollDirection: Axis.vertical,
                itemCount: 6,
                shrinkWrap: true,
                physics: AlwaysScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Shimmer.fromColors(
                    baseColor: Colors.grey[400],
                    highlightColor: Colors.grey[100],
                    child: ClipRRect(
                        borderRadius:
                            BorderRadius.all(Radius.circular(spacing_middle)),
                        child: Container(
                          color: Colors.black,
                        )),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget listDataNull(width, FeedSuccessLoad data) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: spacing_standard_new,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(spacing_middle),
                    bottomRight: Radius.circular(spacing_standard_new)),
                child: Image.memory(
                  fileFoto,
                  fit: BoxFit.cover,
                  height: width * 0.35,
                  // cacheHeight: 300,
                ),
              ),
            ),
            SizedBox(
              width: spacing_standard_new,
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: text("$namaUser",
                        fontFamily: fontMedium, fontSize: textSizeLargeMedium),
                  ),
                  SizedBox(
                    height: spacing_control,
                  ),
                  Row(
                    children: <Widget>[
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PostAlumni(),
                              ));
                        },
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0),
                        ),
                        padding: const EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(0),
                            ),
                          ),
                          child: Container(
                            constraints: const BoxConstraints(
                                minWidth: 150.0, minHeight: 36.0),
                            alignment: Alignment.center,
                            child: const Text(
                              'Tambah Data',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: spacing_standard,
                  ),
                  // Row(
                  //   children: <Widget>[
                  //     (idUser != 59)?RaisedButton(
                  //       onPressed: () {
                  //         Navigator.push(
                  //             context,
                  //             MaterialPageRoute(
                  //               builder: (context) => EditFoto(),
                  //             ));
                  //       },
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(80.0),
                  //       ),
                  //       padding: const EdgeInsets.all(0.0),
                  //       child: Ink(
                  //         decoration: const BoxDecoration(
                  //           color: Colors.white,
                  //           borderRadius: BorderRadius.all(
                  //             Radius.circular(0),
                  //           ),
                  //         ),
                  //         child: Container(
                  //           constraints: const BoxConstraints(
                  //               minWidth: 150.0, minHeight: 36.0),
                  //           alignment: Alignment.center,
                  //           child: const Text(
                  //             'Edit Foto',
                  //             textAlign: TextAlign.center,
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontWeight: FontWeight.bold,
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ):Container(),
                  //     SizedBox(width:5),
                  //     (idUser != 59)?RaisedButton(
                  //       onPressed: () {
                  //         Navigator.push(
                  //             context,
                  //             MaterialPageRoute(
                  //               builder: (context) => EditFoto(),
                  //             ));
                  //       },
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(80.0),
                  //       ),
                  //       padding: const EdgeInsets.all(0.0),
                  //       child: Ink(
                  //         decoration: const BoxDecoration(
                  //           color: Colors.white,
                  //           borderRadius: BorderRadius.all(
                  //             Radius.circular(0),
                  //           ),
                  //         ),
                  //         child: Container(
                  //           constraints: const BoxConstraints(
                  //               minWidth: 150.0, minHeight: 36.0),
                  //           alignment: Alignment.center,
                  //           child: const Text(
                  //             'Edit Alumni',
                  //             textAlign: TextAlign.center,
                  //             style: TextStyle(
                  //               color: Colors.black,
                  //               fontWeight: FontWeight.bold,
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ):Container()
                  //   ],
                  // )
                ],
              ),
            )
          ],
        ),
        Container(
          margin: EdgeInsets.all(spacing_standard_new),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              text("Lemdik",
                  fontFamily: fontMedium, fontSize: textSizeLargeMedium),
              text("$namaLemdik",
                  textColor: t10_textColorSecondary, isLongText: true),
              SizedBox(
                height: spacing_standard_new,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // text(theme10_photos + " and " + theme10_videos,
                  //     fontFamily: fontMedium,
                  //     fontSize: textSizeLargeMedium),
                  // RichText(
                  //   text: TextSpan(
                  //     children: [
                  //       TextSpan(
                  //           text: theme10_view_all,
                  //           style: TextStyle(
                  //               fontSize: textSizeMedium,
                  //               fontFamily: fontMedium,
                  //               color: t10_textColorSecondary)),
                  //       WidgetSpan(
                  //         child: Padding(
                  //           padding: EdgeInsets.only(left: 0),
                  //           child: Icon(
                  //             Icons.keyboard_arrow_right,
                  //             color: t10_textColorPrimary,
                  //             size: 18,
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
              // BlocListener<DetailprofilBloc, DetailprofilState>(
              //   bloc: blocDetailProfil,
              //   listener: (context, state) {
              //     if (state is GetDetailSukses) {
              //       Fluttertoast.showToast(
              //           msg: "Sukses ambil data profil");
              //           setState(() {
              //             fileData = state.listAlumni[0];
              //           });
              //     }
              //   },
              //   child: Container(
              //     child: BlocBuilder<DetailprofilBloc,
              //         DetailprofilState>(
              //       bloc: blocDetailProfil,
              //       builder: (context, state) {
              //         if (state is GetDetailSukses) {
              //           print("sini");
              //           print(state.listAlumni);
              //           print(state.listAlumni.length);
              //           if (state.listAlumni.length != 0) {
              //             return _columnDetailProfil(state);
              //           } else {
              //             return Container();
              //           }
              //         }
              //         return Container();
              //       },
              //     ),
              //   ),
              // ),
              // _columnDetailProfil(state),
              SizedBox(
                height: spacing_standard_new,
              ),
              ToggleSwitch(
                fontSize: 12.0,
                minWidth: width,
                initialLabelIndex: initialIndex,
                // cornerRadius: 20.0,
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                labels: ['Galeri', 'Informasi', 'Proper'],
                icons: [
                  Icons.border_all,
                  Icons.assignment_ind,
                  Icons.menu_book
                ],
                activeBgColors: [Colors.blue, Colors.blue, Colors.blue],
                onToggle: (index) {
                  print('switched to: $index');
                  setState(() {
                    initialIndex = index;
                  });
                },
              ),
              SizedBox(
                height: spacing_middle,
              ),
              (initialIndex == 0)
                  ? buildGridViewFoto(data)
                  : (initialIndex == 1)
                      ? cardView()
                      : (initialIndex == 2)
                          ? ListProper()
                          : Container(
                              child: Center(child: Text("Data Kosong"))),
              SizedBox(height: 50),
              (statusSCroll == true ||
                      initialIndex != 0 ||
                      data.listPost.length == 0)
                  ? Container()
                  : Container(
                      child: Center(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
              // Container(
              //     margin: EdgeInsets.only(top: 16),
              //     alignment: Alignment.topCenter,
              //     child: SimpleFoldingCell(
              //         key: _foldingCellKey,
              //         frontWidget: _buildFrontWidget(),
              //         innerTopWidget: _buildInnerTopWidget(),
              //         innerBottomWidget:
              //             _buildInnerBottomWidget(),
              //         cellSize: Size(
              //             MediaQuery.of(context).size.width,
              //             125),
              //         padding: EdgeInsets.all(15),
              //         animationDuration:
              //             Duration(milliseconds: 300),
              //         borderRadius: 10,
              //         onOpen: () => print('cell opened'),
              //         onClose: () => print('cell closed')),
              //   ),
              // ListPosts(tabController: _tabController, data: data),
              // Container(child: Text("sini"))
              // Text(data.listPost[0].stringGambar),
            ],
          ),
        ),
      ],
    );
  }

  Widget listData(width, GetDetailSukses state, FeedSuccessLoad data) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: spacing_standard_new,
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(spacing_middle),
                    bottomRight: Radius.circular(spacing_standard_new)),
                child: Image.memory(
                  state.listAlumni[0].gambarBase64,
                  fit: BoxFit.cover,
                  height: width * 0.35,
                  // cacheHeight: 300,
                ),
              ),
            ),
            SizedBox(
              width: spacing_standard_new,
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: text("$namaUser",
                        fontFamily: fontMedium, fontSize: textSizeLargeMedium),
                  ),
                  SizedBox(
                    height: spacing_standard,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      EditFoto(dataList: state.listAlumni[0]),
                                ));
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0),
                          ),
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(0),
                              ),
                            ),
                            child: Container(
                              constraints: const BoxConstraints(
                                  minWidth: 150.0, minHeight: 36.0),
                              alignment: Alignment.center,
                              child: const Text(
                                'Edit Foto',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      Flexible(
                        flex: 1,
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      EditAlumni(myData: state.listAlumni[0]),
                                ));
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0),
                          ),
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(
                                Radius.circular(0),
                              ),
                            ),
                            child: Container(
                              constraints: const BoxConstraints(
                                  minWidth: 150.0, minHeight: 36.0),
                              alignment: Alignment.center,
                              child: const Text(
                                'Edit Alumni',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
        Container(
          margin: EdgeInsets.all(spacing_standard_new),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              text("Lemdik",
                  fontFamily: fontMedium, fontSize: textSizeLargeMedium),
              text("$namaLemdik",
                  textColor: t10_textColorSecondary, isLongText: true),
              SizedBox(
                height: spacing_standard_new,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // text(theme10_photos + " and " + theme10_videos,
                  //     fontFamily: fontMedium,
                  //     fontSize: textSizeLargeMedium),
                  // RichText(
                  //   text: TextSpan(
                  //     children: [
                  //       TextSpan(
                  //           text: theme10_view_all,
                  //           style: TextStyle(
                  //               fontSize: textSizeMedium,
                  //               fontFamily: fontMedium,
                  //               color: t10_textColorSecondary)),
                  //       WidgetSpan(
                  //         child: Padding(
                  //           padding: EdgeInsets.only(left: 0),
                  //           child: Icon(
                  //             Icons.keyboard_arrow_right,
                  //             color: t10_textColorPrimary,
                  //             size: 18,
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                ],
              ),
              // BlocListener<DetailprofilBloc, DetailprofilState>(
              //   bloc: blocDetailProfil,
              //   listener: (context, state) {
              //     if (state is GetDetailSukses) {
              //       Fluttertoast.showToast(
              //           msg: "Sukses ambil data profil");
              //           setState(() {
              //             fileData = state.listAlumni[0];
              //           });
              //     }
              //   },
              //   child: Container(
              //     child: BlocBuilder<DetailprofilBloc,
              //         DetailprofilState>(
              //       bloc: blocDetailProfil,
              //       builder: (context, state) {
              //         if (state is GetDetailSukses) {
              //           print("sini");
              //           print(state.listAlumni);
              //           print(state.listAlumni.length);
              //           if (state.listAlumni.length != 0) {
              //             return _columnDetailProfil(state);
              //           } else {
              //             return Container();
              //           }
              //         }
              //         return Container();
              //       },
              //     ),
              //   ),
              // ),
              _columnDetailProfil(state),
              SizedBox(
                height: spacing_standard_new,
              ),
              ToggleSwitch(
                fontSize: 12.0,
                minWidth: width,
                initialLabelIndex: initialIndex,
                // cornerRadius: 20.0,
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                labels: ['Galeri', 'Informasi', 'Proper'],
                icons: [
                  Icons.border_all,
                  Icons.assignment_ind,
                  Icons.menu_book
                ],
                activeBgColors: [Colors.blue, Colors.blue, Colors.blue],
                onToggle: (index) {
                  print('switched to: $index');
                  setState(() {
                    initialIndex = index;
                  });
                },
              ),
              SizedBox(
                height: spacing_middle,
              ),
              (initialIndex == 0)
                  ? buildGridViewFoto(data)
                  : (initialIndex == 1)
                      ? cardView()
                      : (initialIndex == 2)
                          ? ListProper()
                          : Container(
                              child: Center(child: Text("Data Kosong"))),
              SizedBox(height: 50),
              (statusSCroll == true ||
                      initialIndex != 0 ||
                      data.listPost.length == 0)
                  ? Container()
                  : Container(
                      child: Center(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
              // Container(
              //     margin: EdgeInsets.only(top: 16),
              //     alignment: Alignment.topCenter,
              //     child: SimpleFoldingCell(
              //         key: _foldingCellKey,
              //         frontWidget: _buildFrontWidget(),
              //         innerTopWidget: _buildInnerTopWidget(),
              //         innerBottomWidget:
              //             _buildInnerBottomWidget(),
              //         cellSize: Size(
              //             MediaQuery.of(context).size.width,
              //             125),
              //         padding: EdgeInsets.all(15),
              //         animationDuration:
              //             Duration(milliseconds: 300),
              //         borderRadius: 10,
              //         onOpen: () => print('cell opened'),
              //         onClose: () => print('cell closed')),
              //   ),
              // ListPosts(tabController: _tabController, data: data),
              // Container(child: Text("sini"))
              // Text(data.listPost[0].stringGambar),
            ],
          ),
        ),
      ],
    );
  }

  Widget _columnDetailProfil(GetDetailSukses modelProfil) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Expertise"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].expertise.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(
                      width: 100, child: Text("Tempat, tanggal lahir"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].tempatLahir.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Jenis Kelamin"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].jenisKelamin.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3, child: Container(width: 100, child: Text("Agama"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].agama.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Pangkat"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].pangkat.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Asal Instansi"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].asalInstansi.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Alamat Kantor"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].alamatKantor.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("No Telp"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].noTelp.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Nomor Kra"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].nomorKra.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Jabatan"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].jabatan.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  flex: 3,
                  child: Container(width: 100, child: Text("Golongan"))),
              Flexible(flex: 1, child: Container(child: Text(":"))),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      text(modelProfil.listAlumni[0].gol.toString(),
                          fontSize: textSizeMedium,
                          maxLine: 2,
                          textColor: t1TextColorPrimary),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ListView cardView() {
    return ListView.builder(
        itemCount: InforCard().list.length,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return CardOpen(nama: InforCard().list[index], indexTo: index);
        });
  }

  Widget buildGridViewFoto(FeedSuccessLoad data) {
    return GridView.builder(
      // reverse: true,
      scrollDirection: Axis.vertical,
      itemCount: data.listPost.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return (data.listPost[index].url == null ||
                data.listPost[index].url == "")
            ? InkWell(
                onLongPress: () {
                  _showAlert(context, data, index);
                },
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          FeedProfilePage(indexTo: index, dataList: data),
                    ),
                  );
                },
                child: ClipRRect(
                    borderRadius:
                        BorderRadius.all(Radius.circular(spacing_middle)),
                    child: Container(
                      color: Colors.black,
                      child: Image.memory(data.listPost[index].gambarBase64),
                    )),
              )
            : InkWell(
                onLongPress: () {
                  _showAlert(context, data, index);
                },
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          FeedProfilePage(indexTo: index, dataList: data),
                    ),
                  );
                },
                child: ClipRRect(
                  borderRadius:
                      BorderRadius.all(Radius.circular(spacing_middle)),
                  child: Container(
                    color: Colors.black,
                    child: Stack(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: YoutubePlayer(
                            // key: ObjectKey(_controller),
                            controller: new YoutubePlayerController(
                                initialVideoId: YoutubePlayer.convertUrlToId(
                                    data.listPost[index].url),
                                flags: YoutubePlayerFlags(autoPlay: false)),
                            actionsPadding: const EdgeInsets.only(left: 16.0),
                            bottomActions: [
                              CurrentPosition(),
                              const SizedBox(width: 10.0),
                              ProgressBar(isExpanded: true),
                              const SizedBox(width: 10.0),
                              RemainingDuration(),
                              FullScreenButton(),
                            ],
                          ),
                        ),
                        Positioned(
                            top: 0,
                            child: ClipRRect(
                              child: Image.memory(
                                data.listPost[index].gambarBase64,
                                cacheHeight: 150,
                                cacheWidth: 150,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ))
                      ],
                    ),
                  ),
                ),
              );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }

  Widget topBar(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerRight,
            end: Alignment.centerLeft,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        height: 60,
        margin: EdgeInsets.only(right: spacing_standard_new),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              color: Colors.white,
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            ),
            Center(
              child: text(
                "Profil",
                textColor: Colors.white,
                fontFamily: fontBold,
                fontSize: textSizeLargeMedium,
              ),
            ),
            SizedBox(width: 10)
          ],
        ),
      ),
    );
  }

  Future<void> _showAlert(
      BuildContext context, FeedSuccessLoad data, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: ShowDialogInProfil(
              data: data.listPost[index],
              gambar64: data.listPost[index].gambarBase64,
              indexTo: index,
            ),
          );
        });
  }
}
