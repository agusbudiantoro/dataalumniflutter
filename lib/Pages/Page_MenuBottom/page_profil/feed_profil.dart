import 'dart:io';
import 'dart:ui';

import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedGalery.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedYoutube.dart';
import 'package:dataAlumni/Pages/Widget/ShowCaption.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:like_button/like_button.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:dataAlumni/util/constant.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

const double itemHeight = 500;

class FeedProfilePage extends StatefulWidget {
  final int indexTo;
  final FeedSuccessLoad dataList;
  FeedProfilePage({@required this.indexTo, @required this.dataList});
  @override
  _FeedProfilePageState createState() =>
      _FeedProfilePageState(indexTo: this.indexTo, dataList: this.dataList);
}

class _FeedProfilePageState extends State<FeedProfilePage> {
  final int indexTo;
  final FeedSuccessLoad dataList;
  _FeedProfilePageState({@required this.indexTo, @required this.dataList});
  // ScrollController _controller = ScrollController();
  final bloc = FeedProfileBloc();
  File imageFile;
  bool scrollCoba = false;
  AutoScrollController _controller;
  final scrollDirection = Axis.vertical;

  void onScroll() {
    // double maxScroll = controller.position.maxScrollExtent;
    // double currentScroll = controller.position.pixels;

    // if (currentScroll == maxScroll) {
    // return bloc.add(FeedEvent());
    // }
    // _controller.animateTo(indexTo * itemHeight,
    //     duration: new Duration(seconds: 1), curve: Curves.ease);
  }

  void onScrollHabis() {
    Fluttertoast.showToast(msg: "data habis");
  }

  bool show = false;
  List<Feeds> feed1 = [
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user1.png',
      name: 'John Doe',
      feedImage: '$BaseUrl/images/food/food_ic_popular2.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user2.png',
      name: 'Carry Milton',
      feedImage: '$BaseUrl/images/food/food_ic_popular3.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user3.png',
      name: 'Jhonny Smith',
      feedImage: '$BaseUrl/images/food/food_ic_popular1.jpg',
    ),
  ];
  bool isActive;
  @override
  void initState() {
    super.initState();
    isActive = true;
    print(isActive);
    init();
    print("sini");
    print(indexTo);
    _controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);
    _scrollToIndex();
    // print(_controller.attach(position));
    // _controller.animateTo(indexTo * itemHeight,
    //     duration: new Duration(seconds: 2), curve: Curves.ease);
    // _controller.addListener(onScroll);
    // _controller.jumpTo(500);
    // onScroll();
    // _controller.initialScrollOffset;
  }

  init() async {
    await Future.delayed(
      Duration(seconds: 3),
    );
    setState(() {
      isActive = false;
    });
  }

  int counter = 2;
  Future _scrollToIndex() async {
    setState(() {
      // counter--;

      // if (counter >= maxCount)
      counter = indexTo;
    });

    await _controller.scrollToIndex(counter,
        preferPosition: AutoScrollPosition.begin);
    _controller.highlight(counter);
  }

//   You can set the initial value of the count.
  final int likeCount = 0;

//   Set thr Size of the Button/Icons.
  final double buttonSize = 30.0;

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    // return bloc.add(FeedRefresh());
  }


  @override
  Widget build(BuildContext context) {
    // bloc = BlocProvider.of<FeedProfileBloc>(context);

    double c_width = MediaQuery.of(context).size.width * 0.8;
    // controller.addListener(onScroll);
    return SafeArea(
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Text('Postingan'),
          leading: Container(
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back, color: Colors.white),
            ),
          ),
        ),
        body: buildListView(dataList),
      ),
    );
  }

  ListView buildListView(FeedSuccessLoad data) {
    final _komentarBlocBloc = KomentarBloc();
    // _controller.addListener(onScroll);
    // return LazyLoadScrollView(
      // return RefreshIndicator(
      //   onRefresh: reffreshIn,
        return ListView.builder(
          // reverse: true,
          scrollDirection: scrollDirection,
          controller: _controller,
          itemCount: data.listPost.length,
          itemBuilder: (BuildContext context, int index) {
            return AutoScrollTag(
              disabled: false,
              key: ValueKey(index),
              controller: _controller,
              index: index,
              child: (index < data.listPost.length)
                  ? Container(
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(
                          //                   <--- left side
                          color: Colors.grey,
                        ),
                      )),
                      margin: EdgeInsets.only(
                        top: 15,
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 15),
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: MemoryImage(data.listPost[index].fotoBase64),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    data.listPost[index].nama,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(
                                  Icons.more_vert,
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ),
                          (data.listPost[index].url == null ||
                                  data.listPost[index].url == "")
                              ? Container(
                                height: 300,
                                  margin: EdgeInsets.only(top: 15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Image.memory(
                                    data.listPost[index].gambarBase64,
                                    // cacheHeight: 300,
                                    // cacheWidth: 450,
                                  ))
                              : Container(
                                  margin: EdgeInsets.only(top: 15),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: YoutubePlayer(
                                    // data.listPost[index].url.substring(32)
                                    // key: ObjectKey(_controllers[index]),
                                    controller: new YoutubePlayerController(
                                        initialVideoId:
                                            YoutubePlayer.convertUrlToId(
                                                data.listPost[index].url),
                                        flags: YoutubePlayerFlags(
                                            autoPlay: false)),
                                    actionsPadding:
                                        const EdgeInsets.only(left: 16.0),
                                    bottomActions: [
                                      CurrentPosition(),
                                      const SizedBox(width: 10.0),
                                      ProgressBar(isExpanded: true),
                                      const SizedBox(width: 10.0),
                                      RemainingDuration(),
                                      FullScreenButton(),
                                    ],
                                  )),
                          Container(
                            margin: EdgeInsets.only(left: 5, top: 20),
                            child: Row(
                              children: <Widget>[
                                (data.listPost[index].cekLove == false)
                                    ? buildIconButtonLike(data, index)
                                    : buildIconButtonUnLike(data, index),
                                // LikeButton(
                                //   // size of the button
                                //   size: buttonSize,
                                //   // the total number of count
                                //   likeCount: data.listPost[index].love,
                                //   isLiked: data.listPost[index].cekLove,
                                //   // to handle the count behaviour
                                //   countBuilder:
                                //       (int count, bool isLiked, String text) {
                                //     // color of the count
                                //     isLiked = data.listPost[index].cekLove;
                                //     Text(isLiked.toString());
                                //     final ColorSwatch<int> color =
                                //         isLiked ? Colors.pinkAccent : Colors.grey;
                                //     Widget result;
                                //     if (count == 0) {
                                //       result = Text(
                                //         '0',
                                //         style: TextStyle(color: color),
                                //       );
                                //     } else
                                //       result = Text(
                                //         count >= 1000
                                //             ? (count / 1000.0).toStringAsFixed(1) +
                                //                 'k'
                                //             : text,
                                //         style: TextStyle(color: color),
                                //       );

                                //     return result;
                                //   },
                                //   // position of the count you want to show(i.e. TOP,BOTTOM,RIGHT,LEFT)
                                //   countPostion: CountPostion.right,
                                //   // Animation for the count
                                //   likeCountAnimationType: likeCount < 1000
                                //       ? LikeCountAnimationType
                                //           .all // the whole number will change
                                //       : LikeCountAnimationType.part,
                                //   // only the last digit will change
                                //   onTap: onLikeButtonTapped,
                                //   // onTap: likeButtonFungsi(),
                                // ),
                                buildInkWellKomentar(data, index),
                                SizedBox(width: 4),
                              ],
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 20, bottom: 10),
                              child: Row(
                                children: [
                                  Text(
                                      data.listPost[index].love.toString() +
                                          " Likes",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              (data.listPost[index].cekLove ==
                                                      true
                                                  ? Colors.red
                                                  : Colors.grey)))
                                ],
                              )),
                          buildColumnCaption(data, index),
                          // ShowCaption(data: data.listPost[index]),
                          buildContainerKomentar(data, index),
                        ],
                      ),
                    )
                  : Container(
                      child: Center(
                        child: SizedBox(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
            );
          },
        );
        // child: ListView.builder(
        //   controller: _controller,
        //   itemCount: data.listPost.length,
        //   scrollDirection: Axis.vertical,
        //   shrinkWrap: true,
        //   physics: AlwaysScrollableScrollPhysics(),
        //   itemBuilder: (BuildContext context, int index) =>
        // ),
      // );
    //   onEndOfPage: onScrollHabis,
    // );
  }

  Column buildColumnCaption(FeedSuccessLoad data, int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 15),
          child: data.listPost[index].showCaption
              ? Text(
                  data.listPost[index].caption,
                  textAlign: TextAlign.justify,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Text(
                        (data.listPost[index].caption.length < 50)
                            ? data.listPost[index].caption
                            : data.listPost[index].caption.substring(0, 50) +
                                '...',
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
        ),
        (data.listPost[index].caption.length < 50)
            ? Text("")
            : Container(
                margin: EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      data.listPost[index].showCaption =
                          !data.listPost[index].showCaption;
                    });
                    print(data.listPost[index].showCaption);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      data.listPost[index].showCaption
                          ? Text(
                              "Less",
                              style: TextStyle(color: Colors.blue),
                            )
                          : Text("More", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  Container buildContainerKomentar(FeedSuccessLoad data, int index) {
    return Container(
        margin: EdgeInsets.only(left: 20, bottom: 10),
        child: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Feed_Komentar(
                        data: data.listPost[index],
                        gambar64: data.listPost[index].gambarBase64),
                  ),
                );
              },
              child: Text(
                (data.listPost[index].jumkomentar == null)
                    ? "0 Komentar"
                    : "Lihat Semua " +
                        (data.listPost[index].jumkomentar).toString() +
                        " Komentar",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
          ],
        ));
  }

  IconButton buildIconButtonLike(FeedSuccessLoad data, int index) {
    return IconButton(
      padding: EdgeInsets.only(right: 0),
      icon: Icon(Icons.favorite),
      color: data.listPost[index].cekLove ? Colors.red : Colors.grey,
      onPressed: () {
        setState(() {
          print("cek love");
          print(data.listPost[index].cekLove);
          print(index);
          data.listPost[index].cekLove = true;
          data.listPost[index].love += 1;
          // if (data.listPost[index].cekLove == true) {
          //   // data.listPost[index].cekLove = false;
          //   // data.listPost[index].love -= 1;

          // } else {

          // }
          if (data.listPost[index].cekLove == true) {
            print("t");
            bloc.add(PostProfileLiked(
                  idPostingan: data.listPost[index].id, likeIndex: index));
          } else {
            print("y");
          }
          print("hasillike");
          print(data.listPost[index].cekLove);
        });
      },
    );
  }

  IconButton buildIconButtonUnLike(FeedSuccessLoad data, int index) {
    return IconButton(
      padding: EdgeInsets.only(right: 0),
      icon: Icon(Icons.favorite),
      color: data.listPost[index].cekLove ? Colors.red : Colors.grey,
      onPressed: () {
        setState(() {
          print("cek love");
          print(data.listPost[index].cekLove);
          print(index);
          data.listPost[index].cekLove = false;
          data.listPost[index].love -= 1;
          // if (data.listPost[index].cekLove == true) {
          //   // data.listPost[index].cekLove = false;
          //   // data.listPost[index].love -= 1;
          //   bloc
          //     ..add(UnLiked(
          //         idPostingan: data.listPost[index].id, likeIndex: index));
          // } else {
          //   bloc
          //     ..add(PostLiked(
          //         idPostingan: data.listPost[index].id, likeIndex: index));
          // }
          if (data.listPost[index].cekLove == false) {
            print("tu");
            bloc
              ..add(UnProfileLiked(
                  idPostingan: data.listPost[index].id, likeIndex: index));
          } else {
            print("yu");
          }
          print("hasil");
          print(data.listPost[index].cekLove);
        });
      },
    );
  }

  InkWell buildInkWellKomentar(FeedSuccessLoad data, yangKe) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Feed_Komentar(
                data: data.listPost[yangKe],
                gambar64: data.listPost[yangKe].gambarBase64),
          ),
        );
      },
      child: Icon(Icons.comment, color: Colors.grey),
    );
  }

  RefreshIndicator loadingStartPage() {
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: ListView.builder(
        reverse: true,
        itemCount: feed1.length,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: AlwaysScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return SizedBox(
            child: Shimmer.fromColors(
              baseColor: Colors.grey[400],
              highlightColor: Colors.grey[100],
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(
                    //                   <--- left side
                    color: Colors.black,
                    width: 1.0,
                  ),
                )),
                margin: EdgeInsets.only(
                  top: 15,
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 15, right: 10),
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            height: 8,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 10,
                          margin: EdgeInsets.only(right: 10),
                          color: Colors.grey,
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      height: 200,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    /// send your request here
    // final bool success= await sendRequest();

    /// if failed, you can do nothing
    // return success? !isLiked:isLiked;

    return !isLiked;
  }
}

class Feeds {
  String profileImg;
  String name;
  String feedImage;

  Feeds({this.profileImg, this.name, this.feedImage});
}
