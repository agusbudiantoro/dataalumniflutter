import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/InformasiUser/informasi_user_bloc.dart';
import 'package:dataAlumni/bloc/ProperById/properbyid_bloc.dart';
import 'package:dataAlumni/model/modeProper.dart';
import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:dataAlumni/util/model/informasiUser.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator/listingTheme1.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OlahProper extends StatefulWidget {
  final int posInd;
  final int idUserKu;
  static var tag = "/T1Listing";

  OlahProper({this.posInd, @required this.idUserKu});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OlahProperState(posInd: this.posInd, idUserKu: this.idUserKu);
  }
}

class OlahProperState extends State<OlahProper> {
  // for proper
  final TextEditingController _judul = TextEditingController();
  final TextEditingController _tahun = TextEditingController();
  final TextEditingController _lokasi = TextEditingController();
  final TextEditingController _abstrak = TextEditingController();
  final TextEditingController _kataKunci = TextEditingController();
  final TextEditingController _evidence = TextEditingController();
  // end proper

  int idData;
  int indexData;

  List<T1Model> mListings;
  final _bloc = ProperbyidBloc();
  final int posInd;
  final int idUserKu;
  OlahProperState({this.posInd, @required this.idUserKu});
  List<ModelProper> dataInProper;

  @override
  void initState() {
    super.initState();
    mListings = getListings();
    _bloc.add(GetProperById(id: idUserKu));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Kelola Proper",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            InkWell(
              onTap: () {
                _showAlert(context, _bloc);
              },
              child: Row(
                children: [
                  Center(
                      child: Text(
                    'Tambah',
                    style: TextStyle(color: Colors.black),
                  )),
                  Center(
                    child: Icon(
                      Icons.add_box_outlined,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        body: BlocListener<ProperbyidBloc, ProperbyidState>(
          bloc: _bloc,
          listener: (context, state) {
            if (state is GetProperSukses) {
              setState(() {
                dataInProper = state.listProper;
              });
              return buildContainer(state);
            }
            if (state is GetProperError) {
              Fluttertoast.showToast(msg: "gagal");
              return Center(
                  child: Container(child: CircularProgressIndicator()));
            }
          },
          child: BlocBuilder<ProperbyidBloc, ProperbyidState>(
            bloc: _bloc,
            builder: (context, state) {
              if (state is GetProperSukses) {
                return buildContainer(state);
              }
              if (state is GetProperWaiting) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
              if (state is GetProperError) {
                Fluttertoast.showToast(msg: "gagal");
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
              if (state is ProperbyidInitial) {
                return Center(
                    child: Container(child: CircularProgressIndicator()));
              }
            },
          ),
        ));
  }

  Container buildContainer(GetProperSukses data) {
    return Container(
      child: Column(
        children: <Widget>[
          // TopBar(t1_Listing, true),
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: data.listProper.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return T1ListItem(context, data, index);
                }),
          )
        ],
      ),
    );
  }

  Future<void> _showAlertHapus(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentHapus(context),
          );
        });
  }

  dialogContentHapus(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                margin: EdgeInsets.all(10),
                child: Text("Apakah sudah yakin ingin menghapus data?"),
              )
            ]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    print(_tahun.text);
                    print("woyy");
                    ModelProper dataPost = ModelProper(
                        id: idData,
                        judul: _judul.text.toString(),
                        abstrak: _abstrak.text.toString(),
                        kataKunci: _kataKunci.text.toString(),
                        evidence: _evidence.text.toString());
                        _bloc..add(HapusProper(indexData: indexData, dataProper: dataPost, dataProperOld: dataInProper));
                        Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.delete, color: Colors.redAccent),
                      Text("Hapus")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showAlertEdit(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContentEdit(context),
          );
        });
  }

  dialogContentEdit(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _judul,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Judul ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _abstrak,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Abstrak ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _kataKunci,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Kata Kunci ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _evidence,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Evidence ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _tahun,
                    keyboardType: TextInputType.multiline,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Tahun ...."),
                  )),
            ]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    var tahun1 = int.parse(_tahun.text);
                    ModelProper dataPost = ModelProper(
                        id: idData,
                        judul: _judul.text.toString(),
                        tahun: tahun1,
                        abstrak: _abstrak.text.toString(),
                        kataKunci: _kataKunci.text.toString(),
                        evidence: _evidence.text.toString());
                    _bloc
                      ..add(EditProper(
                          indexData: indexData,
                          dataProper: dataPost,
                          dataProperOld: dataInProper));
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.save,
                        color: Colors.blueAccent,
                      ),
                      Text("Simpan")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showAlert(BuildContext context, ProperbyidBloc _bloc) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: dialogContent(context, _bloc),
          );
        });
  }

  dialogContent(BuildContext context, ProperbyidBloc _bloc) {
    return Container(
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _judul,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Judul ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _abstrak,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Abstrak ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _kataKunci,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Kata Kunci ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _evidence,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Evidence ...."),
                  )),
              SizedBox(
                height: 5,
              ),
              Flexible(
                  flex: 2,
                  child: TextField(
                    controller: _tahun,
                    keyboardType: TextInputType.multiline,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    minLines: 1,
                    maxLines: 5,
                    decoration: InputDecoration(hintText: "Tahun ...."),
                  )),
            ]),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    print("woyy");
                    print(widget.posInd);
                    var tahun1 = int.parse(_tahun.text);
                    ModelProper dataPost = ModelProper(
                        judul: _judul.text.toString(),
                        tahun: tahun1,
                        abstrak: _abstrak.text.toString(),
                        kataKunci: _kataKunci.text.toString(),
                        evidence: _evidence.text.toString());
                    _bloc
                      ..add(AddProper(
                          dataProper: dataPost, dataProperOld: dataInProper));
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(
                        Icons.save,
                        color: Colors.blueAccent,
                      ),
                      Text("Simpan")
                    ],
                  ),
                ),
                SizedBox(width: 50),
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Row(
                    children: [
                      Icon(Icons.close, color: Colors.grey),
                      Text("Close")
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget T1ListItem(BuildContext context, GetProperSukses model, int pos) {
    var width = MediaQuery.of(context).size.width;
    return Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Container(
          decoration: boxDecoration(radius: 10, showShadow: true),
          child: Column(
            children: [
              Stack(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(16),
                      child: widgetProper(model, pos)),
                  Container(
                    width: 4,
                    height: 35,
                    margin: EdgeInsets.only(top: 16),
                    color: pos % 2 == 0 ? t1TextColorPrimary : t1_colorPrimary,
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () {
                          _showAlertEdit(context);
                          setState(() {
                            _judul.text = model.listProper[pos].judul;
                            _tahun.text =
                                model.listProper[pos].tahun.toString();
                            _abstrak.text =
                                model.listProper[pos].abstrak.toString();
                            _kataKunci.text =
                                model.listProper[pos].kataKunci.toString();
                            _evidence.text =
                                model.listProper[pos].evidence.toString();
                            idData = model.listProper[pos].id;
                            indexData = pos;
                          });
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.edit,
                              color: Colors.orange,
                            ),
                            Text("Edit")
                          ],
                        ),
                      ),
                      SizedBox(width: 50),
                      InkWell(
                        onTap: () {
                          _showAlertHapus(context);
                          setState(() {
                            idData = model.listProper[pos].id;
                            indexData = pos;
                          });
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.delete_forever,
                              color: Colors.red,
                            ),
                            Text("Hapus")
                          ],
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ));
  }

  Widget widgetProper(GetProperSukses data, int ind) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Judul"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.listProper[ind].judul.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Abstrak"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.listProper[ind].abstrak.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Kata Kunci"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.listProper[ind].kataKunci.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3,
                child: Container(width: 100, child: Text("Evidence:"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.listProper[ind].evidence.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: <Widget>[
            Flexible(
                flex: 3, child: Container(width: 100, child: Text("Tahun"))),
            Flexible(flex: 1, child: Container(child: Text(":"))),
            Expanded(
              flex: 6,
              child: Container(
                padding: EdgeInsets.only(left: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(data.listProper[ind].tahun.toString(),
                        fontSize: textSizeMedium,
                        maxLine: 2,
                        textColor: t1TextColorPrimary),
                  ],
                ),
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
      ],
    );
  }
}
