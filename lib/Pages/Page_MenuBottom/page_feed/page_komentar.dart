import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_feed.dart';
import 'package:dataAlumni/Pages/Widget/Friend_komentar.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
// import 'package:dataAlumni/bloc/Feed/KomentarBloc/komentar_bloc_bloc.dart' as state1;
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/childKomentarBloc/childkomentar_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:dataAlumni/model/feed/komentar_model.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/DataGenerator.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/constant.dart';
import 'package:dataAlumni/model/feed/feed_model.dart';
// import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Widget/boxKomen.dart';
import 'package:dataAlumni/util/model/Dashed.dart';
import 'package:dataAlumni/util/model/Social_model.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:like_button/like_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:date_format/date_format.dart';
import 'dart:convert';

class Feed_Komentar extends StatefulWidget {
  final int idKomentarBalasan;
  final CloningPosting data;
  final List<KomentarModel> stateKomen;
  final state1;
  final bool nilaiBalesan;
  final Uint8List gambar64;

  Feed_Komentar(
      {this.data,
      this.stateKomen,
      this.state1,
      this.idKomentarBalasan,
      this.nilaiBalesan,
      this.gambar64});
  @override
  _Feed_KomentarState createState() => _Feed_KomentarState(
      data: this.data,
      stateKomen: this.stateKomen,
      state1: this.state1,
      idKomentarBalasan: this.idKomentarBalasan,
      nilaiBalesan: this.nilaiBalesan,
      gambar64: this.gambar64);
}

class _Feed_KomentarState extends State<Feed_Komentar> {
  _Feed_KomentarState(
      {this.data,
      this.stateKomen,
      this.state1,
      this.idKomentarBalasan,
      this.nilaiBalesan,
      this.gambar64});
  // final Uint8List gambar64;
  final CloningPosting data;
  final state1;
  final int idKomentarBalasan;
  final bool nilaiBalesan;
  final List<KomentarModel> stateKomen;
  List<SocialUser> mList;
  final _bloc = KomentarBloc();
  final blocFeed = FeedBloc();
  final TextEditingController _isiKomentar = TextEditingController();
  final Uint8List gambar64;
  bool isActive;
  ScrollController listController = ScrollController();
  String komentarSiapa;
  int komentarSiapaId;
  bool reply = false;
  bool showBalasan = false;
  int jumlahKomen = 0;
  Uint8List dataFoto;

  List<Feeds> feed1 = [
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user1.png',
      name: 'John Doe',
      feedImage: '$BaseUrl/images/food/food_ic_popular2.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user2.png',
      name: 'Carry Milton',
      feedImage: '$BaseUrl/images/food/food_ic_popular3.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user3.png',
      name: 'Jhonny Smith',
      feedImage: '$BaseUrl/images/food/food_ic_popular1.jpg',
    ),
  ];
  YoutubePlayerController _controller;
  @override
  void initState() {
    super.initState();
    isActive = true;
    print(isActive);
    init();
    ambilGambat();
    mList = addStatusData();
    print(state1);
    _bloc..add(KomentarBlocEvent(idPosting: data.id));
    listController = new ScrollController(initialScrollOffset: 1111100.0);
    // print("gambar");
    // print(gambar64);
    // if (data.url != null || data.url != "") {
    //   _controller = YoutubePlayerController(
    //     initialVideoId: YoutubePlayer.convertUrlToId(data.url),
    //     flags: YoutubePlayerFlags(
    //       autoPlay: true,
    //       mute: true,
    //     ),
    //   );
    // } else {
    //   print("url kosong");
    // }
  }

  ambilGambat() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    dataFoto = base64Decode(preferences.getString("fotoProfil"));
  }

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    _bloc..add(KomentarBlocEvent(idPosting: data.id));
  }

  init() async {
    await Future.delayed(
      Duration(seconds: 3),
    );
    setState(() {
      isActive = false;
    });
  }

  void backPage() {
    setState(() {
      FeedPage();
    });
  }

  bool show = false;

//   You can set the initial value of the count.
  final int likeCount = 999;

//   Set thr Size of the Button/Icons.
  final double buttonSize = 30.0;
  @override
  Widget build(BuildContext context) {
    // blocFeed = BlocProvider.of<FeedBloc>(context);
    return Scaffold(
      appBar: GradientAppBar(
        backgroundColorStart: Color(0xFF398AE5),
        backgroundColorEnd: Color(0xFF61A4F1),
        title: Text("Komentar"),
        leading: Container(
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        ),
        elevation: 0,
      ),
      body: SafeArea(
        child: Column(children: [
          Container(
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(
                //                   <--- left side
                color: Colors.grey,
              ),
            )),
            margin: EdgeInsets.only(
              top: 15,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 15),
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: MemoryImage(data.fotoBase64),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          data.nama,
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      // child: Icon(
                      //   Icons.more_vert,
                      //   color: Colors.black,
                      // ),
                    )
                  ],
                ),
                // (data.url == null || data.url == "")
                //     ? Container(
                //         margin: EdgeInsets.only(top: 15),
                //         decoration: BoxDecoration(
                //           borderRadius: BorderRadius.circular(15),
                //         ),
                //         child: Image.memory(
                //           gambar64,
                //           cacheHeight: 250,
                //           cacheWidth: 450,
                //         ))
                //     : Container(
                //         margin: EdgeInsets.only(top: 15),
                //         decoration: BoxDecoration(
                //           borderRadius: BorderRadius.circular(15),
                //         ),
                //         child: YoutubePlayer(
                //           // key: ObjectKey(_controller),
                //           controller: new YoutubePlayerController(
                //               initialVideoId:
                //                   YoutubePlayer.convertUrlToId(data.url),
                //               flags: YoutubePlayerFlags(autoPlay: false)),
                //           actionsPadding: const EdgeInsets.only(left: 16.0),
                //           bottomActions: [
                //             CurrentPosition(),
                //             const SizedBox(width: 10.0),
                //             ProgressBar(isExpanded: true),
                //             const SizedBox(width: 10.0),
                //             RemainingDuration(),
                //             FullScreenButton(),
                //           ],
                //         )),
                Container(
                  margin: EdgeInsets.only(left: 15, top: 20),
                  child: Row(
                    children: <Widget>[
                      // BlocListener<FeedBloc, FeedState>(
                      //   bloc: blocFeed,
                      //   listener: (context, state) {
                      //     print(state);
                      //     print("cobastate");
                      //     print(data.love);
                      //     if (state is FeedLoaded) {
                      //       if (state.statusLike == true) {
                      //         setState(() {
                      //           data.cekLove = true;
                      //           data.love += 1;
                      //           state.indLike = null;
                      //         });
                      //         print("berhasil true");
                      //       } else if (state.statusUnLike == true) {
                      //         print("gagal");
                      //         setState(() {
                      //           print(data.love);
                      //           data.cekLove = false;
                      //           data.love -= 1;
                      //           state.indLike = null;
                      //         });
                      //       }
                      //     }
                      //     if (state is FeedError) {
                      //       Fluttertoast.showToast(msg: "koneksi gagal");
                      //     }
                      //   },
                      //   child: Container(
                      //     child: buildLikeButton(),
                      //   ),
                      // ),
                      // SizedBox(width: 10),
                      Icon(Icons.comment, color: Colors.grey),
                      SizedBox(width: 4),
                      Text(
                        "$jumlahKomen",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, right: 15, top: 10),
                  child: show
                      ? Text(
                          data.caption,
                          textAlign: TextAlign.justify,
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                              flex: 2,
                              child: Text(
                                (data.caption.length < 50)
                                    ? data.caption
                                    : data.caption.substring(0, 50) + "....",
                                textAlign: TextAlign.justify,
                              ),
                            ),
                          ],
                        ),
                ),
                (data.caption.length < 50)
                    ? Text("")
                    : Container(
                        margin: EdgeInsets.only(right: 15, bottom: 20),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              show = !show;
                            });
                            print(show);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              show
                                  ? Text(
                                      "Less",
                                      style: TextStyle(color: Colors.blue),
                                    )
                                  : Text("More",
                                      style: TextStyle(color: Colors.blue))
                            ],
                          ),
                        ),
                      ),
              ],
            ),
          ),
          BlocBuilder<KomentarBloc, KomentarBlocState>(
            bloc: _bloc,
            builder: (context, state) {
              // if (state is GetKomentarBlocInitial) {
              //   return CircularProgressIndicator();
              // }
              if (state is GetKomentarLoadState) {
                // print(state.listChildKomentar.length);
                jumlahKomen = state.listKomentar.length;
                print(state.listKomentar.length);
                return listKomen(state);
              }
              if (state is GetKomentarLoadErrorState) {
                Fluttertoast.showToast(msg: state.errorMessage);
                Fluttertoast.showToast(msg: "Memuat data Gagal");
              }
              return Container();
            },
          )
        ]),
      ),
      bottomNavigationBar: TextFieldKomen(),
    );
  }

  LikeButton buildLikeButton() {
    return LikeButton(
      isLiked: data.cekLove,
      // size of the button
      size: buttonSize,
      // the total number of count
      likeCount: data.love,
      // to handle the count behaviour
      countBuilder: (int count, bool isLiked, String text) {
        // color of the count
        final ColorSwatch<int> color =
            isLiked ? Colors.pinkAccent : Colors.grey;
        Widget result;
        if (count == 0) {
          result = Text(
            '0',
            style: TextStyle(color: color),
          );
        } else
          result = Text(
            count >= 1000 ? (count / 1000.0).toStringAsFixed(1) + 'k' : text,
            style: TextStyle(color: color),
          );
        return result;
      },
      // position of the count you want to show(i.e. TOP,BOTTOM,RIGHT,LEFT)
      countPostion: CountPostion.right,
      // Animation for the count
      likeCountAnimationType: likeCount < 1000
          ? LikeCountAnimationType.all // the whole number will change
          : LikeCountAnimationType.part,
      // only the last digit will change
      onTap: onLikeButtonTapped,
    );
  }

  buildButtonLike(isLiked) {
    if (isLiked == false) {
      blocFeed..add(PostLiked(idPostingan: data.id));
    } else if (isLiked == true) {
      blocFeed..add(UnLiked(idPostingan: data.id));
    }
  }

  Widget TextFieldKomen() {
    return Transform.translate(
      offset: Offset(0.0, -1 * MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        height: 85.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              offset: Offset(0, -2),
              blurRadius: 6.0,
            ),
          ],
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: TextField(
            controller: _isiKomentar,
            decoration: InputDecoration(
              border: InputBorder.none,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.grey),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: BorderSide(color: Colors.grey),
              ),
              contentPadding: EdgeInsets.all(20.0),
              hintText: ' Add a comment',
              prefixIcon: Container(
                margin: EdgeInsets.all(4.0),
                width: 48.0,
                height: 48.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(0, 2),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: (dataFoto != null)
                    ? CircleAvatar(
                        child: ClipOval(
                          child: Image.memory(
                            dataFoto,
                            fit: BoxFit.cover,
                            width: 48.0,
                            height: 48.0,
                          ),
                        ),
                      )
                    : CircleAvatar(
                        child: ClipOval(
                          child: CachedNetworkImage(
                            height: 48.0,
                            width: 48.0,
                            imageUrl: feed1[1].profileImg,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
              ),
              suffixIcon: Container(
                  margin: EdgeInsets.only(right: 4.0),
                  width: 20.0,
                  child: BlocListener<KomentarBloc, KomentarBlocState>(
                    bloc: _bloc,
                    listener: (context, state) {
                      if (state is GetKomentarLoadErrorState) {
                        Fluttertoast.showToast(msg: state.errorMessage);
                        Fluttertoast.showToast(msg: "Memuat data Gagal");
                      }
                      if (state is GetKomentarLoadState) {
                        Fluttertoast.showToast(msg: "Success Memuat");
                        setState(() {
                          _isiKomentar.clear();
                        });
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //       builder: (context) => Feed_Komentar(
                        //         data: data.listPost[index],
                        //         ytUrl: _controllersN[index],
                        //         listKomentar: state.listKomentar,
                        //         myIdKomentator: state.idPosting,
                        //       ),
                        //     ));
                      }
                      if (state is GetKomentarLoadWaiting) {
                        Fluttertoast.showToast(msg: "Loading data");
                      }
                    },
                    child: Container(
                      child: BlocBuilder<KomentarBloc, KomentarBlocState>(
                        bloc: _bloc,
                        builder: (context, state) {
                          if (state is GetKomentarLoadWaiting) {
                            return CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            );
                          }
                          if (state is GetKomentarLoadErrorState) {
                            return buildFlatButtonSendKomen(_bloc);
                          }
                          if (state is GetKomentarLoadState) {
                            return buildFlatButtonSendKomen(_bloc);
                          }
                          return buildFlatButtonSendKomen(_bloc);
                        },
                      ),
                    ),
                  )),
            ),
          ),
        ),
      ),
    );
  }

  FlatButton buildFlatButtonSendKomen(KomentarBloc bloc) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      color: Colors.white,
      onPressed: () {
        bloc
          ..add(PostKomentarEvent(
              idPostingan: data.id, isiKomentar: _isiKomentar.text));
      },
      child: Icon(
        Icons.send,
        size: 25.0,
        color: food_colorPrimaryDark,
      ),
    );
  }

  Widget listKomen(GetKomentarLoadState data) {
    // controllerr.addListener(autoScroll);
    return Expanded(
      child: RefreshIndicator(
        onRefresh: reffreshIn,
        child: SingleChildScrollView(
          controller: listController,
          child: Container(
            decoration: boxDecorationKomen(showShadow: true),
            padding: EdgeInsets.all(spacing_standard_new),
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: data.listKomentar.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  DateTime todayDate =
                      DateTime.parse(data.listKomentar[index].updatedOn);
                  var updateOn = formatDate(todayDate.toLocal(), [
                    yyyy,
                    '/',
                    mm,
                    '/',
                    dd,
                    ' ',
                    hh,
                    ':',
                    nn,
                    ':',
                    ss,
                    ' ',
                    am
                  ]);
                  return Komen_Widget(
                      model: data.listKomentar[index],
                      pos: index,
                      tanggalPost: updateOn);
                }),
          ),
        ),
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    /// send your request here
    final bool success = await buildButtonLike(isLiked);

    /// if failed, you can do nothing
    // return success? !isLiked:isLiked;
    return !isLiked;
  }
}

class Feeds {
  String profileImg;
  String name;
  String feedImage;

  Feeds({this.profileImg, this.name, this.feedImage});
}
