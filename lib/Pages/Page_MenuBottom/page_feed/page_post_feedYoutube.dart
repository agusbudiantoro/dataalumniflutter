import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class AddYoutube extends StatefulWidget {
  @override
  _AddYoutubeState createState() => _AddYoutubeState();
}

class _AddYoutubeState extends State<AddYoutube> {
  final TextEditingController _caption = TextEditingController();
  final TextEditingController _judul = TextEditingController();
  final TextEditingController _subjudul = TextEditingController();
  final TextEditingController _urlY = TextEditingController();
  final _postBloc = FeedBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GradientAppBar(
            backgroundColorStart: Color(0xFF398AE5),
            backgroundColorEnd: Color(0xFF61A4F1),
            title: Text('Postingan Baru'),
            leading: Container(
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => Home()),
                  // );
                },
                child: Icon(Icons.arrow_back_ios, color: Colors.white),
              ),
            ),
            actions: <Widget>[
              BlocListener<FeedBloc, FeedState>(
                bloc: _postBloc,
                listener: (context, state) {
                  if (state is PostSukses) {
                    Fluttertoast.showToast(msg: "Success posting");
                    Navigator.of(context).pop(true);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()));
                  }
                  if (state is PostError) {
                    Fluttertoast.showToast(msg: "Posting Gagal");
                  }
                  if (state is PostWaiting) {
                    return Container(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
                child: BlocBuilder<FeedBloc, FeedState>(
                  bloc: _postBloc,
                  builder: (context, state) {
                    // if (state is FeedLoaded) {
                    //   return buildIconButtonPost(_postBloc);
                    // }
                    if (state is FeedError) {
                      Fluttertoast.showToast(msg: "koneksi gagal");
                    }
                    if(state is PostWaiting){
                      return Container(child: CircularProgressIndicator());
                    }
                    return buildIconButtonPost(_postBloc);
                  },
                ),
              )
            ]),
        body: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Flexible(
                    flex: 2,
                    child: TextField(
                      controller: _urlY,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(hintText: "URL Youtube.."),
                    )),
                Flexible(
                    flex: 2,
                    child: TextField(
                      controller: _caption,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(hintText: "Caption ...."),
                    )),
                    Flexible(
                    flex: 2,
                    child: TextField(
                      controller: _judul,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(hintText: "Judul ...."),
                    )),
                    Flexible(
                    flex: 2,
                    child: TextField(
                      controller: _subjudul,
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 5,
                      decoration: InputDecoration(hintText: "SubJudul ...."),
                    )),
              ],
            )));
  }
  IconButton buildIconButtonPost(FeedBloc _postBloc) {
    return IconButton(
      icon: Icon(Icons.send),
      onPressed: () {
        CloningPosting postData =
            CloningPosting(caption: _caption.text.toString(), judul: _judul.text.toString(), subjudul: _subjudul.text.toString(), url: _urlY.text.toString());
        _postBloc.add(PostFeedYoutube(dataPost: postData));
        Container(width: 50,child: CircularProgressIndicator());
      },
    );
  }
}
