import 'dart:io';

import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class PostGalery extends StatefulWidget {
  File imageFile;

  PostGalery({this.imageFile});
  @override
  _PostGaleryState createState() => _PostGaleryState(imageFile: this.imageFile);
}

class _PostGaleryState extends State<PostGalery> {
  bool statusSend = false;
  final TextEditingController _caption = TextEditingController();
  final TextEditingController _judul = TextEditingController();
  final TextEditingController _subjudul = TextEditingController();
  File imageFile;
  _PostGaleryState({this.imageFile});
  @override
  Widget build(BuildContext context) {
    final _postBloc = FeedBloc();
    return Scaffold(
        appBar: GradientAppBar(
            backgroundColorStart: Color(0xFF398AE5),
            backgroundColorEnd: Color(0xFF61A4F1),
            title: Text('Postingan Baru'),
            leading: Container(
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => Home()),
                  // );
                },
                child: Icon(Icons.arrow_back_ios, color: Colors.white),
              ),
            ),
            actions: <Widget>[
              BlocListener<FeedBloc, FeedState>(
                bloc: _postBloc,
                listener: (context, state) {
                  print("inistate");
                  print(state);
                  if (state is PostSukses) {
                    Fluttertoast.showToast(msg: "Success posting");
                    Navigator.of(context).pop(true);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()));
                  }
                  if (state is PostError) {
                    Fluttertoast.showToast(msg: "Posting Gagal");
                  }
                  if (state is PostWaiting) {
                    return Container(
                      child: Container(width: 50,child: CircularProgressIndicator()),
                    );
                  }
                },
                // child: (statusSend == false) ? buildIconButtonPost(_postBloc) : Container(width: 50,child: CircularProgressIndicator()),
                child: BlocBuilder<FeedBloc, FeedState>(
                  bloc: _postBloc,
                  builder: (context, state) {
                    if (state is FeedError) {
                      Fluttertoast.showToast(msg: "koneksi gagal");
                    }
                    if(state is PostWaiting){
                      return Container(width: 50,child: CircularProgressIndicator());
                    }
                    return buildIconButtonPost(_postBloc);
                  },
                ),
              )
            ]),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                        height: MediaQuery.of(context).size.height / 7,
                        child: Image.file(
                          imageFile,
                          width: 110,
                          height: 50,
                        )),
                  ),
                  Padding(padding: EdgeInsets.all(6.0)),
                  Flexible(
                      flex: 2,
                      child: TextField(
                        controller: _caption,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(hintText: "Caption ...."),
                      )),
                ],
              ),
              Flexible(
                      flex: 2,
                      child: TextField(
                        controller: _judul,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(hintText: "Judul ...."),
                      )),
                      Flexible(
                      flex: 2,
                      child: TextField(
                        controller: _subjudul,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(hintText: "Subjudul ...."),
                      )),
            ],
          ),
        ));
  }

  IconButton buildIconButtonPost(FeedBloc _postBloc) {
    return IconButton(
      icon: Icon(Icons.send),
      onPressed: () {
        // setState(() {
        //   statusSend = true;
        // });
        print("page");
        print(imageFile.path);
        CloningPosting postData =
            CloningPosting(caption: _caption.text.toString(), judul: _judul.text.toString(), subjudul: _subjudul.text.toString());
        _postBloc.add(PostFeedGambar(dataPost: postData, gambar: imageFile));
        Container(width: 50,child: CircularProgressIndicator());
      },
    );
  }
}
