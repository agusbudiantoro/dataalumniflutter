import 'dart:io';
import 'dart:ui';

import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedGalery.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_post_feedYoutube.dart';
import 'package:dataAlumni/Pages/Widget/ShowCaption.dart';
import 'package:dataAlumni/Pages/alumniTerbaik.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/KomentarBloc/komentar_bloc_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:like_button/like_button.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:dataAlumni/util/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:showcaseview/showcase.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_share/flutter_share.dart';
// import 'package:documents_picker/documents_picker.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  ScrollController controller = ScrollController();
  FeedBloc bloc;
  File imageFile;
  bool statusShow;
  GlobalKey _one = GlobalKey();
  // GlobalKey _two = GlobalKey();
  // GlobalKey _three = GlobalKey();
  // GlobalKey _four = GlobalKey();
  // GlobalKey _five = GlobalKey();
  BuildContext myContext;

  Future<void> share() async {
    await FlutterShare.share(
        title: 'Example share',
        text: 'Example share text',
        linkUrl: 'https://flutter.dev/',
        chooserTitle: 'Example Chooser Title');
  }

  // Future<void> shareFile() async {
  //   List<dynamic> docs = await DocumentsPicker.pickDocuments;
  //   if (docs == null || docs.isEmpty) return null;

  //   await FlutterShare.shareFile(
  //     title: 'Example share',
  //     text: 'Example share text',
  //     filePath: docs[0] as String,
  //   );
  // }

  void onScroll() {
    // double maxScroll = controller.position.maxScrollExtent;
    // double currentScroll = controller.position.pixels;

    // if (currentScroll == maxScroll) {
    return bloc.add(FeedEvent());
    // }
  }

  void onScrollHabis() {
    Fluttertoast.showToast(msg: "data habis");
  }

  bool show = false;
  List<Feeds> feed1 = [
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user1.png',
      name: 'John Doe',
      feedImage: '$BaseUrl/images/food/food_ic_popular2.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user2.png',
      name: 'Carry Milton',
      feedImage: '$BaseUrl/images/food/food_ic_popular3.jpg',
    ),
    Feeds(
      profileImg: '$BaseUrl/images/grocery/grocery_ic_user3.png',
      name: 'Jhonny Smith',
      feedImage: '$BaseUrl/images/food/food_ic_popular1.jpg',
    ),
  ];
  bool isActive;
  @override
  void initState() {
    super.initState();
    isActive = true;
    print(isActive);
    init();
    print("sini");
    getTanda();
  }

  init() async {
    await Future.delayed(
      Duration(seconds: 3),
    );
    setState(() {
      isActive = false;
    });
  }

  void getTanda() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      statusShow = prefs.getBool("tandaShowCase");
    });
    if (statusShow == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Future.delayed(Duration(milliseconds: 200),
            () => ShowCaseWidget.of(myContext).startShowCase([_one]));
      });
    } else {}
    print("sini woyy22");
    print(statusShow);
    print(prefs.getBool("tandaShowCase"));
  }

//   You can set the initial value of the count.
  final int likeCount = 0;

//   Set thr Size of the Button/Icons.
  final double buttonSize = 30.0;

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return bloc.add(FeedRefresh());
  }

  void statusTanda() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("tandaShowCase", false);
    print("sini woyy");
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<FeedBloc>(context);

    double c_width = MediaQuery.of(context).size.width * 0.8;
    // controller.addListener(onScroll);
    return ShowCaseWidget(
      onComplete: (index, key) {
        print('onComplete: $index, $key');
        statusTanda();
      },
      builder: Builder(
        builder: (context) {
          myContext = context;
          return SafeArea(
            child: Scaffold(
              appBar: GradientAppBar(
                backgroundColorStart: Color(0xFF398AE5),
                backgroundColorEnd: Color(0xFF61A4F1),
                title: Text('Forum Alumni'),
                leading: Container(
                  child: Showcase(
                    key: _one,
                    title: "Alumni Terbaik",
                    description: "Daftar ALumni Terbaik",
                    child: InkWell(
                      onTap: () {
                        // Navigator.of(context).pop();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AlumniTerbaik()));
                      },
                      child: Icon(Icons.emoji_events, color: Colors.white),
                    ),
                  ),
                ),
              ),
              body: BlocListener<FeedBloc, FeedState>(
                bloc: bloc,
                listener: (context, state) {
                  print(state);
                  print("cobastate");
                  if (state is FeedLoaded) {
                    if (state.statusLike == true) {
                      setState(() {
                        state.indLike = null;
                      });
                      print("berhasil true");
                    } else if (state.statusUnLike == true) {
                      state.indLike = null;
                    } else {
                      print("gagal");
                      return buildListView(state);
                    }
                  }
                  if (state is FeedError) {
                    Fluttertoast.showToast(msg: "koneksi gagal");
                  }
                  return loadingStartPage();
                },
                child: Container(
                  child: BlocBuilder<FeedBloc, FeedState>(
                    builder: (context, state) {
                      if (state is FeedInitial) {
                        return loadingStartPage();
                      }
                      if (state is FeedLoaded) {
                        print("load");
                        print(state.hasReachMax);
                        return buildListView(state);
                      }
                      if (state is FeedError) {
                        Fluttertoast.showToast(msg: "koneksi gagal");
                        return loadingStartPage();
                      }
                      return loadingStartPage();
                    },
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  LazyLoadScrollView buildListView(FeedLoaded data) {
    final _komentarBlocBloc = KomentarBloc();
    // controller.addListener(onScroll);
    return LazyLoadScrollView(
      child: RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.builder(
          itemCount: (data.hasReachMax)
              ? data.listPost.length
              : data.listPost.length + 1,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) => (index <
                  data.listPost.length)
              ? Container(
                  // height: (data.listPost[index].showCaption == false) ? 500 : null,
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      //                   <--- left side
                      color: Colors.grey,
                    ),
                  )),
                  margin: EdgeInsets.only(
                    top: 15,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15),
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: MemoryImage(data.listPost[index].fotoBase64),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                data.listPost[index].nama,
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            // child: InkWell(
                            //   onTap: (){},
                            //   child: Icon(
                            //     Icons.more_vert,
                            //     color: Colors.black,
                            //   ),
                            // ),
                          )
                        ],
                      ),
                      (data.listPost[index].url == null ||
                              data.listPost[index].url == "")
                          ? Container(
                              height: 300,
                              margin: EdgeInsets.only(top: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Image.memory(
                                data.listPost[index].gambarBase64,
                                // cacheHeight: 300,
                              ))
                          : Container(
                              margin: EdgeInsets.only(top: 15),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: YoutubePlayer(
                                // data.listPost[index].url.substring(32)
                                // key: ObjectKey(_controllers[index]),
                                controller: new YoutubePlayerController(
                                    initialVideoId:
                                        YoutubePlayer.convertUrlToId(
                                            data.listPost[index].url),
                                    flags: YoutubePlayerFlags(autoPlay: false)),
                                actionsPadding:
                                    const EdgeInsets.only(left: 16.0),
                                bottomActions: [
                                  CurrentPosition(),
                                  const SizedBox(width: 10.0),
                                  ProgressBar(isExpanded: true),
                                  const SizedBox(width: 10.0),
                                  RemainingDuration(),
                                  FullScreenButton(),
                                ],
                              )),
                      Container(
                        margin: EdgeInsets.only(left: 5, top: 20),
                        child: Row(
                          children: <Widget>[
                            (data.listPost[index].cekLove == false)
                                ? buildIconButtonLike(data, index)
                                : buildIconButtonUnLike(data, index),
                            // LikeButton(
                            //   // size of the button
                            //   size: buttonSize,
                            //   // the total number of count
                            //   likeCount: data.listPost[index].love,
                            //   isLiked: data.listPost[index].cekLove,
                            //   // to handle the count behaviour
                            //   countBuilder:
                            //       (int count, bool isLiked, String text) {
                            //     // color of the count
                            //     isLiked = data.listPost[index].cekLove;
                            //     Text(isLiked.toString());
                            //     final ColorSwatch<int> color =
                            //         isLiked ? Colors.pinkAccent : Colors.grey;
                            //     Widget result;
                            //     if (count == 0) {
                            //       result = Text(
                            //         '0',
                            //         style: TextStyle(color: color),
                            //       );
                            //     } else
                            //       result = Text(
                            //         count >= 1000
                            //             ? (count / 1000.0).toStringAsFixed(1) +
                            //                 'k'
                            //             : text,
                            //         style: TextStyle(color: color),
                            //       );

                            //     return result;
                            //   },
                            //   // position of the count you want to show(i.e. TOP,BOTTOM,RIGHT,LEFT)
                            //   countPostion: CountPostion.right,
                            //   // Animation for the count
                            //   likeCountAnimationType: likeCount < 1000
                            //       ? LikeCountAnimationType
                            //           .all // the whole number will change
                            //       : LikeCountAnimationType.part,
                            //   // only the last digit will change
                            //   onTap: onLikeButtonTapped,
                            //   // onTap: likeButtonFungsi(),
                            // ),
                            buildInkWellKomentar(data, index),
                            SizedBox(width: 4),
                            // InkWell(
                            //   onTap: share,
                            //   child: Icon(
                            //     Icons.share,
                            //     color: Colors.grey,
                            //   ),
                            // )
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 20, bottom: 10),
                          child: Row(
                            children: [
                              Text(
                                  data.listPost[index].love.toString() +
                                      " Likes",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      color:
                                          (data.listPost[index].cekLove == true
                                              ? Colors.red
                                              : Colors.grey)))
                            ],
                          )),
                      buildColumnCaption(data, index),
                      // ShowCaption(data: data.listPost[index]),
                      buildContainerKomentar(data, index),
                    ],
                  ),
                )
              : Container(
                  child: Center(
                    child: SizedBox(
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
        ),
      ),
      onEndOfPage: (data.hasReachMax == false) ? onScroll : onScrollHabis,
    );
  }

  Column buildColumnCaption(FeedLoaded data, int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 20, right: 15),
          child: data.listPost[index].showCaption
              ? Text(
                  data.listPost[index].caption,
                  textAlign: TextAlign.justify,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Text(
                        (data.listPost[index].caption.length < 50)
                            ? data.listPost[index].caption
                            : data.listPost[index].caption.substring(0, 50) +
                                '...',
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
        ),
        (data.listPost[index].caption.length < 50)
            ? Text("")
            : Container(
                margin: EdgeInsets.only(right: 15),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      data.listPost[index].showCaption =
                          !data.listPost[index].showCaption;
                    });
                    print(data.listPost[index].showCaption);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      data.listPost[index].showCaption
                          ? Text(
                              "Less",
                              style: TextStyle(color: Colors.blue),
                            )
                          : Text("More", style: TextStyle(color: Colors.blue))
                    ],
                  ),
                ),
              ),
      ],
    );
  }

  Container buildContainerKomentar(FeedLoaded data, int index) {
    return Container(
        margin: EdgeInsets.only(left: 20, bottom: 10),
        child: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Feed_Komentar(
                        data: data.listPost[index],
                        gambar64: data.listPost[index].gambarBase64),
                  ),
                );
              },
              child: Text(
                (data.listPost[index].jumkomentar == null)
                    ? "0 Komentar"
                    : "Lihat Semua " +
                        (data.listPost[index].jumkomentar).toString() +
                        " Komentar",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
          ],
        ));
  }

  IconButton buildIconButtonLike(FeedLoaded data, int index) {
    return IconButton(
      padding: EdgeInsets.only(right: 0),
      icon: Icon(Icons.favorite),
      color: data.listPost[index].cekLove ? Colors.red : Colors.grey,
      onPressed: () {
        setState(() {
          print("cek love");
          print(data.listPost[index].cekLove);
          print(index);
          data.listPost[index].cekLove = true;
          data.listPost[index].love += 1;
          // if (data.listPost[index].cekLove == true) {
          //   // data.listPost[index].cekLove = false;
          //   // data.listPost[index].love -= 1;

          // } else {

          // }
          if (data.listPost[index].cekLove == true) {
            print("t");
            bloc
              ..add(PostLiked(
                  idPostingan: data.listPost[index].id, likeIndex: index));
          } else {
            print("y");
          }
          print("hasillike");
          print(data.listPost[index].cekLove);
        });
      },
    );
  }

  IconButton buildIconButtonUnLike(FeedLoaded data, int index) {
    return IconButton(
      padding: EdgeInsets.only(right: 0),
      icon: Icon(Icons.favorite),
      color: data.listPost[index].cekLove ? Colors.red : Colors.grey,
      onPressed: () {
        setState(() {
          print("cek love");
          print(data.listPost[index].cekLove);
          print(index);
          data.listPost[index].cekLove = false;
          data.listPost[index].love -= 1;
          // if (data.listPost[index].cekLove == true) {
          //   // data.listPost[index].cekLove = false;
          //   // data.listPost[index].love -= 1;
          //   bloc
          //     ..add(UnLiked(
          //         idPostingan: data.listPost[index].id, likeIndex: index));
          // } else {
          //   bloc
          //     ..add(PostLiked(
          //         idPostingan: data.listPost[index].id, likeIndex: index));
          // }
          if (data.listPost[index].cekLove == false) {
            print("tu");
            bloc
              ..add(UnLiked(
                  idPostingan: data.listPost[index].id, likeIndex: index));
          } else {
            print("yu");
          }
          print("hasil");
          print(data.listPost[index].cekLove);
        });
      },
    );
  }

  InkWell buildInkWellKomentar(FeedLoaded data, yangKe) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Feed_Komentar(
                data: data.listPost[yangKe],
                gambar64: data.listPost[yangKe].gambarBase64),
          ),
        );
      },
      child: Icon(Icons.comment, color: Colors.grey),
    );
  }

  LazyLoadScrollView loadingStartPage() {
    return LazyLoadScrollView(
      onEndOfPage: () {},
      child: RefreshIndicator(
        onRefresh: reffreshIn,
        child: ListView.builder(
          itemCount: feed1.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return SizedBox(
              child: Shimmer.fromColors(
                baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
                child: Container(
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      //                   <--- left side
                      color: Colors.black,
                      width: 1.0,
                    ),
                  )),
                  margin: EdgeInsets.only(
                    top: 15,
                  ),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 15, right: 10),
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 10,
                              ),
                              height: 8,
                              color: Colors.grey,
                            ),
                          ),
                          Container(
                            height: 10,
                            margin: EdgeInsets.only(right: 10),
                            color: Colors.grey,
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        height: 200,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> onLikeButtonTapped(bool isLiked) async {
    /// send your request here
    // final bool success= await sendRequest();

    /// if failed, you can do nothing
    // return success? !isLiked:isLiked;

    return !isLiked;
  }
}

class Feeds {
  String profileImg;
  String name;
  String feedImage;

  Feeds({this.profileImg, this.name, this.feedImage});
}
