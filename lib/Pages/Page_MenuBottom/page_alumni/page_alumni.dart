import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_alumni/page_detail_profile.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_alumni/page_profilUser.dart';
import 'package:dataAlumni/bloc/MenuAlumni/SearchAlumni/dataalumni_bloc.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class Alumni extends StatefulWidget {
  static String tag = '/T10Cards';
  @override
  _AlumniState createState() => _AlumniState();
}

class _AlumniState extends State<Alumni> {
  // List<T10Product> mList;
  DataalumniBloc bloc;
  bool statusScroll;
  String textSearch = "";
  List<DataAlumni> listAlumniByName;
  @override
  void initState() {
    super.initState();
    // mList = getProducts();
  }

  void onScroll() {
    // double maxScroll = controller.position.maxScrollExtent;
    // double currentScroll = controller.position.pixels;

    // if (currentScroll == maxScroll) {
    return bloc.add(DataalumniEvent());
    // }
  }

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return bloc.add(AlumniRefreshEvent());
  }

  void onScrollHabis() {
    Fluttertoast.showToast(msg: "data habis");
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<DataalumniBloc>(context);
    return SafeArea(
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Center(child: Text('Daftar Alumni')),
        ),
        body: LazyLoadScrollView(
          onEndOfPage: (statusScroll == false) ? onScroll : onScrollHabis,
          child: RefreshIndicator(
            onRefresh: reffreshIn,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Container(
                      decoration: boxDecoration(radius: 10, showShadow: true),
                      child: TextField(
                        onChanged: (text) {
                          setState(() {
                            textSearch = text;
                          });
                          print("text ke:" + text + "|" + textSearch);
                          print(text.length);
                          if (textSearch.length != 0) {
                            return bloc.add(SearchByNameEvent(
                                nama: textSearch.toString(),
                                listAlumniByName: listAlumniByName));
                          } else {
                            return bloc.add(AlumniRefreshEvent());
                          }
                        },
                        textAlignVertical: TextAlignVertical.center,
                        // autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Cari Alumni",
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.search),
                          contentPadding: EdgeInsets.only(
                              left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                        ),
                      ),
                      alignment: Alignment.center,
                    ),
                  ),
                  BlocListener<DataalumniBloc, DataalumniState>(
                    bloc: bloc,
                    listener: (context, state) {
                      if (state is GetDataLoading) {
                        return CircularProgressIndicator();
                      }
                      if (state is GetDataSukses) {
                        print("sini1");
                        // print(textSearch);
                        setState(() {
                          statusScroll = state.hasReachMax;
                          listAlumniByName = state.listAlumni;
                        });
                        // return buildListView(state);
                        // if(textSearch.length == 0 || textSearch == null){
                        //   return buildListView(state);
                        // } else {
                        //   setState(() {
                        //     listAlumniByName = state.listAlumni;
                        //   });
                        //   return Container(child: Text("cari"),);
                        // }
                      }
                      if (state is GetDataError) {
                        Fluttertoast.showToast(msg: "koneksi gagal");
                        return CircularProgressIndicator();
                      }
                    },
                    child: Container(
                        child: BlocBuilder<DataalumniBloc, DataalumniState>(
                      bloc: bloc,
                      builder: (context, state) {
                        if (state is GetDataLoading) {
                          return CircularProgressIndicator();
                        }
                        if (state is GetDataSukses) {
                          print("sini2");
                          print(state.listAlumni);
                          return buildListView(state);
                          //   if(textSearch.length == 0){
                          //     print("sini2");
                          //   return buildListView(state);
                          // } else {
                          //   return buildListView(state);
                          // }
                        }
                        if (state is GetDataError) {
                          Fluttertoast.showToast(msg: "koneksi gagal");
                          return CircularProgressIndicator();
                        }
                        return CircularProgressIndicator();
                      },
                    )),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildListView(GetDataSukses data) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: (data.hasReachMax)
            ? data.listAlumni.length
            : data.listAlumni.length + 1,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => (index < data.listAlumni.length)
            ? ProductList(data.listAlumni[index], index)
            : Container(
                child: Center(
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                ),
              ));
  }
}

class ProductList extends StatelessWidget {
  DataAlumni model;

  ProductList(DataAlumni model, int pos) {
    this.model = model;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailProfile(listDataAlumni: model),
          ),
        );
      },
      child: Container(
        decoration: boxDecoration(showShadow: true, radius: spacing_middle),
        padding: EdgeInsets.all(spacing_standard),
        margin: EdgeInsets.only(
            left: spacing_standard_new,
            right: spacing_standard_new,
            bottom: spacing_standard_new),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: ClipRRect(
                  borderRadius:
                      BorderRadius.all(Radius.circular(spacing_middle)),
                  child: Image.memory(
                    model.gambarBase64,
                    fit: BoxFit.fill,
                    height: width * 0.2,
                    // cacheHeight: 300,
                  )),
            ),
            SizedBox(
              width: spacing_standard_new,
            ),
            Expanded(
              flex: 3,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.nama, fontFamily: fontMedium, isLongText: true),
                    text(model.pangkat,
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.tempatLahir,
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.jenisKelamin,
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.jabatan,
                        fontFamily: fontRegular,
                        fontSize: textSizeSmall,
                        isLongText: true),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
