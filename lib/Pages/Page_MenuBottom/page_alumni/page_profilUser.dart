import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_feed/page_komentar.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/feed_profil.dart';
import 'package:dataAlumni/Pages/Widget/Profil/WidgetCard.dart';
import 'package:dataAlumni/Pages/Widget/Profil/feed_dialog.dart';
import 'package:dataAlumni/Pages/Widget/Profil/profileSide.dart';
import 'package:dataAlumni/Pages/login.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/model/T4Models.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/DataGenerator/T4DataGenerator.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/Widget/widgetCardOpen.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:dataAlumni/util/model/informasiUser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_profil/feed_profil.dart';
import 'package:dataAlumni/bloc/MenuProfile/Feed_Profile/FeedProfile/feed_profile_bloc.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'package:flutter/material.dart';
import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator/T10.dart';
import 'package:dataAlumni/util/DbExtension.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/LblString.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class T10ProfileUser extends StatefulWidget {
  static String tag = '/T10Profile';

  @override
  T10ProfileUserState createState() => T10ProfileUserState();
}

class T10ProfileUserState extends State<T10ProfileUser> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _foldingCellKey = GlobalKey<SimpleFoldingCellState>();
  List<T10Images> mList;
  FeedProfileBloc bloc;
  String namaUser;
  String namaLemdik;
  String email;
  TabController _tabController;
  int initialIndex = 0;

  @override
  void initState() {
    super.initState();
    mList = getProfile();
    getFromSharedPreferences();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      email = prefs.getString("email");
      namaUser = prefs.getString("namaUser");
      namaLemdik = prefs.getString("namaLemdik");
    });
  }

  Future<Null> reffreshIn() async {
    await Future.delayed(Duration(seconds: 2));
    return bloc.add(GetFeed());
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<FeedProfileBloc>(context);
    changeStatusColor(t10_white);
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: t10_white,
      body: BlocListener<FeedProfileBloc, FeedProfileState>(
        bloc: bloc,
        listener: (context, state) {
          print(state);
          print("cobastate");
          if (state is FeedSuccessLoad) {
            return buildSafeAreaBody(width, state);
          }
          if (state is FeedProfilError) {
            Fluttertoast.showToast(msg: "koneksi gagal");
          }
          return loadingPage(width);
        },
        child: Container(
          child: BlocBuilder<FeedProfileBloc, FeedProfileState>(
            builder: (context, state) {
              if (state is FeedProfileInitial) {
                return loadingPage(width);
              }
              if (state is FeedSuccessLoad) {
                return buildSafeAreaBody(width, state);
              }
              if (state is FeedProfilError) {
                Fluttertoast.showToast(msg: "koneksi gagal");
                return loadingPage(width);
              }
              return loadingPage(width);
            },
          ),
        ),
      ),
      drawer: T10Drawer(),
    );
  }

  RefreshIndicator loadingPage(double width) {
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            topBar(context),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(spacing_middle),
                                bottomRight:
                                    Radius.circular(spacing_standard_new)),
                            child: CachedNetworkImage(
                              imageUrl: t10_profile,
                              fit: BoxFit.cover,
                              height: width * 0.35,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: spacing_standard_new,
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              text("$namaUser",
                                  fontFamily: fontMedium,
                                  fontSize: textSizeLargeMedium),
                              text("$email", textColor: t10_textColorSecondary),
                              SizedBox(
                                height: spacing_control,
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            right: spacing_control),
                                        child: Icon(
                                          Icons.location_on,
                                          color: t10_textColorSecondary,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                        text: "Jakarta Timur",
                                        style: TextStyle(
                                            fontSize: textSizeMedium,
                                            color: t10_textColorSecondary)),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                              Row(
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      text(theme10_lbl_150,
                                          fontFamily: fontMedium),
                                      text(theme10_lbl_design,
                                          textColor: t10_textColorSecondary),
                                    ],
                                  ),
                                  Container(
                                    height: width * 0.1,
                                    width: 0.5,
                                    color: t10_view_color,
                                    margin: EdgeInsets.only(
                                        left: spacing_standard_new,
                                        right: spacing_standard_new),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      text(theme10_lbl_2K,
                                          fontFamily: fontMedium),
                                      text(theme10_lbl_followers,
                                          textColor: t10_textColorSecondary),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.all(spacing_standard_new),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          text("Lemdik",
                              fontFamily: fontMedium,
                              fontSize: textSizeLargeMedium),
                          text("$namaLemdik",
                              textColor: t10_textColorSecondary,
                              isLongText: true),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              // text(theme10_photos + " and " + theme10_videos,
                              //     fontFamily: fontMedium,
                              //     fontSize: textSizeLargeMedium),
                              // RichText(
                              //   text: TextSpan(
                              //     children: [
                              //       TextSpan(
                              //           text: theme10_view_all,
                              //           style: TextStyle(
                              //               fontSize: textSizeMedium,
                              //               fontFamily: fontMedium,
                              //               color: t10_textColorSecondary)),
                              //       WidgetSpan(
                              //         child: Padding(
                              //           padding: EdgeInsets.only(left: 0),
                              //           child: Icon(
                              //             Icons.keyboard_arrow_right,
                              //             color: t10_textColorPrimary,
                              //             size: 18,
                              //           ),
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          // ListPosts(tabController: _tabController),
                          // Text(data.listPost[0].stringGambar),
                          GridView.builder(
                            reverse: true,
                            scrollDirection: Axis.vertical,
                            itemCount: 9,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Shimmer.fromColors(
                                baseColor: Colors.grey[400],
                                highlightColor: Colors.grey[100],
                                child: ClipRRect(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(spacing_middle)),
                                    child: Container(
                                      color: Colors.black,
                                    )),
                              );
                            },
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 10,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  RefreshIndicator buildSafeAreaBody(double width, FeedSuccessLoad data) {
    return RefreshIndicator(
      onRefresh: reffreshIn,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            topBar(context),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: spacing_standard_new,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(spacing_middle),
                                bottomRight:
                                    Radius.circular(spacing_standard_new)),
                            child: CachedNetworkImage(
                              imageUrl: t10_profile,
                              fit: BoxFit.cover,
                              height: width * 0.35,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: spacing_standard_new,
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              text("$namaUser",
                                  fontFamily: fontMedium,
                                  fontSize: textSizeLargeMedium),
                              text("$email", textColor: t10_textColorSecondary),
                              text("Nama Lemdik : " + "$namaLemdik",
                                  textColor: t10_textColorSecondary),
                              SizedBox(
                                height: spacing_control,
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    WidgetSpan(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            right: spacing_control),
                                        child: Icon(
                                          Icons.location_on,
                                          color: t10_textColorSecondary,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                        text: "Jakarta Timur",
                                        style: TextStyle(
                                            fontSize: textSizeMedium,
                                            color: t10_textColorSecondary)),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: spacing_standard,
                              ),
                              Row(
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      text(theme10_lbl_150,
                                          fontFamily: fontMedium),
                                      text(theme10_lbl_design,
                                          textColor: t10_textColorSecondary),
                                    ],
                                  ),
                                  Container(
                                    height: width * 0.1,
                                    width: 0.5,
                                    color: t10_view_color,
                                    margin: EdgeInsets.only(
                                        left: spacing_standard_new,
                                        right: spacing_standard_new),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      text(theme10_lbl_2K,
                                          fontFamily: fontMedium),
                                      text(theme10_lbl_followers,
                                          textColor: t10_textColorSecondary),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.all(spacing_standard_new),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          text("Lemdik",
                              fontFamily: fontMedium,
                              fontSize: textSizeLargeMedium),
                          text("$namaLemdik",
                              textColor: t10_textColorSecondary,
                              isLongText: true),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              // text(theme10_photos + " and " + theme10_videos,
                              //     fontFamily: fontMedium,
                              //     fontSize: textSizeLargeMedium),
                              // RichText(
                              //   text: TextSpan(
                              //     children: [
                              //       TextSpan(
                              //           text: theme10_view_all,
                              //           style: TextStyle(
                              //               fontSize: textSizeMedium,
                              //               fontFamily: fontMedium,
                              //               color: t10_textColorSecondary)),
                              //       WidgetSpan(
                              //         child: Padding(
                              //           padding: EdgeInsets.only(left: 0),
                              //           child: Icon(
                              //             Icons.keyboard_arrow_right,
                              //             color: t10_textColorPrimary,
                              //             size: 18,
                              //           ),
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                            ],
                          ),
                          SizedBox(
                            height: spacing_standard_new,
                          ),
                          ToggleSwitch(
                            minWidth: width,
                            initialLabelIndex: initialIndex,
                            // cornerRadius: 20.0,
                            activeFgColor: Colors.white,
                            inactiveBgColor: Colors.grey,
                            inactiveFgColor: Colors.white,
                            labels: ['Foto dan Video', 'Informasi User'],
                            icons: [Icons.border_all, Icons.assignment_ind],
                            activeBgColors: [Colors.blue, Colors.blue],
                            onToggle: (index) {
                              print('switched to: $index');
                              setState(() {
                                initialIndex = index;
                              });
                            },
                          ),
                          SizedBox(
                            height: spacing_middle,
                          ),
                          (initialIndex == 0)
                              ? buildGridViewFoto(data)
                              : cardView()
                          // Container(
                          //     margin: EdgeInsets.only(top: 16),
                          //     alignment: Alignment.topCenter,
                          //     child: SimpleFoldingCell(
                          //         key: _foldingCellKey,
                          //         frontWidget: _buildFrontWidget(),
                          //         innerTopWidget: _buildInnerTopWidget(),
                          //         innerBottomWidget:
                          //             _buildInnerBottomWidget(),
                          //         cellSize: Size(
                          //             MediaQuery.of(context).size.width,
                          //             125),
                          //         padding: EdgeInsets.all(15),
                          //         animationDuration:
                          //             Duration(milliseconds: 300),
                          //         borderRadius: 10,
                          //         onOpen: () => print('cell opened'),
                          //         onClose: () => print('cell closed')),
                          //   ),
                          // ListPosts(tabController: _tabController, data: data),
                          // Container(child: Text("sini"))
                          // Text(data.listPost[0].stringGambar),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ListView cardView() {
    return ListView.builder(
        itemCount: InforCard().list.length,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return CardOpen(nama: InforCard().list[index], indexTo: index);
        });
  }

  GridView buildGridViewFoto(FeedSuccessLoad data) {
    return GridView.builder(
      // reverse: true,
      scrollDirection: Axis.vertical,
      itemCount: data.listPost.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        return (data.listPost[index].url == null ||
                data.listPost[index].url == "")
            ? InkWell(
                onLongPress: () {
                  _showAlert(context, data, index);
                },
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          FeedProfilePage(indexTo: index, dataList: data),
                    ),
                  );
                },
                child: ClipRRect(
                    borderRadius:
                        BorderRadius.all(Radius.circular(spacing_middle)),
                    child: Container(
                      color: Colors.black,
                      child: Image.memory(data.listPost[index].gambarBase64),
                    )),
              )
            : InkWell(
                onLongPress: () {
                  _showAlert(context, data, index);
                },
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          FeedProfilePage(indexTo: index, dataList: data),
                    ),
                  );
                },
                child: ClipRRect(
                  borderRadius:
                      BorderRadius.all(Radius.circular(spacing_middle)),
                  child: Container(
                    color: Colors.black,
                    child: Stack(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: YoutubePlayer(
                            // key: ObjectKey(_controller),
                            controller: new YoutubePlayerController(
                                initialVideoId: YoutubePlayer.convertUrlToId(
                                    data.listPost[index].url),
                                flags: YoutubePlayerFlags(autoPlay: false)),
                            actionsPadding: const EdgeInsets.only(left: 16.0),
                            bottomActions: [
                              CurrentPosition(),
                              const SizedBox(width: 10.0),
                              ProgressBar(isExpanded: true),
                              const SizedBox(width: 10.0),
                              RemainingDuration(),
                              FullScreenButton(),
                            ],
                          ),
                        ),
                        Positioned(
                            top: 0,
                            child: ClipRRect(
                              child: Image.memory(
                                data.listPost[index].gambarBase64,
                                cacheHeight: 150,
                                cacheWidth: 150,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ))
                      ],
                    ),
                  ),
                ),
              );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }

  Widget topBar(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerRight,
            end: Alignment.centerLeft,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        height: 60,
        margin: EdgeInsets.only(right: spacing_standard_new),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              color: Colors.white,
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
            ),
            Center(
              child: text(
                "Profil",
                textColor: Colors.white,
                fontFamily: fontBold,
                fontSize: textSizeLargeMedium,
              ),
            ),
            SizedBox(width: 10)
          ],
        ),
      ),
    );
  }

  Future<void> _showAlert(
      BuildContext context, FeedSuccessLoad data, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            child: ShowDialogInProfil(
                data: data.listPost[index],
                gambar64: data.listPost[index].gambarBase64,
                indexTo: index),
          );
        });
  }
}
