import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dataAlumni/Pages/Page_MenuBottom/page_alumni/page_profilUser.dart';
import 'package:dataAlumni/bloc/MenuFeed/Feed/FeedBloc/feed_bloc_bloc.dart';
import 'package:dataAlumni/bloc/alumni_terbaik/alumniterbaik_bloc.dart';
import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/model/modelAlumniTerbaik.dart';
import 'package:dataAlumni/model/reqAlumni.dart';
import 'package:dataAlumni/network/alumniTerbaikApi.dart';
import 'package:dataAlumni/util/Colors.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/DataGenerator.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/Widget/AplWidget.dart';
import 'package:dataAlumni/util/model/Shimmer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class DetailAlumniTerbaik extends StatefulWidget {
  static String tag = '/T10Cards';
  final String myData;

  DetailAlumniTerbaik({this.myData});
  @override
  _DetailAlumniTerbaikState createState() => _DetailAlumniTerbaikState();
}

class _DetailAlumniTerbaikState extends State<DetailAlumniTerbaik> {
  // List<T10Product> mList;
  AlumniterbaikBloc bloc = AlumniterbaikBloc();
  bool statusScroll;
  String textSearch = "";
  List <Reqalumni> listAlumniByName = [];
  @override
  void initState() {
    super.initState();
    print("coba tampil");
    print(listAlumniByName.length);
    decodeData(widget.myData);
    // print(widget.myData);
    // print(json.decode(isi));
    // print(json.decode(widget.myData));
  }
  void decodeData(item) async {
    List<Reqalumni> data = await AlumniTerbaikApi.decodeJsonItem(item);
    print(data);
    setState(() {
      listAlumniByName = data;
    });
  }
  

  void onScroll() {
    // double maxScroll = controller.position.maxScrollExtent;
    // double currentScroll = controller.position.pixels;

    // if (currentScroll == maxScroll) {
    return bloc.add(GetDataAlumniTerbaik());
    // }
  }

  // Future<Null> reffreshIn() async {
  //   await Future.delayed(Duration(seconds: 2));
  //   return bloc.add(GetDataAlumniTerbaik());
  // }

  // void onScrollHabis() {
  //   Fluttertoast.showToast(msg: "data habis");
  // }

  @override
  Widget build(BuildContext context) {
    // bloc = BlocProvider.of<AlumniterbaikBloc>(context);
    return SafeArea(
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: Color(0xFF398AE5),
          backgroundColorEnd: Color(0xFF61A4F1),
          title: Center(child: Text('Daftar Alumni Terbaik')),
        ),
        body: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    // Padding(
                    //   padding: const EdgeInsets.all(16),
                    //   child: Container(
                    //     decoration: boxDecoration(radius: 10, showShadow: true),
                    //     child: TextField(
                    //       onChanged: (text){
                    //         setState(() {
                    //           textSearch = text;
                    //         });
                    //         print("text ke:" + text +"|"+textSearch);
                    //         print(text.length);
                    //         if(textSearch.length != 0){
                    //           return bloc.add(SearchByNameEvent(nama: textSearch.toString(), listAlumniByName: listAlumniByName));
                    //         } else {
                    //           return bloc.add(AlumniRefreshEvent());
                    //         }
                    //       },
                    //       textAlignVertical: TextAlignVertical.center,
                    //       // autofocus: true,
                    //       decoration: InputDecoration(
                    //         filled: true,
                    //         fillColor: Colors.white,
                    //         hintText: "Cari Alumni",
                    //         border: InputBorder.none,
                    //         prefixIcon: Icon(Icons.search),
                    //         contentPadding: EdgeInsets.only(
                    //             left: 26.0, bottom: 8.0, top: 8.0, right: 50.0),
                    //       ),
                    //     ),
                    //     alignment: Alignment.center,
                    //   ),
                    // ),
                    Container(
                      child: buildListView(),
                    )
                  ],
                ),
              ),
        ),
      ),
    );
  }

  Widget buildListView() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: (listAlumniByName.length != 0) ? listAlumniByName.length : 7,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) =>
        // (index < data.listAlumni.length)? 
            (listAlumniByName.length != 0) ? ProductList(listAlumniByName[index], index) : ProductListLoading(7)
            // : Container(
            //     child: Center(
            //       child: SizedBox(
            //         width: 30,
            //         height: 30,
            //         child: CircularProgressIndicator(),
            //       ),
            //     ),
            //   )
              );
  }
}

class ProductListLoading extends StatelessWidget {

  ProductListLoading(int pos) {
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        
      },
      child: Shimmer.fromColors(
        baseColor: Colors.grey[400],
                highlightColor: Colors.grey[100],
              child: Container(
          decoration: boxDecoration(showShadow: true, radius: spacing_middle),
          padding: EdgeInsets.all(spacing_standard),
          margin: EdgeInsets.only(
              left: spacing_standard_new,
              right: spacing_standard_new,
              bottom: spacing_standard_new),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(spacing_middle)),
                  child: Image.asset(
                    userIcon,
                    fit: BoxFit.fill,
                    height: width * 0.2,
                  ),
                ),
              ),
              SizedBox(
                width: spacing_standard_new,
              ),
              Expanded(
                flex: 3,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: 20,)
                      // text(model.pangkat.toString(),
                      //     fontFamily: fontRegular, fontSize: textSizeSmall),
                      // text(model.juduldiklat.toString(),
                      //     fontFamily: fontRegular, fontSize: textSizeSmall),
                      // text(model.jenisKelamin.toString(),
                      //     fontFamily: fontRegular, fontSize: textSizeSmall),
                      // text(model.jabatan.toString(),
                      //     fontFamily: fontRegular,
                      //     fontSize: textSizeSmall,
                      //     isLongText: true),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ProductList extends StatelessWidget {
  Reqalumni model;

  ProductList(Reqalumni model, int pos) {
    this.model = model;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        
      },
      child: Container(
        decoration: boxDecoration(showShadow: true, radius: spacing_middle),
        padding: EdgeInsets.all(spacing_standard),
        margin: EdgeInsets.only(
            left: spacing_standard_new,
            right: spacing_standard_new,
            bottom: spacing_standard_new),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(spacing_middle)),
                child: Image.memory(
                  model.fotoBase64,
                  fit: BoxFit.fill,
                  height: width * 0.2,
                ),
              ),
            ),
            SizedBox(
              width: spacing_standard_new,
            ),
            Expanded(
              flex: 3,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    text(model.nama.toString(), fontFamily: fontMedium, isLongText: true),
                    text(model.pangkat.toString(),
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.juduldiklat.toString(),
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.jenisKelamin.toString(),
                        fontFamily: fontRegular, fontSize: textSizeSmall),
                    text(model.jabatan.toString(),
                        fontFamily: fontRegular,
                        fontSize: textSizeSmall,
                        isLongText: true),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
