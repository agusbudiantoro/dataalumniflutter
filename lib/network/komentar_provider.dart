import 'dart:convert';
import 'dart:io';
import 'package:dataAlumni/model/feed/komentar_model.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'package:http/http.dart' as client;
import 'package:http/io_client.dart';

class KomentarApi {
  // get komentar
  static Future<List<KomentarModel>> connectToApi(
      String token, int idPost) async {
    print(idPost);
    print(token);
    String hasilFotoBase64;
    String apiURL = "https://dev-smartbangkom.lan.go.id:9091/komentar";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      var jsonObject = json.decode(apiResult.body) as List;
      // print(jsonObject.where((element) =>  == '$idPost').toList());
      var convert = jsonObject
          .map<KomentarModel>((item) => KomentarModel(
              id: item["id"],
              created: item["created"],
              idKomentar: item["idKomentar"],
              idKomentator: item["idKomentator"],
              idPosting: item["idPosting"],
              isiKomentar: item["isiKomentar"],
              updatedOn: item["updateon"],
              namakomentator: item["namakomentator"],
              foto: item["foto"]))
          .where((element) => element.idPosting == idPost)
          .toList();
      print(convert
          .where((element) => element.idPosting
              .toString()
              .toLowerCase()
              .contains(idPost.toString()))
          .toList());
      print(convert);
      List<KomentarModel> isi = [];
      for (var i = 0; i < convert.length; i++) {
        print("isi komen");
        print(i);
        print(convert.length);
        if (i < convert.length) {
          if (convert[i].foto != null || convert[i].foto != "") {
            hasilFotoBase64 =
                await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                    token, convert[i].foto);
          } else {
            hasilFotoBase64 = GambarNull().gbr;
          }
          var finalData = KomentarModel(
            id: convert[i].id,
            created: convert[i].created,
            updatedOn: convert[i].updatedOn,
            stringFoto: hasilFotoBase64,
            idKomentar: convert[i].idKomentar,
            idKomentator: convert[i].idKomentator,
            idPosting: convert[i].idPosting,
            isiKomentar: convert[i].isiKomentar,
            namakomentator: convert[i].namakomentator,
          );

          var isiData = jsonEncode(finalData);
          var dataIsi = jsonDecode(isiData);
          print(isiData);
          var autoConvert = KomentarModel.fromJson(dataIsi);
          print(autoConvert);
          isi.add(autoConvert);
        } else if (i == convert.length - 1) {
          break;
        }
      }
      return isi
          .map<KomentarModel>((e) => KomentarModel(
                id: e.id,
                created: e.created,
                updatedOn: e.updatedOn,
                fotoBase64: base64Decode(e.stringFoto),
                idKomentar: e.idKomentar,
                idKomentator: e.idKomentator,
                idPosting: e.idPosting,
                isiKomentar: e.isiKomentar,
                namakomentator: e.namakomentator,
              ))
          .toList();
      // return convert;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  // post komentar
  static Future<List<KomentarModel>> connectToApiPostKomen(String token,
      int idUser, int idPostingan, String isiKomentar) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    Map<String, String> isi = {
      'idKomentar': "0",
      'idKomentator': idUser.toString(),
      'idPosting': idPostingan.toString(),
      'isiKomentar': isiKomentar,
    };
    String apiURL = "https://dev-smartbangkom.lan.go.id:9091/komentar";
    var apiResult = await ioClient.post(apiURL,
        body: utf8.encode(jsonEncode(isi)),
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        });
    String hasilFotoBase64;
    print(apiResult.body);
    var convert = json.decode(apiResult.body);
    print(convert);
    var komen1 = KomentarModel.fromJson(convert);
    print("cek nama");
    print(komen1.namakomentator);
    if (apiResult.statusCode == 200) {
      List<KomentarModel> isi = [];
      if (komen1.foto == null || komen1.foto == "") {
        hasilFotoBase64 = GambarNull().gbr;
      } else {
        hasilFotoBase64 =
            await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                token, komen1.foto);
      }
      var finalData = KomentarModel(
        id: komen1.id,
        created: komen1.created,
        updatedOn: komen1.updatedOn,
        stringFoto: hasilFotoBase64,
        idKomentar: komen1.idKomentar,
        idKomentator: komen1.idKomentator,
        idPosting: komen1.idPosting,
        isiKomentar: komen1.isiKomentar,
        namakomentator: komen1.namakomentator,
      );
      var isiData = jsonEncode(finalData);
      var dataIsi = jsonDecode(isiData);
      print(isiData);
      print(dataIsi);
      print("ini");
      var autoConvert = KomentarModel.fromJson(dataIsi);
      isi.add(autoConvert);
      print(autoConvert.stringFoto);
      return isi.map<KomentarModel>((e) => KomentarModel(
                id: e.id,
                created: e.created,
                updatedOn: e.updatedOn,
                fotoBase64: base64Decode(e.stringFoto),
                idKomentar: e.idKomentar,
                idKomentator: e.idKomentator,
                idPosting: e.idPosting,
                isiKomentar: e.isiKomentar,
                namakomentator: e.namakomentator,
              )).toList();
      //     var isiData2 = jsonEncode(isi);
      // var dataIsi2 = jsonDecode(isiData2);
      // var autoConvert2 = KomentarModel.fromJson(dataIsi2);
      // print(dataIsi2);
      // print("ini isi");
      // print(isi[0].fotoBase64);
      // print(komen1);
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  // get child komentar
  static Future<List<KomentarModel>> connectToApiChildKomen(
      String token, int idKomen) async {
    print(idKomen);
    print(token);
    String hasilFotoBase64;
    String apiURL = "https://dev-smartbangkom.lan.go.id:9091/komentar";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      var jsonObject = json.decode(apiResult.body) as List;
      // print(jsonObject.where((element) =>  == '$idPost').toList());
      var convert = jsonObject
          .map<KomentarModel>((item) => KomentarModel(
              id: item["id"],
              created: item["created"],
              idKomentar: item["idKomentar"],
              idKomentator: item["idKomentator"],
              idPosting: item["idPosting"],
              isiKomentar: item["isiKomentar"],
              updatedOn: item["updateon"],
              namakomentator: item["namakomentator"],
              foto: item["foto"]))
          .where((element) => element.idKomentar == idKomen)
          .toList();
      print(convert
          .where((element) => element.idKomentar
              .toString()
              .toLowerCase()
              .contains(idKomen.toString()))
          .toList());
      List<KomentarModel> isi = [];
      for (var i = 0; i < convert.length; i++) {
        print("isi komen");
        print(i);
        print(convert.length);
        if (i < convert.length) {
          if (convert[i].foto != null || convert[i].foto != "") {
            hasilFotoBase64 =
                await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                    token, convert[i].foto);
          } else {
            hasilFotoBase64 = GambarNull().gbr;
          }
          var finalData = KomentarModel(
            id: convert[i].id,
            created: convert[i].created,
            updatedOn: convert[i].updatedOn,
            stringFoto: hasilFotoBase64,
            idKomentar: convert[i].idKomentar,
            idKomentator: convert[i].idKomentator,
            idPosting: convert[i].idPosting,
            isiKomentar: convert[i].isiKomentar,
            namakomentator: convert[i].namakomentator,
          );

          var isiData = jsonEncode(finalData);
          var dataIsi = jsonDecode(isiData);
          print(isiData);
          var autoConvert = KomentarModel.fromJson(dataIsi);
          print(autoConvert);
          isi.add(autoConvert);
        } else if (i == convert.length - 1) {
          break;
        }
      }
      return isi
          .map<KomentarModel>((e) => KomentarModel(
                id: e.id,
                created: e.created,
                updatedOn: e.updatedOn,
                fotoBase64: base64Decode(e.stringFoto),
                idKomentar: e.idKomentar,
                idKomentator: e.idKomentator,
                idPosting: e.idPosting,
                isiKomentar: e.isiKomentar,
                namakomentator: e.namakomentator,
              ))
          .toList();
    } else {
      throw Exception("Get Data tidak ditemukan");
    }
  }

  // post Child komentar
  static Future<List<KomentarModel>> connectToApiPostChildKomen(
      String token, int idKomentar, String isiKomentar, int idUser) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    String hasilFotoBase64;
    print("cek");
    print(idKomentar);
    print(idUser);
    Map<String, String> isi = {
      'idKomentar': idKomentar.toString(),
      'idKomentator': idUser.toString(),
      'idPosting': "0",
      'isiKomentar': isiKomentar,
    };
    String apiURL = "https://dev-smartbangkom.lan.go.id:9091/komentar";
    var apiResult = await ioClient.post(apiURL,
        body: utf8.encode(jsonEncode(isi)),
        headers: {
          "Authorization": "Bearer $token",
          "Content-Type": "application/json"
        });
    var convert = json.decode(apiResult.body);
    print(apiResult.body);
    print(convert);
    var komen1 = KomentarModel.fromJson(convert);
    if (apiResult.statusCode == 200) {
      print(komen1);
      List<KomentarModel> isi = [];
      if (komen1.foto == null || komen1.foto == "") {
        hasilFotoBase64 = GambarNull().gbr;
      } else {
        hasilFotoBase64 =
            await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                token, komen1.foto);
      }
      var finalData = KomentarModel(
        id: komen1.id,
        created: komen1.created,
        updatedOn: komen1.updatedOn,
        stringFoto: hasilFotoBase64,
        idKomentar: komen1.idKomentar,
        idKomentator: komen1.idKomentator,
        idPosting: komen1.idPosting,
        isiKomentar: komen1.isiKomentar,
        namakomentator: komen1.namakomentator,
      );
      var isiData = jsonEncode(finalData);
      var dataIsi = jsonDecode(isiData);
      print(isiData);
      print(dataIsi);
      print("ini");
      var autoConvert = KomentarModel.fromJson(dataIsi);
      isi.add(autoConvert);
      print(autoConvert.stringFoto);
      return isi.map<KomentarModel>((e) => KomentarModel(
                id: e.id,
                created: e.created,
                updatedOn: e.updatedOn,
                fotoBase64: base64Decode(e.stringFoto),
                idKomentar: e.idKomentar,
                idKomentator: e.idKomentator,
                idPosting: e.idPosting,
                isiKomentar: e.isiKomentar,
                namakomentator: e.namakomentator,
              )).toList();
    } else {
      throw Exception("Post Data tidak ditemukan");
    }
  }
}
