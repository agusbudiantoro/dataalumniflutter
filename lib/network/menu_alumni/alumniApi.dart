import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'dart:io';
import 'package:async/async.dart';

class AlumniApi {
  static Future<List<DataAlumni>> getDataAlumni(
      String token, int start, int limit) async {
    print(token);
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9092/alumni/$start/$limit";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject.length);
      if (jsonObject.length == 0) {
        var hasilFinal = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]
                ))
            .toList();
        return hasilFinal;
      } else {
        var data = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]))
            .toList();
        print("ulang");
        print(data);
        List<DataAlumni> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            List daftarLove = [];
            print(data[i].foto);
            if (data[i].foto == null) {
              print("data ke dua");
              hasilGambarBase64 = IconOrang().gbrPeople;
            } else {
              hasilGambarBase64 =
                  await GetGambarBase64Alumni.getGambarBase64NonKompresAlumni(
                      token, data[i].foto);
              print("data ke dua else");
            }
            print(hasilGambarBase64);
            print('jika daftar love null');
            print(jsonEncode(data[i]));
            print(data);
            var finalData = DataAlumni(
                id: data[i].id,
                idSmartbangkom: data[i].idSmartbangkom,
                idSipkabangkom: data[i].idSipkabangkom,
                idUser: data[i].idUser,
                nama: data[i].nama,
                foto: data[i].foto,
                expertise: data[i].expertise,
                organisasi: data[i].organisasi,
                nomorId: data[i].nomorId,
                tempatLahir: data[i].tempatLahir,
                jenisKelamin: data[i].jenisKelamin,
                agama: data[i].agama,
                pangkat: data[i].pangkat,
                email: data[i].email,
                asalInstansi: data[i].asalInstansi,
                alamatKantor: data[i].alamatKantor,
                noTelp: data[i].noTelp,
                nomorKra: data[i].nomorKra,
                jabatan: data[i].jabatan,
                gol: data[i].gol,
                stringGambar: hasilGambarBase64);
            var isiData = jsonEncode(finalData);
            var dataIsi = jsonDecode(isiData);
            print(isiData);
            var autoConvert = DataAlumni.fromJson(dataIsi);
            print(autoConvert);
            isi.add(autoConvert);
            print(isi[0].id);
            print("isi null");
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("isi2");
        var valueFinal = jsonEncode(isi.toList());
        var convertFinal = jsonDecode(valueFinal);
        return isi
            .map<DataAlumni>((e) => DataAlumni(
                id: e.id,
                idSmartbangkom: e.idSmartbangkom,
                idSipkabangkom: e.idSipkabangkom,
                idUser: e.idUser,
                nama: e.nama,
                foto: e.foto,
                expertise: e.expertise,
                organisasi: e.organisasi,
                nomorId: e.nomorId,
                tempatLahir: e.tempatLahir,
                jenisKelamin: e.jenisKelamin,
                agama: e.agama,
                pangkat: e.pangkat,
                email: e.email,
                asalInstansi: e.asalInstansi,
                alamatKantor: e.alamatKantor,
                noTelp: e.noTelp,
                nomorKra: e.nomorKra,
                jabatan: e.jabatan,
                gol: e.gol,
                stringGambar: hasilGambarBase64,
                gambarBase64: base64Decode(e.stringGambar),
                ))
            .toList();
      }
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<DataAlumni>> getDataAlumniByName(
      String token, int start, int limit, String nama) async {
    print(token);
    print(start);
    print(limit);
    print(nama);
    print("siniwoyyyy");
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9092/alumni/caribynama/$start/$limit?nama=$nama";
    var apiResult =
        await client.post(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      print(apiResult.body);
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject.length);
      if (jsonObject.length == 0) {
        var hasilFinal = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]
                ))
            .toList();
        return hasilFinal;
      } else {
        var data = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]))
            .toList();
        print("ulang");
        print(data);
        List<DataAlumni> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            List daftarLove = [];
            print(data[i].foto);
            if (data[i].foto == null) {
              print("data ke dua");
              hasilGambarBase64 = IconOrang().gbrPeople;
            } else {
              hasilGambarBase64 =
                  await GetGambarBase64Alumni.getGambarBase64NonKompresAlumni(
                      token, data[i].foto);
              print("data ke dua else");
            }
            print(hasilGambarBase64);
            print('jika daftar love null');
            print(jsonEncode(data[i]));
            print(data);
            var finalData = DataAlumni(
                id: data[i].id,
                idSmartbangkom: data[i].idSmartbangkom,
                idSipkabangkom: data[i].idSipkabangkom,
                idUser: data[i].idUser,
                nama: data[i].nama,
                foto: data[i].foto,
                expertise: data[i].expertise,
                organisasi: data[i].organisasi,
                nomorId: data[i].nomorId,
                tempatLahir: data[i].tempatLahir,
                jenisKelamin: data[i].jenisKelamin,
                agama: data[i].agama,
                pangkat: data[i].pangkat,
                email: data[i].email,
                asalInstansi: data[i].asalInstansi,
                alamatKantor: data[i].alamatKantor,
                noTelp: data[i].noTelp,
                nomorKra: data[i].nomorKra,
                jabatan: data[i].jabatan,
                gol: data[i].gol,
                stringGambar: hasilGambarBase64);
            var isiData = jsonEncode(finalData);
            var dataIsi = jsonDecode(isiData);
            print(isiData);
            var autoConvert = DataAlumni.fromJson(dataIsi);
            print(autoConvert);
            isi.add(autoConvert);
            print(isi[0].id);
            print("isi null");
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("isi2");
        var valueFinal = jsonEncode(isi.toList());
        var convertFinal = jsonDecode(valueFinal);
        return isi
            .map<DataAlumni>((e) => DataAlumni(
                id: e.id,
                idSmartbangkom: e.idSmartbangkom,
                idSipkabangkom: e.idSipkabangkom,
                idUser: e.idUser,
                nama: e.nama,
                foto: e.foto,
                expertise: e.expertise,
                organisasi: e.organisasi,
                nomorId: e.nomorId,
                tempatLahir: e.tempatLahir,
                jenisKelamin: e.jenisKelamin,
                agama: e.agama,
                pangkat: e.pangkat,
                email: e.email,
                asalInstansi: e.asalInstansi,
                alamatKantor: e.alamatKantor,
                noTelp: e.noTelp,
                nomorKra: e.nomorKra,
                jabatan: e.jabatan,
                gol: e.gol,
                stringGambar: hasilGambarBase64,
                gambarBase64: base64Decode(e.stringGambar),
                ))
            .toList();
      }
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<DataAlumni>> getDataAlumniById(int idUser, String token) async {
    print("siniwoyyyy");
    print(idUser);
    print(token);
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9092/alumni/$idUser";
    var apiResult =
        await client.get(apiURL);
    print("feed");
    print(apiResult.statusCode);
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print(apiResult.body);
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject.length);
      if (jsonObject.length == 0) {
        var hasilFinal = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]
                ))
            .toList();
        return hasilFinal;
      } else {
        var data = jsonObject
            .map<DataAlumni>((item) => DataAlumni(
                id: item["id"],
                idSmartbangkom: item["idSmartbangkom"],
                idSipkabangkom: item["idSipkabangkom"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                expertise: item["expertise"],
                organisasi: item["organisasi"],
                nomorId: item["nomorId"],
                tempatLahir: item["tempatLahir"],
                jenisKelamin: item["jenisKelamin"],
                agama: item["agama"],
                pangkat: item["pangkat"],
                email: item["email"],
                asalInstansi: item["asalInstansi"],
                alamatKantor: item["alamatKantor"],
                noTelp: item["noTelp"],
                nomorKra: item["nomorKra"],
                jabatan: item["jabatan"],
                gol: item["gol"]))
            .toList();
        print("ulang");
        print(data);
        List<DataAlumni> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            List daftarLove = [];
            print(data[i].foto);
            if (data[i].foto == null || data[i].foto =="") {
              print("data ke dua");
              hasilGambarBase64 = IconOrang().gbrPeople;
            } else {
              hasilGambarBase64 =
                  await GetGambarBase64Alumni.getGambarBase64NonKompresAlumni(
                      token, data[i].foto);
              print("data ke dua else");
            }
            print(hasilGambarBase64);
            print('jika daftar love null');
            print(jsonEncode(data[i]));
            print(data);
            var finalData = DataAlumni(
                id: data[i].id,
                idSmartbangkom: data[i].idSmartbangkom,
                idSipkabangkom: data[i].idSipkabangkom,
                idUser: data[i].idUser,
                nama: data[i].nama,
                foto: data[i].foto,
                expertise: data[i].expertise,
                organisasi: data[i].organisasi,
                nomorId: data[i].nomorId,
                tempatLahir: data[i].tempatLahir,
                jenisKelamin: data[i].jenisKelamin,
                agama: data[i].agama,
                pangkat: data[i].pangkat,
                email: data[i].email,
                asalInstansi: data[i].asalInstansi,
                alamatKantor: data[i].alamatKantor,
                noTelp: data[i].noTelp,
                nomorKra: data[i].nomorKra,
                jabatan: data[i].jabatan,
                gol: data[i].gol,
                stringGambar: hasilGambarBase64);
            var isiData = jsonEncode(finalData);
            var dataIsi = jsonDecode(isiData);
            print(isiData);
            var autoConvert = DataAlumni.fromJson(dataIsi);
            print(autoConvert);
            isi.add(autoConvert);
            print(isi[0].id);
            print("isi null");
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("isi2");
        var valueFinal = jsonEncode(isi.toList());
        var convertFinal = jsonDecode(valueFinal);
        return isi
            .map<DataAlumni>((e) => DataAlumni(
                id: e.id,
                idSmartbangkom: e.idSmartbangkom,
                idSipkabangkom: e.idSipkabangkom,
                idUser: e.idUser,
                nama: e.nama,
                foto: e.foto,
                expertise: e.expertise,
                organisasi: e.organisasi,
                nomorId: e.nomorId,
                tempatLahir: e.tempatLahir,
                jenisKelamin: e.jenisKelamin,
                agama: e.agama,
                pangkat: e.pangkat,
                email: e.email,
                asalInstansi: e.asalInstansi,
                alamatKantor: e.alamatKantor,
                noTelp: e.noTelp,
                nomorKra: e.nomorKra,
                jabatan: e.jabatan,
                gol: e.gol,
                stringGambar: hasilGambarBase64,
                gambarBase64: base64Decode(e.stringGambar),
                ))
            .toList();
      }
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> postDataAlumni(DataAlumni data,int idUser, String token) async {
    print("siniwoyyyy");
    print(idUser);
    print(token);
    print(data);
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/alumni";
    Map<String, dynamic> isi = {
      "agama": data.agama.toString(),
      "alamatKantor": data.alamatKantor.toString(),
      "asalInstansi": data.asalInstansi.toString(),
      "email": data.email.toString(),
      "expertise": data.expertise.toString(),
      "gol": data.gol.toString(),
      "idUser": idUser,
      "jabatan": data.jabatan.toString(),
      "jenisKelamin": data.jenisKelamin.toString(),
      "nama": data.nama.toString(),
      "noTelp": data.noTelp.toString(),
      "nomorId": data.nomorId.toString(),
      "nomorKra": data.nomorKra.toString(),
      "organisasi": data.organisasi.toString(),
      "pangkat": data.pangkat.toString(),
      "tempatLahir": data.tempatLahir.toString()
    };
    print(isi);
    var apiResult =
        await client.post(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
    print("feed");
    print(isi);
    print("wooyy");
    print(apiResult.statusCode);
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("status ok");
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> putDataAlumni(DataAlumni data,int idUser, String token) async {
    print("siniwoyyyy");
    print(idUser);
    print(token);
    String id = data.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/alumni/$id";
    Map<String, String> isi = {
      "agama": data.agama.toString(),
      "alamatKantor": data.alamatKantor.toString(),
      "asalInstansi": data.asalInstansi.toString(),
      "email": data.email.toString(),
      "expertise": data.expertise.toString(),
      "gol": data.gol.toString(),
      "idUser": idUser.toString(),
      "jabatan": data.jabatan.toString(),
      "jenisKelamin": data.jenisKelamin.toString(),
      "nama": data.nama.toString(),
      "noTelp": data.noTelp.toString(),
      "nomorId": data.nomorId.toString(),
      "nomorKra": data.nomorKra.toString(),
      "organisasi": data.organisasi.toString(),
      "pangkat": data.pangkat.toString(),
      "tempatLahir": data.tempatLahir.toString()
    };
    var apiResult =
        await client.put(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
    print("feed");
    print(apiResult.statusCode);
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("status ok");
      return true;
    } else {
      return false;
    }
  }
}
