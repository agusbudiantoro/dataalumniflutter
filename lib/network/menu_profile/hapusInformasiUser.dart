import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:async/async.dart';

class HapusInformasionApi {
  static Future<bool> hapusPendidikan(
      String token, int idUser, ModelPendidikanUser model) async {
        print("konekasi");
        print(model.id);
        String id = model.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/pendidikan/$id";

    var apiResult =
        await client.delete(apiURL, headers: {"Authorization": "Bearer $token"});
        print(apiResult.statusCode);
        print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("lengthkoneksi");
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> hapusKarir(String token, int idUser, ModelKarir model) async {
    String id = model.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/karir/$id";

    var apiResult =
        await client.delete(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> hapusOrganisasi(String token, int idUser, ModelOrganisasi model) async {
    String id = model.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/organisasi/$id";

    var apiResult =
        await client.delete(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      return true;
    } else {
      return false;
    }
  }
}
