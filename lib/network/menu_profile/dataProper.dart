import 'dart:convert';
import 'package:dataAlumni/model/modeProper.dart';
import 'package:http/http.dart' as client;

class GetDataProperApi {
  static Future<List<ModelProper>> getProper(int idUser) async {
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/proper/$idUser";

    var apiResult =
        await client.get(apiURL);
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.statusCode);
      print(apiResult.body);
      var convertData = jsonDecode(apiResult.body) as List;
      print(convertData.length);
      var hasilFinal = convertData
          .map<ModelProper>((item) => ModelProper(
              id: item["id"],
              idAlumni: item['idAlumni'],
              abstrak: item['abstrak'],
              evidence: item['evidence'],
              idSmartbangkom: item['idSmartbangkom'],
              judul: item['judul'],
              kataKunci: item['kataKunci'],
              tahun: item['tahun']
              ))
          .toList();
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<ModelProper> addProper(
      String token, int idUser, ModelProper model) async {
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/proper";
    Map<dynamic, dynamic> isi = {
      'idAlumni': idUser,
      'judul': model.judul.toString(),
      'tahun': model.tahun,
      'abstrak': model.abstrak.toString(),
      'kataKunci': model.kataKunci.toString(),
      'evidence': model.evidence.toString()
    };

    var apiResult =
        await client.post(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
        print(apiResult.statusCode);
        print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("length");
      var convertData = jsonDecode(apiResult.body);
      print(convertData.length);
      var hasilFinal = ModelProper.fromJson(convertData);
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<ModelProper> editProper(
      String token, int idUser, ModelProper model) async {
        String idProper = model.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/proper/$idProper";
    Map<dynamic, dynamic> isi = {
      'idAlumni': idUser,
      'judul': model.judul.toString(),
      'tahun': model.tahun,
      'abstrak': model.abstrak.toString(),
      'kataKunci': model.kataKunci.toString(),
      'evidence': model.evidence.toString()
    };

    var apiResult =
        await client.put(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
        print(apiResult.statusCode);
        print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("length");
      var convertData = jsonDecode(apiResult.body);
      print(convertData.length);
      var hasilFinal = ModelProper.fromJson(convertData);
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> hapusProper(String token, int idUser, ModelProper model) async {
    String id = model.id.toString();
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/proper/$id";

    var apiResult =
        await client.delete(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      return true;
    } else {
      return false;
    }
  }
}