import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:async/async.dart';

class GetInformasionApi {
  static Future<List<ModelPendidikanUser>> getPendidikan(
      String token, int idUser) async {
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/pendidikan/$idUser";

    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      var convertData = jsonDecode(apiResult.body) as List;
      print(convertData.length);
      var hasilFinal = convertData
          .map<ModelPendidikanUser>((item) => ModelPendidikanUser(
              id: item["id"],
              idAlumni: item['idAlumni'],
              jenjang: item['jenjang'],
              namauniv: item['namauniv'],
              lokasi: item['lokasi'],
              tahun: item['tahun']))
          .toList();
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<ModelKarir>> getKarir(String token, int idUser) async {
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/karir/$idUser";

    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      var convertData = jsonDecode(apiResult.body) as List;
      print(convertData.length);
      var hasilFinal = convertData
          .map<ModelKarir>((item) => ModelKarir(
              id: item["id"],
              idAlumni: item['idAlumni'],
              posisi: item['posisi'],
              tempat: item['tempat'],
              tahun: item['tahun']))
          .toList();
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<ModelOrganisasi>> getOrganisasi(String token, int idUser) async {
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/organisasi/$idUser";

    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      var convertData = jsonDecode(apiResult.body) as List;
      print(convertData.length);
      var hasilFinal = convertData
          .map<ModelOrganisasi>((item) => ModelOrganisasi(
              id: item["id"],
              idAlumni: item['idAlumni'],
              namaOrganisasi: item['namaOrganisasi'],
              jabatan: item['jabatan'],
              bidangOrganisasi: item['bidangOrganisasi'],
              tahun: item['tahun']))
          .toList();
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}
