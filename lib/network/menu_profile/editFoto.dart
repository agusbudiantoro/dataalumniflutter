import 'dart:io';
import 'dart:typed_data';

import 'package:dataAlumni/model/alumni/modelAlumni.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:http/io_client.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart' as path;
import 'package:image/image.dart' as img;
import 'package:path/path.dart';
import 'dart:math' as Math;
import 'package:async/async.dart';

class EditFotoApi {
  static Future<DataAlumni> editFotoApi(
     String token, id, File gambar64) async {
    print(gambar64.path);
    String url = "https://dev-smartbangkom.lan.go.id:9092/alumni/foto?id=$id";
    
    
    String fileName = basename(gambar64.path);
    String base64Image = base64Encode(gambar64.readAsBytesSync());
    print(base64Image);
    print("File base name: $fileName");
    var stream = new client.ByteStream(DelegatingStream.typed(gambar64.openRead()));
    var length = await gambar64.length();
    print(gambar64.path);
      // String isi = '{'
      //     '"caption": "$cap",'
      //     '"judul": "$jud",'
      //     '"subjudul": "$sub",'
      //     '"idcreator": "$idUs",'
      //     '"url": ""'
      //     '}';
    var request = client.MultipartRequest("POST", Uri.parse(url));
    // request.fields["posting"] = isi;
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept':'*/*',
      HttpHeaders.authorizationHeader: 'Bearer $token'
    });
    print(stream.toString());
    print(request.headers);
    var pic = await client.MultipartFile("foto", stream, length,filename: fileName, contentType: MediaType("multipart/form-data","multipart/form-data"));
    request.files.add(pic);
    print(pic.toString());
    client.Response response = await client.Response.fromStream(await request.send());
    

    print(response.toString());
    print(request.files.toString());
    print("kkk");
    print(response.body);
    print(response.statusCode);
    if(response.statusCode == 200){
      var isi = jsonDecode(response.body);
      var isiFinal = DataAlumni.fromJson(isi);
      return isiFinal;
    } else{
      throw Exception("Data tidak ditemukan");
    }    
  }
}