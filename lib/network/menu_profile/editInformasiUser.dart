import 'package:dataAlumni/model/profile/InformasiUser/KarirUser.dart';
import 'package:dataAlumni/model/profile/InformasiUser/Organisasi.dart';
import 'package:dataAlumni/model/profile/InformasiUser/PendidikanUser.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:async/async.dart';

class EditInformasionApi {
  static Future<ModelPendidikanUser> editPendidikan(
      String token, int idUser, ModelPendidikanUser model) async {
        print("konekasi");
        print(model.id);
        String id = model.id.toString();
        Map<String, String> isi = {
      'idAlumni': idUser.toString(),
      'jenjang': model.jenjang,
      'lokasi': model.lokasi,
      'namauniv': model.namauniv,
      'tahun': model.tahun,
    };
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/pendidikan/$id";

    var apiResult =
        await client.put(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
        print(apiResult.statusCode);
        print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("lengthkoneksi");
      var convertData = jsonDecode(apiResult.body);
      print(convertData.length);
      var hasilFinal = ModelPendidikanUser.fromJson(convertData);
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<ModelKarir> editKarir(String token, int idUser, ModelKarir model) async {
    String id = model.id.toString();
    Map<String, String> isi = {
      'idAlumni': idUser.toString(),
      'posisi': model.posisi.toString(),
      'tempat': model.tempat.toString(),
      'tahun': model.tahun.toString(),
    };
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/karir/$id";

    var apiResult =
        await client.put(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      var convertData = jsonDecode(apiResult.body);
      print(convertData.length);
      var hasilFinal = ModelKarir.fromJson(convertData);
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<ModelOrganisasi> editOrganisasi(String token, int idUser, ModelOrganisasi model) async {
    String id = model.id.toString();
    Map<String, String> isi = {
      'idAlumni': idUser.toString(),
      'namaOrganisasi': model.namaOrganisasi.toString(),
      'bidangOrganisasi': model.bidangOrganisasi.toString(),
      'jabatan': model.jabatan.toString(),
      'tahun': model.tahun.toString(),
    };
    String apiURL = "https://dev-smartbangkom.lan.go.id:9092/organisasi/$id";

    var apiResult =
        await client.put(apiURL, headers: {"Authorization": "Bearer $token", "Content-Type":"application/json"}, body: utf8.encode(jsonEncode(isi)));
        print(apiResult.statusCode);
        print(apiResult.body);
    if (apiResult.statusCode == 200) {
      print("length");
      print(apiResult.body);
      var convertData = jsonDecode(apiResult.body);
      print(convertData.length);
      var hasilFinal = ModelOrganisasi.fromJson(convertData);
      return hasilFinal;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}
