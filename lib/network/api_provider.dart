import 'dart:io';

import 'package:dataAlumni/model/cek_token.dart';
import 'package:dataAlumni/model/registModel.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:http/http.dart' show Client;
import 'package:dataAlumni/model/post.dart';
import 'package:dataAlumni/model/login_model.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiProvider {
  Dio dio = Dio();
  Response response;
  Client client = Client();
  Future<List<Post>> getListPost() async {
    try {
      response = await dio.get("https://jsonplaceholder.typicode.com/posts");
      List listResponse = response.data;
      List<Post> listPost = listResponse.map((f) => Post.fromJson(f)).toList();
      return listPost;
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<RegisterModel> regist(RegisterModel registData) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    print("woy");
    print(registData.nip);
    Map<String, String> isi = {
      'nama': registData.nama.toString(),
      'nip': registData.nip.toString(),
      'password': registData.password.toString(),
      'verifikasi': 0.toString()
    };
    print(isi);
    final response = await ioClient.post(
        "https://dev-smartbangkom.lan.go.id:9093/Register",
        body: utf8.encode(jsonEncode(isi)),
        headers: {"Content-Type": "application/json"});
    var listResponse = response.body;
    print(listResponse);
    final listConvert = json.decode(listResponse);
    RegisterModel listPost = RegisterModel.fromJson(listConvert);
    if (response.statusCode == 200) {
      return listPost;
    } else {
      throw Exception("Gagal Daftar");
    }
  }

  Future<LoginModel> login(LoginModel loginData) async {
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    print("woy");
    print(loginData.username);
    Map<String, String> isi = {
      'username': loginData.username,
      'password': loginData.password,
      'grant_type': 'password',
    };
    print(isi);
    String usrnm = 'USER_CLIENT_APP';
    String pass = 'password';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$usrnm:$pass'));
    final response = await ioClient.post(
      "https://smartbangkom.lan.go.id:9001/oauth/token",
      body: isi,
      headers: {'Authorization': basicAuth},
    );
    var listResponse = response.body;
    print(listResponse);
    final listConvert = json.decode(listResponse);
    LoginModel listPost = LoginModel.fromJson(listConvert);
    if (response.statusCode == 200) {
      return listPost;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> cekToken(String cek, String refCek) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    Map<String, String> isi = {
      'grant_type': 'refresh_token',
      'refresh_token': '$refCek'
    };
    String usrnm = 'USER_CLIENT_APP';
    String pass = 'password';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$usrnm:$pass'));
    String urlRef = "https://smartbangkom.lan.go.id:9001/oauth/token";
    print(cek);
    print("printcek");
    String bearer = cek;
    print(bearer);
    var hasil = await ioClient.get(
      "https://dev-smartbangkom.lan.go.id:8090/users",
      headers: {'Authorization': 'Bearer ' + bearer},
    );
    if (cek != "") {
      if (hasil.statusCode == 200) {
        return true;
      } else if (hasil.statusCode == 401) {
        bool hasilRef = await ApiProvider.refreshToken(refCek);
        if(hasilRef == true){
          return true;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }

    // final response = await ioClient.get("https://103.130.186.150:8094/master/provinsi", headers: {'Authorization': 'Bearer '+ bearer},);
    // var listResponse = response.body;
    // print(response.statusCode);
    // final listConvert=json.decode(listResponse);
    // print(listConvert);
    // CekToken listPost = CekToken.fromJson(listConvert[0]);
    // if(response.statusCode == 200){
    //   print("sini woy");
    //   return listPost;
    // } else {
    //   throw Exception("Data tidak ditemukan");
    // }
  }

  static Future<bool> refreshToken(String refCek) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    HttpClient client = new HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);

    print("woy");
    Map<String, String> isi = {
      'grant_type': 'refresh_token',
      'refresh_token': '$refCek'
    };
    print(isi);
    String usrnm = 'USER_CLIENT_APP';
    String pass = 'password';
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('$usrnm:$pass'));
    final response = await ioClient.post(
      "https://smartbangkom.lan.go.id:9001/oauth/token",
      body: isi,
      headers: {'Authorization': basicAuth},
    );
    var listResponse = response.body;
    print(listResponse);
    final listConvert = json.decode(listResponse);
    LoginModel listPost = LoginModel.fromJson(listConvert);
    if (response.statusCode == 200) {
      await preferences.setString("token", listPost.accessToken);
      await preferences.setString("refToken", listPost.refreshToken);
      return true;
    } else {
      return false;
    }
  }
}
