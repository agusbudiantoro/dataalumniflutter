import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as client;
import 'package:http/io_client.dart';

// get gambar feed
class GetGambarBase64 {
  static Future<String> getGambarBase64Nokompres(
      String token,String url) async {
        HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9091/posting/gambarori/$url";
    var apiResult =
        await ioClient.post(apiURL,headers: {"Authorization": "Bearer $token"});
        print("base64");
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      return apiResult.body;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<String> getGambarBase64Kompres(
      String token,String url) async {
        HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9091/posting/gambarcom/$url";
    var apiResult =
        await ioClient.post(apiURL,headers: {"Authorization": "Bearer $token"});
        print("base64");
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      return apiResult.body;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}


class GetGambarBase64Pengumuman{
  static Future<String> getGambarBase64NokompresPengumuman(
      String token,String url) async {
        HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9093/Pengumuman/gambarori/$url";
    var apiResult =
        await ioClient.post(apiURL,headers: {"Authorization": "Bearer $token"});
        print("base64");
    print(apiResult.body);
    if (apiResult.statusCode == 200) {
      return apiResult.body;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}

class GetGambarBase64Alumni{
  static Future<String> getGambarBase64NonKompresAlumni(
      String token,String url) async {
        HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9092/alumni/gambarori/$url";
    var apiResult =
        await ioClient.post(apiURL,headers: {"Authorization": "Bearer $token"});
        print("base64");
    print(apiResult.body);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      return apiResult.body;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<String> getGambarBase64KompresAlumni(
      String token,String url) async {
        HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = new IOClient(client);
    
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9092/alumni/gambarcom/$url";
    var apiResult =
        await ioClient.post(apiURL,headers: {"Authorization": "Bearer $token"});
        print("base64");
    print(apiResult.body);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      return apiResult.body;
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}