import 'dart:io';
import 'dart:typed_data';

import 'package:dataAlumni/model/feed/cloning_feed.dart';
import 'package:dataAlumni/model/feed/feed_model.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:http/io_client.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart' as path;
import 'package:image/image.dart' as img;
import 'package:path/path.dart';
import 'dart:math' as Math;
import 'package:async/async.dart';

class FeedInProfil {
  static Future<List<CloningPosting>> getFeedByIdUser(
      String token, int idUser, int awal, int limit) async {
    print(idUser);
    print(token);
    String hasilFotoBase64;
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9091/posting/byiduser/$idUser/$awal/$limit";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject.length);
      if (jsonObject.length == 0) {
        var hasilFinal = jsonObject
            .map<CloningPosting>((item) => CloningPosting(
                id: item["id"],
                caption: item["caption"],
                gambar: item["gambar"],
                url: item["url"],
                idcreator: item["idcreator"],
                love: item["love"],
                judul: item["judul"],
                subjudul: item["subjudul"],
                created: item["created"],
                updatedOn: item["updateOn"],
                token: item["token"],
                daftarlove: item['daftarlove'],
                jumkomentar: item['jumkomentar'],
                nama: item['nama'],
                status: item['status'],
                showCaption: false,
                foto: item['foto']))
            .toList();
        return hasilFinal;
      } else {
        var data = jsonObject
            .map<FeedPosting>((item) => FeedPosting(
                id: item["id"],
                caption: item["caption"],
                gambar: item["gambar"],
                url: item["url"],
                idcreator: item["idcreator"],
                love: item["love"],
                judul: item["judul"],
                subjudul: item["subjudul"],
                created: item["created"],
                updatedOn: item["updateOn"],
                token: item["token"],
                daftarlove: item['daftarlove'],
                jumkomentar: item['jumkomentar'],
                status: item['status'],
                nama: item['nama'],
                showCaption: false,
                foto: item['foto']))
            .toList();
        print("ulang");

        print(data);
        List<CloningPosting> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            List daftarLove = [];
            print(data[i].gambar);
            if (data[i].daftarlove == null || data[i].daftarlove == "") {
              print("masuk");
              if (data[i].gambar == null) {
                print("data ke dua");
                hasilGambarBase64 = GambarInProfil().gbrP;
              } else {
                hasilGambarBase64 =
                    await GetGambarBase64.getGambarBase64Kompres(
                        token, data[i].gambar);
                print("data ke dua else");
              }
              if (data[i].foto == null) {
                print("data ke dua");
                hasilFotoBase64 = IconOrang().gbrPeople;
              } else {
                hasilFotoBase64 =
                    await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                        token, data[i].foto);
                print("data ke dua else");
              }
              print(hasilGambarBase64);
              print('jika daftar love null');
              print(jsonEncode(data[i]));
              print(data);
              var finalData = CloningPosting(
                  id: data[i].id,
                  caption: data[i].caption,
                  gambar: data[i].gambar,
                  url: data[i].url,
                  idcreator: data[i].idcreator,
                  love: data[i].love,
                  judul: data[i].judul,
                  subjudul: data[i].subjudul,
                  created: data[i].created,
                  updatedOn: data[i].updatedOn,
                  counter: data[i].counter,
                  daftarlove: data[i].daftarlove,
                  jumkomentar: data[i].jumkomentar,
                  stringGambar: hasilGambarBase64,
                  nama: data[i].nama,
                  status: data[i].status,
                  showCaption: false,
                  cekLove: false,
                  stringFoto: hasilFotoBase64);
              var isiData = jsonEncode(finalData);
              var dataIsi = jsonDecode(isiData);
              print(isiData);
              var autoConvert = CloningPosting.fromJson(dataIsi);
              print(autoConvert);
              isi.add(autoConvert);
              print(isi[0].id);
              print("isi null");
            } else {
              print('jika daftar love tidak null');
              print(data[i].daftarlove.split(';'));
              daftarLove = data[i].daftarlove.split(';');
              print(daftarLove.length);
              if (daftarLove.length == 1) {
                print('jika length love ==1');
                if (data[i].gambar == null) {
                  hasilGambarBase64 = GambarInProfil().gbrP;
                } else {
                  hasilGambarBase64 =
                      await GetGambarBase64.getGambarBase64Kompres(
                          token, data[i].gambar);
                }
                if (data[i].foto == null) {
                print("data ke dua");
                hasilFotoBase64 = IconOrang().gbrPeople;
              } else {
                hasilFotoBase64 =
                    await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                        token, data[i].foto);
                print("data ke dua else");
              }
                print(jsonEncode(data[i]));
                print(data);
                var finalData = CloningPosting(
                    id: data[i].id,
                    caption: data[i].caption,
                    gambar: data[i].gambar,
                    url: data[i].url,
                    idcreator: data[i].idcreator,
                    love: data[i].love,
                    judul: data[i].judul,
                    subjudul: data[i].subjudul,
                    created: data[i].created,
                    updatedOn: data[i].updatedOn,
                    counter: data[i].counter,
                    daftarlove: data[i].daftarlove,
                    jumkomentar: data[i].jumkomentar,
                    stringGambar: hasilGambarBase64,
                    status: data[i].status,
                    nama: data[i].nama,
                    showCaption: false,
                    cekLove: false,
                    stringFoto: hasilFotoBase64);
                var isiData = jsonEncode(finalData);
                var dataIsi = jsonDecode(isiData);
                print(isiData);
                var autoConvert = CloningPosting.fromJson(dataIsi);
                print(autoConvert);
                isi.add(autoConvert);
                print(isi[0].id);
                print("isi");
              } else if (daftarLove.length > 1) {
                print("jika love length lebih dari satu");
                print(daftarLove[0]);
                for (var j = 0; j < daftarLove.length - 1; j++) {
                  if (daftarLove[j] == idUser.toString()) {
                    print('jika samadengan idUser');
                    if (data[i].gambar == null) {
                      hasilGambarBase64 = GambarInProfil().gbrP;
                    } else {
                      hasilGambarBase64 =
                          await GetGambarBase64.getGambarBase64Kompres(
                              token, data[i].gambar);
                    }
                    if (data[i].foto == null) {
                      print("data ke dua");
                      hasilFotoBase64 = IconOrang().gbrPeople;
                    } else {
                      hasilFotoBase64 =
                          await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                              token, data[i].foto);
                      print("data ke dua else");
                    }
                    print(jsonEncode(data[i]));
                    print(data);
                    var finalData = CloningPosting(
                        id: data[i].id,
                        caption: data[i].caption,
                        gambar: data[i].gambar,
                        url: data[i].url,
                        idcreator: data[i].idcreator,
                        love: data[i].love,
                        judul: data[i].judul,
                        subjudul: data[i].subjudul,
                        created: data[i].created,
                        updatedOn: data[i].updatedOn,
                        counter: data[i].counter,
                        daftarlove: data[i].daftarlove,
                        jumkomentar: data[i].jumkomentar,
                        stringGambar: hasilGambarBase64,
                        status: data[i].status,
                        nama: data[i].nama,
                        showCaption: false,
                        cekLove: true,
                        stringFoto: hasilFotoBase64);
                    var isiData = jsonEncode(finalData);
                    var dataIsi = jsonDecode(isiData);
                    print(isiData);
                    var autoConvert = CloningPosting.fromJson(dataIsi);
                    print(autoConvert);
                    isi.add(autoConvert);
                    print(isi[0].id);
                    print("isi samadengan idUser");
                    break;
                  } else {
                    if (j == daftarLove.length - 2) {
                      print(j);
                      print(daftarLove.length);
                      print('jika tidak sama dengan iduser dan index = 1');
                      if (data[i].gambar == null) {
                        hasilGambarBase64 = GambarInProfil().gbrP;
                      } else {
                        hasilGambarBase64 =
                            await GetGambarBase64.getGambarBase64Kompres(
                                token, data[i].gambar);
                      }
                      if (data[i].foto == null) {
                        print("data ke dua");
                        hasilFotoBase64 = IconOrang().gbrPeople;
                      } else {
                        hasilFotoBase64 =
                            await GetGambarBase64Alumni.getGambarBase64KompresAlumni(
                                token, data[i].foto);
                        print("data ke dua else");
                      }
                      print(jsonEncode(data[i]));
                      print(data);
                      var finalData = CloningPosting(
                          id: data[i].id,
                          caption: data[i].caption,
                          gambar: data[i].gambar,
                          url: data[i].url,
                          idcreator: data[i].idcreator,
                          love: data[i].love,
                          judul: data[i].judul,
                          subjudul: data[i].subjudul,
                          created: data[i].created,
                          updatedOn: data[i].updatedOn,
                          counter: data[i].counter,
                          daftarlove: data[i].daftarlove,
                          jumkomentar: data[i].jumkomentar,
                          stringGambar: hasilGambarBase64,
                          status: data[i].status,
                          nama: data[i].nama,
                          showCaption: false,
                          cekLove: false,
                          stringFoto: hasilFotoBase64);
                      var isiData = jsonEncode(finalData);
                      var dataIsi = jsonDecode(isiData);
                      print(isiData);
                      var autoConvert = CloningPosting.fromJson(dataIsi);
                      print(autoConvert);
                      isi.add(autoConvert);
                      print(isi[0].id);
                      print("isitidak sama dengan iduser");
                    }
                  }
                }
              }
            }
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("isi2");
        var valueFinal = jsonEncode(isi.toList());
        var convertFinal = jsonDecode(valueFinal);
        return isi
            .map<CloningPosting>((e) => CloningPosting(
                id: e.id,
                caption: e.caption,
                gambar: e.gambar,
                url: e.url,
                idcreator: e.idcreator,
                love: e.love,
                judul: e.judul,
                subjudul: e.subjudul,
                created: e.created,
                updatedOn: e.updatedOn,
                token: e.token,
                daftarlove: e.daftarlove,
                cekLove: e.cekLove,
                jumkomentar: e.jumkomentar,
                stringGambar: e.stringGambar,
                gambarBase64: base64Decode(e.stringGambar),
                status: e.status,
                nama: e.nama,
                showCaption: e.showCaption,
                fotoBase64: base64Decode(e.stringFoto)))
            .toList();
      }
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<bool> deletePosting(int idPost, String token) async {
    print(idPost);
    print(token);
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9091/posting/$idPost";
    // return true;
    var apiResult =
        await client.delete(apiURL, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> editFeed(
      File gambar64, CloningPosting dataPost, String tokens, int idUser) async {
    print(idUser);
    print(tokens);
    print(dataPost.caption);
    print(dataPost.id);
    String cap = dataPost.caption;
    String jud = dataPost.judul;
    String sub = dataPost.judul;
    String idUs = idUser.toString();
    String idPos = dataPost.id.toString();
    String url = "https://dev-smartbangkom.lan.go.id:9091/posting/$idPos?";

     String isi = '{'
        '"caption": "$cap",'
        '"judul": "$jud",'
        '"subjudul": "$sub",'
        '"idcreator": "$idUs",'
        '"url": ""'
        '}';
    var request = client.MultipartRequest("POST", Uri.parse(url));
    request.fields["posting"] = isi;
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $tokens'
    });

    if (gambar64 != null) {
      String fileName = basename(gambar64.path);
      String base64Image = base64Encode(gambar64.readAsBytesSync());
      print("File base name: $fileName");
      var stream =
          new client.ByteStream(DelegatingStream.typed(gambar64.openRead()));
      var length = await gambar64.length();
      var pic = await client.MultipartFile("gbrpost", stream, length,
        filename: fileName,
        contentType: MediaType("multipart/form-data", "multipart/form-data"));
        request.files.add(pic);
    } 
    client.Response response = await client.Response.fromStream(await request.send());
    print(response.toString());
    print("kkk");
    print(response.statusCode);
    var data = jsonDecode(response.body);
    print(response.body);
    var finalData  = CloningPosting.fromJson(data);
    print("hasil");
    print(finalData.id);
    if (response.statusCode == 200) {
      
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> editFeedYtb(CloningPosting dataPost, String tokens, int idUser) async {
    print(idUser);
    print(tokens);
    print(dataPost.caption);
    print(dataPost.id);
    print(dataPost.url);
    String cap = dataPost.caption;
    String jud = dataPost.judul;
    String sub = dataPost.judul;
    String idUs = idUser.toString();
    String idPos = dataPost.id.toString();
    String urlYtb = dataPost.url.toString();
    String url = "https://dev-smartbangkom.lan.go.id:9091/posting/$idPos?";

     String isi = '{'
        '"caption": "$cap",'
        '"judul": "$jud",'
        '"subjudul": "$sub",'
        '"idcreator": "$idUs",'
        '"url": "$urlYtb"'
        '}';
    var request = client.MultipartRequest("POST", Uri.parse(url));
    request.fields["posting"] = isi;
    request.headers.addAll({
      'Content-type': 'multipart/form-data',
      'Accept': '*/*',
      HttpHeaders.authorizationHeader: 'Bearer $tokens'
    });

    var response = await request.send();
    print(response.toString());
    print("kkk");
    print(response.statusCode);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> setStatusEdit(int idPost, String token) async {
    print(idPost);
    
    String url = "https://dev-smartbangkom.lan.go.id:9091/posting/setstatus/$idPost";

     var apiResult =
        await client.post(url, headers: {"Authorization": "Bearer $token"});
    if (apiResult.statusCode == 200) {
      print("sukses status");
      return true;
    } else {
      return false;
    }
  }
}
