import 'package:dataAlumni/model/reqAlumni.dart';
import 'package:dataAlumni/network/api_provider.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'package:dataAlumni/model/modelAlumniTerbaik.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AlumniTerbaikApi {
  static Future<List<AlumiTerbaikModel>> getDataAlumniTerbaik(
      String token, String refCek) async {
    print(token);
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9093/alumniTerbaik/getallbydiklat";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      print(apiResult.body);
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject);
        var hasilFinal = jsonObject
            .map<AlumiTerbaikModel>((item) => AlumiTerbaikModel(
                id: item["id"],
                judul: item["judul"],
                reqalumni: json.encode(item["reqalumni"]),
                tahun: item["tahun"],
                ))
            .toList();
            // print(hasilFinal[0].reqalumni);
        return hasilFinal;
      // } else {
      //   var data = jsonObject
      //       .map<AlumiTerbaikModel>((item) => AlumiTerbaikModel(
      //           id: item["id"],
      //           judul: item["judul"],
      //           reqalumni: item["reqalumni"],
      //           tahun: item["tahun"],
      //           ))
      //       .toList();
      //   print("ulang");
      //   print(data);
      //   List<AlumiTerbaikModel> isi = [];
      //   for (var i = 0; i < data.length; i++) {
      //     print(i);
      //     if (i < data.length) {
      //       print("jika i < data.length");
      //       List daftarLove = [];
      //       print(data[i]);
      //       if (data[i].foto == null) {
      //         print("data ke dua");
      //         hasilGambarBase64 = GambarNull().gbr;
      //       } else {
      //         hasilGambarBase64 =
      //             await GetGambarBase64Pengumuman.getGambarBase64NokompresPengumuman(
      //                 token, data[i].foto);
      //         print("data ke dua else");
      //       }
      //       print(hasilGambarBase64);
      //       print('jika daftar love null');
      //       print(jsonEncode(data[i]));
      //       print(data);
      //       var finalData = AlumiTerbaikModel(
      //           id: data[i].id,
      //           idUser: data[i].idUser,
      //           nama: data[i].nama,
      //           foto: data[i].foto,
      //           jenisKelamin: data[i].jenisKelamin,
      //           pangkat: data[i].pangkat,
      //           asalInstansi: data[i].asalInstansi,
      //           jabatan: data[i].jabatan,
      //           gol: data[i].gol,
      //           stringGambar: hasilGambarBase64,
      //           tahun: data[i].tahun,
      //           juduldiklat: data[i].juduldiklat);
      //       var isiData = jsonEncode(finalData);
      //       var dataIsi = jsonDecode(isiData);
      //       print(isiData);
      //       var autoConvert = AlumiTerbaikModel.fromJson(dataIsi);
      //       print(autoConvert);
      //       isi.add(autoConvert);
      //       print(isi[0].id);
      //       print("isi null");
      //     } else if (i == data.length - 1) {
      //       break;
      //     }
      //   }
      //   print("isi2");
      //   var valueFinal = jsonEncode(isi.toList());
      //   var convertFinal = jsonDecode(valueFinal);
      //   return isi
      //       .map<AlumiTerbaikModel>((e) => AlumiTerbaikModel(
      //           id: e.id,
      //           idUser: e.idUser,
      //           nama: e.nama,
      //           foto: e.foto,
      //           jenisKelamin: e.jenisKelamin,
      //           pangkat: e.pangkat,
      //           asalInstansi: e.asalInstansi,
      //           jabatan: e.jabatan,
      //           gol: e.gol,
      //           stringGambar: hasilGambarBase64,
      //           gambarBase64: base64Decode(e.stringGambar),
      //           tahun: e.tahun,
      //           juduldiklat: e.juduldiklat
      //           ))
      //       .toList();
      // }
    } else if(apiResult.statusCode == 401){
      bool hasilRef = await ApiProvider.refreshToken(refCek);
      if(hasilRef == true){
        return AlumniTerbaikApi.getDataAlumniTerbaik(token, refCek);
      }
    } else{
      throw Exception("Data tidak ditemukan");
    }
  }

  static Future<List<Reqalumni>>decodeJsonItem(item) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString("token");
   var jsonObject = json.decode(item) as List;
  //  Reqalumni result = new Reqalumni();
  print("woyyy");
  String hasilGambarBase64;
   var data = jsonObject
            .map<Reqalumni>((item) => Reqalumni(
                id: item["id"],
                idUser: item["idUser"],
                nama: item["nama"],
                foto: item["foto"],
                jenisKelamin: item["jenisKelamin"],
                pangkat: item["pangkat"],
                asalInstansi: item["asalInstansi"],
                jabatan: item["jabatan"],
                gol: item["gol"],
                juduldiklat: item["juduldiklat"],
                urutan: item["urutan"],
                tahun: item["tahun"],
                ))
            .toList();
            List<Reqalumni> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            print(data[i].foto);
            if (data[i].foto == null) {
              print("data ke dua");
              hasilGambarBase64 = IconOrang().gbrPeople;
            } else {
              hasilGambarBase64 =
                  await GetGambarBase64Alumni.getGambarBase64NonKompresAlumni(
                      token, data[i].foto);
              print("data ke dua else");
            }
            print(hasilGambarBase64);
            print('jika daftar love null');
            print(jsonEncode(data[i]));
            print(data);
            var finalData = Reqalumni(
                id: data[i].id,
                idUser: data[i].idUser,
                nama: data[i].nama,
                foto: data[i].foto,
                jenisKelamin: data[i].jenisKelamin,
                pangkat: data[i].pangkat,
                asalInstansi: data[i].asalInstansi,
                jabatan: data[i].jabatan,
                gol: data[i].gol,
                stringFoto: hasilGambarBase64);
            var isiData = jsonEncode(finalData);
            var dataIsi = jsonDecode(isiData);
            print(isiData);
            var autoConvert = Reqalumni.fromJson(dataIsi);
            print(autoConvert);
            isi.add(autoConvert);
            print("isi null");
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("ini isinya");
        for(var j=0;j<isi.length;j++){
          print("pengulangan");
          print(isi[j].foto);
        }
        return isi
            .map<Reqalumni>((e) => Reqalumni(
                id: e.id,
                idUser: e.idUser,
                nama: e.nama,
                foto: e.foto,
                jenisKelamin: e.jenisKelamin,
                pangkat: e.pangkat,
                asalInstansi: e.asalInstansi,
                jabatan: e.jabatan,
                gol: e.gol,
                stringFoto: hasilGambarBase64,
                fotoBase64: base64Decode(e.stringFoto),
                ))
            .toList();
  }
  
}
