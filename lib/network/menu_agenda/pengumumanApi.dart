import 'package:dataAlumni/model/agenda/modelPengumuman.dart';
import 'package:dataAlumni/network/getGambar.dart';
import 'package:dataAlumni/util/Widget/gambarBase64Tiruan.dart';
import 'dart:convert';
import 'package:http/http.dart' as client;
import 'dart:io';
import 'package:async/async.dart';

class PengumumanApi {
  static Future<List<ModelPengumuman>> connectToApi(
      String token, int start, int limit, int idUser) async {
    print(start);
    print(limit);
    print(idUser);
    print(token);
    String hasilGambarBase64;
    String apiURL =
        "https://dev-smartbangkom.lan.go.id:9093/Pengumuman/%7Bpage%7D/%7Bsize%7D?page=$start&size=$limit";
    var apiResult =
        await client.get(apiURL, headers: {"Authorization": "Bearer $token"});
    print("feed");
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      var jsonObject = json.decode(apiResult.body) as List;
      print(jsonObject.length);
      if (jsonObject.length == 0) {
        var hasilFinal = jsonObject
            .map<ModelPengumuman>((item) => ModelPengumuman(
                id: item["id"],
                gambar: item["gambar"],
                judul: item["judul"],
                created: item["created"],
                updatedOn: item["updateOn"],
                creator: item["creator"],
                deskripsi: item["deskripsi"],
                tglpelaksanaan: item["tglpelaksanaan"],
                showDeskripsi: false
                ))
            .toList();
        return hasilFinal;
      } else {
        var data = jsonObject
            .map<ModelPengumuman>((item) => ModelPengumuman(
                id: item["id"],
                gambar: item["gambar"],
                judul: item["judul"],
                created: item["created"],
                updatedOn: item["updateOn"],
                creator: item["creator"],
                deskripsi: item["deskripsi"],
                tglpelaksanaan: item["tglpelaksanaan"],
                showDeskripsi: false))
            .toList();
        print("ulang");
        print(data);
        List<ModelPengumuman> isi = [];
        for (var i = 0; i < data.length; i++) {
          print(i);
          if (i < data.length) {
            print("jika i < data.length");
            List daftarLove = [];
            print(data[i].gambar);
            if (data[i].gambar == null) {
              print("data ke dua");
              hasilGambarBase64 = GambarNull().gbr;
            } else {
              hasilGambarBase64 =
                  await GetGambarBase64Pengumuman.getGambarBase64NokompresPengumuman(
                      token, data[i].gambar);
              print("data ke dua else");
            }
            print(hasilGambarBase64);
            print('jika daftar love null');
            print(jsonEncode(data[i]));
            print(data);
            var finalData = ModelPengumuman(
                id: data[i].id,
                gambar: data[i].gambar,
                judul: data[i].judul,
                created: data[i].created,
                updatedOn: data[i].updatedOn,
                creator: data[i].creator,
                deskripsi: data[i].deskripsi,
                tglpelaksanaan: data[i].tglpelaksanaan,
                stringGambar: hasilGambarBase64,
                showDeskripsi: data[i].showDeskripsi);
            var isiData = jsonEncode(finalData);
            var dataIsi = jsonDecode(isiData);
            print(isiData);
            var autoConvert = ModelPengumuman.fromJson(dataIsi);
            print(autoConvert);
            isi.add(autoConvert);
            print(isi[0].id);
            print("isi null");
          } else if (i == data.length - 1) {
            break;
          }
        }
        print("isi2");
        var valueFinal = jsonEncode(isi.toList());
        var convertFinal = jsonDecode(valueFinal);
        return isi
            .map<ModelPengumuman>((e) => ModelPengumuman(
                id: e.id,
                gambar: e.gambar,
                judul: e.judul,
                created: e.created,
                updatedOn: e.updatedOn,
                creator: e.creator,
                deskripsi: e.deskripsi,
                tglpelaksanaan: e.tglpelaksanaan,
                stringGambar: hasilGambarBase64,
                gambarBase64: base64Decode(e.stringGambar),
                showDeskripsi: e.showDeskripsi
                ))
            .toList();
      }
    } else {
      throw Exception("Data tidak ditemukan");
    }
  }
}
