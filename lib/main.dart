import 'package:dataAlumni/bloc/bloc_auth/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'SplashScreen/Splash.dart';
import 'package:upgrader/upgrader.dart';
void main() {
  runApp(MyApp());
  
}

class MyApp extends StatelessWidget {
  MyApp({
    Key key,
  }) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Upgrader Example',
       home: MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
            create: (context) => AuthBloc(),
          ),
          BlocProvider<AuthBloc>(
            create: (context) => AuthBloc()..add(CekTokenEvent()),
          )
        ],
        child: OPSplashScreen()
    ));
  }
}


