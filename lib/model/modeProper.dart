class ModelProper {
  String abstrak;
  String evidence;
  int id;
  int idAlumni;
  int idSmartbangkom;
  String judul;
  String kataKunci;
  int tahun;

  ModelProper(
      {this.abstrak,
      this.evidence,
      this.id,
      this.idAlumni,
      this.idSmartbangkom,
      this.judul,
      this.kataKunci,
      this.tahun});

  ModelProper.fromJson(Map<String, dynamic> json) {
    abstrak = json['abstrak'];
    evidence = json['evidence'];
    id = json['id'];
    idAlumni = json['idAlumni'];
    idSmartbangkom = json['idSmartbangkom'];
    judul = json['judul'];
    kataKunci = json['kataKunci'];
    tahun = json['tahun'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['abstrak'] = this.abstrak;
    data['evidence'] = this.evidence;
    data['id'] = this.id;
    data['idAlumni'] = this.idAlumni;
    data['idSmartbangkom'] = this.idSmartbangkom;
    data['judul'] = this.judul;
    data['kataKunci'] = this.kataKunci;
    data['tahun'] = this.tahun;
    return data;
  }
}