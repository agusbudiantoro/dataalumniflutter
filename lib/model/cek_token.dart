import 'package:flutter/material.dart';

class CekToken {
  String error;
  String errorDescription;
  String token;
  int id;
  int idSipka;
  String namaProvinsi;
  Null createdBy;
  int created;
  int updatedOn;

  CekToken({this.error, this.errorDescription, this.token, this.id,
      this.idSipka,
      this.namaProvinsi,
      this.createdBy,
      this.created,
      this.updatedOn});

  CekToken.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    errorDescription = json['error_description'];
    token = json['token'];
    id = json['id'];
    idSipka = json['idSipka'];
    namaProvinsi = json['namaProvinsi'];
    createdBy = json['created_by'];
    created = json['created'];
    updatedOn = json['updatedOn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['error_description'] = this.errorDescription;
    data['token'] = this.token;
    data['id'] = this.id;
    data['idSipka'] = this.idSipka;
    data['namaProvinsi'] = this.namaProvinsi;
    data['created_by'] = this.createdBy;
    data['created'] = this.created;
    data['updatedOn'] = this.updatedOn;
    return data;
  }
}