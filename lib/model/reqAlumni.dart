import 'dart:typed_data';

class Reqalumni {
  int id;
  int idUser;
  String nama;
  String foto;
  String jenisKelamin;
  String pangkat;
  String asalInstansi;
  String jabatan;
  String gol;
  String juduldiklat;
  String tahun;
  int urutan;
  String stringFoto;
  Uint8List fotoBase64;

  Reqalumni(
      {this.id,
      this.idUser,
      this.nama,
      this.foto,
      this.jenisKelamin,
      this.pangkat,
      this.asalInstansi,
      this.jabatan,
      this.gol,
      this.juduldiklat,
      this.tahun,
      this.urutan,
      this.stringFoto,
      this.fotoBase64
      });

  Reqalumni.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idUser = json['idUser'];
    nama = json['nama'];
    foto = json['foto'];
    jenisKelamin = json['jenisKelamin'];
    pangkat = json['pangkat'];
    asalInstansi = json['asalInstansi'];
    jabatan = json['jabatan'];
    gol = json['gol'];
    juduldiklat = json['juduldiklat'];
    tahun = json['tahun'];
    urutan = json['urutan'];
    stringFoto = json['stringFoto'];
    fotoBase64 = json['fotoBase64'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idUser'] = this.idUser;
    data['nama'] = this.nama;
    data['foto'] = this.foto;
    data['jenisKelamin'] = this.jenisKelamin;
    data['pangkat'] = this.pangkat;
    data['asalInstansi'] = this.asalInstansi;
    data['jabatan'] = this.jabatan;
    data['gol'] = this.gol;
    data['juduldiklat'] = this.juduldiklat;
    data['tahun'] = this.tahun;
    data['urutan'] = this.urutan;
    data['stringFoto'] = this.stringFoto;
    data['fotoBase64'] = this.fotoBase64;
    return data;
  }
}