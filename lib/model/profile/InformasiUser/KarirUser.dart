class ModelKarir {
  int id;
  int idAlumni;
  String tahun;
  String posisi;
  String tempat;

  ModelKarir({this.id, this.idAlumni, this.tahun, this.posisi, this.tempat});

  ModelKarir.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idAlumni = json['idAlumni'];
    tahun = json['tahun'];
    posisi = json['posisi'];
    tempat = json['tempat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idAlumni'] = this.idAlumni;
    data['tahun'] = this.tahun;
    data['posisi'] = this.posisi;
    data['tempat'] = this.tempat;
    return data;
  }
}