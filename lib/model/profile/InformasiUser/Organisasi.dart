class ModelOrganisasi {
  int id;
  int idAlumni;
  String namaOrganisasi;
  String jabatan;
  String bidangOrganisasi;
  String tahun;

  ModelOrganisasi(
      {this.id,
      this.idAlumni,
      this.namaOrganisasi,
      this.jabatan,
      this.bidangOrganisasi,
      this.tahun});

  ModelOrganisasi.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idAlumni = json['idAlumni'];
    namaOrganisasi = json['namaOrganisasi'];
    jabatan = json['jabatan'];
    bidangOrganisasi = json['bidangOrganisasi'];
    tahun = json['tahun'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idAlumni'] = this.idAlumni;
    data['namaOrganisasi'] = this.namaOrganisasi;
    data['jabatan'] = this.jabatan;
    data['bidangOrganisasi'] = this.bidangOrganisasi;
    data['tahun'] = this.tahun;
    return data;
  }
}