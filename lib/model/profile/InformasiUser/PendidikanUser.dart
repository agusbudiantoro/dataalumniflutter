class ModelPendidikanUser {
  int id;
  int idAlumni;
  String jenjang;
  String namauniv;
  String lokasi;
  String tahun;

  ModelPendidikanUser(
      {this.id,
      this.idAlumni,
      this.jenjang,
      this.namauniv,
      this.lokasi,
      this.tahun});

  ModelPendidikanUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idAlumni = json['idAlumni'];
    jenjang = json['jenjang'];
    namauniv = json['namauniv'];
    lokasi = json['lokasi'];
    tahun = json['tahun'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idAlumni'] = this.idAlumni;
    data['jenjang'] = this.jenjang;
    data['namauniv'] = this.namauniv;
    data['lokasi'] = this.lokasi;
    data['tahun'] = this.tahun;
    return data;
  }
}