class RegisterModel {
  int id;
  String nama;
  String nip;
  String password;
  int verifikasi;

  RegisterModel({this.id, this.nama, this.nip, this.password, this.verifikasi});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
    nip = json['nip'];
    password = json['password'];
    verifikasi = json['verifikasi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama'] = this.nama;
    data['nip'] = this.nip;
    data['password'] = this.password;
    data['verifikasi'] = this.verifikasi;
    return data;
  }
}