import 'dart:typed_data';

import 'package:dataAlumni/model/reqAlumni.dart';

class AlumiTerbaikModel {
  int id;
  String judul;
  int tahun;
  String reqalumni;

  AlumiTerbaikModel({this.id, this.judul, this.tahun, this.reqalumni});

  AlumiTerbaikModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    judul = json['judul'];
    tahun = json['tahun'];
    reqalumni = json['reqalumni'];
    // if (json['reqalumni'] != null) {
    //   reqalumni = new List<Reqalumni>();
    //   json['reqalumni'].forEach((v) {
    //     reqalumni.add(new Reqalumni.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['judul'] = this.judul;
    data['tahun'] = this.tahun;
    data['reqalumni'] = this.reqalumni;
    // if (this.reqalumni != null) {
    //   data['reqalumni'] = this.reqalumni.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}

