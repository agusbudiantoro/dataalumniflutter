import 'dart:convert';

import 'dart:typed_data';

// List<KomentarModel> photosModelFromJson(String str) => List<KomentarModel>.from(
//     json.decode(str).map((x) => KomentarModel.fromJson(x)));

// String photosModelToJson(List<KomentarModel> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class KomentarModel {
  String created;
  int id;
  int idKomentar;
  int idKomentator;
  int idPosting;
  String isiKomentar;
  String updatedOn;
  String namakomentator;
  String foto;
  String stringFoto;
  Uint8List fotoBase64;

  KomentarModel(
      {this.created,
      this.id,
      this.idKomentar,
      this.idKomentator,
      this.idPosting,
      this.isiKomentar,
      this.updatedOn, this.namakomentator, this.foto,
      this.stringFoto,
      this.fotoBase64});

  KomentarModel.fromJson(Map<String, dynamic> json) {
    created = json['created'];
    id = json['id'];
    idKomentar = json['idKomentar'];
    idKomentator = json['idKomentator'];
    idPosting = json['idPosting'];
    isiKomentar = json['isiKomentar'];
    updatedOn = json['updateon'];
    namakomentator = json['namakomentator'];
    foto = json['foto'];
    stringFoto = json['stringFoto'];
    fotoBase64 = json['fotoBase64'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created'] = this.created;
    data['id'] = this.id;
    data['idKomentar'] = this.idKomentar;
    data['idKomentator'] = this.idKomentator;
    data['idPosting'] = this.idPosting;
    data['isiKomentar'] = this.isiKomentar;
    data['updateon'] = this.updatedOn;
    data['namakomentator'] = this.namakomentator;
    data['foto'] = this.foto;
    data['stringFoto'] = this.stringFoto;
    data['fotoBase64'] = this.fotoBase64;
    return data;
  }
}