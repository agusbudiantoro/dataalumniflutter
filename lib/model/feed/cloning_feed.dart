import 'dart:convert';

import 'dart:typed_data';

// List<FeedPosting> feedPostingFromJson(String str) => List<FeedPosting>.from(
//   json.decode(str).map((x) => FeedPosting.fromJson(x))
// );

// String feedPostingToJson(List<FeedPosting> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CloningPosting {
  int id;
  String caption;
  String gambar;
  String url;
  int idcreator;
  int love;
  String judul;
  String subjudul;
  String created;
  String updatedOn;
  String token;
  int counter;
  String daftarlove;
  bool cekLove;
  int jumkomentar;
  String stringGambar;
  Uint8List gambarBase64;
  String nama;
  bool showCaption;
  int status;
  String foto;
  String stringFoto;
  Uint8List fotoBase64;

  CloningPosting(
      {this.id,
      this.caption,
      this.gambar,
      this.url,
      this.idcreator,
      this.love,
      this.judul,
      this.subjudul,
      this.created,
      this.updatedOn,
      this.token,
      this.counter,
      this.daftarlove,
      this.cekLove,
      this.jumkomentar,
      this.gambarBase64,
      this.stringGambar,
      this.nama,
      this.showCaption,
      this.status,
      this.foto,
      this.stringFoto,
      this.fotoBase64
      });

  CloningPosting.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    caption = json['caption'];
    gambar = json['gambar'];
    url = json['url'];
    idcreator = json['idcreator'];
    love = json['love'];
    judul = json['judul'];
    subjudul = json['subjudul'];
    created = json['created'];
    updatedOn = json['updatedOn'];
    token = json['token'];
    counter = json['counter'];
    daftarlove = json['daftarlove'];
    cekLove = json['cekLove'];
    jumkomentar = json['jumkomentar'];
    gambarBase64 = json['gambarBase64'];
    stringGambar = json['stringGambar'];
    nama = json['nama'];
    showCaption = json['showCaption'];
    status = json['status'];
    foto = json['foto'];
    stringFoto = json['stringFoto'];
    fotoBase64 = json['fotoBase64'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['caption'] = this.caption;
    data['gambar'] = this.gambar;
    data['url'] = this.url;
    data['idcreator'] = this.idcreator;
    data['love'] = this.love;
    data['judul'] = this.judul;
    data['subjudul'] = this.subjudul;
    data['created'] = this.created;
    data['updatedOn'] = this.updatedOn;
    data['token'] = this.token;
    data['counter'] = this.counter;
    data['daftarlove'] = this.daftarlove;
    data['cekLove'] = this.cekLove;
    data['jumkomentar'] = this.jumkomentar;
    data['gambarBase64'] = this.gambarBase64;
    data['stringGambar'] = this.stringGambar;
    data['nama'] = this.nama;
    data['showCaption'] = this.showCaption;
    data['status'] = this.status;
    data['foto'] = this.foto;
    data['stringFoto'] = this.stringFoto;
    data['fotoBase64'] = this.fotoBase64;
    return data;
  }
}