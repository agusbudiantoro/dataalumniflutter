class LoginModel {
  String accessToken;
  String tokenType;
  String refreshToken;
  int expiresIn;
  String scope;
  String email;
  int iduser;
  String namauser;
  String lemdik;
  int idlemdik;
  String akronim;
  String nik;
  String jti;
  String username;
  String password;

  LoginModel(
      {this.accessToken,
      this.tokenType,
      this.refreshToken,
      this.expiresIn,
      this.scope,
      this.email,
      this.iduser,
      this.namauser,
      this.lemdik,
      this.idlemdik,
      this.akronim,
      this.nik,
      this.jti,
      this.username,
      this.password
      });

  LoginModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    tokenType = json['token_type'];
    refreshToken = json['refresh_token'];
    expiresIn = json['expires_in'];
    scope = json['scope'];
    email = json['email'];
    iduser = json['iduser'];
    namauser = json['namauser'];
    lemdik = json['lemdik'];
    idlemdik = json['idlemdik'];
    akronim = json['akronim'];
    nik = json['nik'];
    jti = json['jti'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    data['token_type'] = this.tokenType;
    data['refresh_token'] = this.refreshToken;
    data['expires_in'] = this.expiresIn;
    data['scope'] = this.scope;
    data['email'] = this.email;
    data['iduser'] = this.iduser;
    data['namauser'] = this.namauser;
    data['lemdik'] = this.lemdik;
    data['idlemdik'] = this.idlemdik;
    data['akronim'] = this.akronim;
    data['nik'] = this.nik;
    data['jti'] = this.jti;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}