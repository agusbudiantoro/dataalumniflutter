import 'dart:typed_data';

class ModelPengumuman {
  String created;
  int creator;
  String deskripsi;
  String gambar;
  int id;
  String judul;
  String tglpelaksanaan;
  String updatedOn;
  String stringGambar;
  Uint8List gambarBase64;
  bool showDeskripsi;

  ModelPengumuman(
      {this.created,
      this.creator,
      this.deskripsi,
      this.gambar,
      this.id,
      this.judul,
      this.tglpelaksanaan,
      this.updatedOn,
      this.stringGambar,
      this.gambarBase64,
      this.showDeskripsi
      });

  ModelPengumuman.fromJson(Map<String, dynamic> json) {
    created = json['created'];
    creator = json['creator'];
    deskripsi = json['deskripsi'];
    gambar = json['gambar'];
    id = json['id'];
    judul = json['judul'];
    tglpelaksanaan = json['tglpelaksanaan'];
    updatedOn = json['updatedOn'];
    stringGambar = json['stringGambar'];
    gambarBase64 = json['gambarBase64'];
    showDeskripsi = json['showDeskripsi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created'] = this.created;
    data['creator'] = this.creator;
    data['deskripsi'] = this.deskripsi;
    data['gambar'] = this.gambar;
    data['id'] = this.id;
    data['judul'] = this.judul;
    data['tglpelaksanaan'] = this.tglpelaksanaan;
    data['updatedOn'] = this.updatedOn;
    data['stringGambar'] = this.stringGambar;
    data['gambarBase64'] = this.gambarBase64;
    data['showDeskripsi'] = this.showDeskripsi;
    return data;
  }
}