import 'dart:typed_data';

class DataAlumni {
  int id;
  int idSmartbangkom;
  int idSipkabangkom;
  int idUser;
  String nama;
  String foto;
  String expertise;
  String organisasi;
  String nomorId;
  String tempatLahir;
  String jenisKelamin;
  String agama;
  String pangkat;
  String email;
  String asalInstansi;
  String alamatKantor;
  String noTelp;
  String nomorKra;
  String jabatan;
  String gol;
  String stringGambar;
  Uint8List gambarBase64;

  DataAlumni(
      {this.id,
      this.idSmartbangkom,
      this.idSipkabangkom,
      this.idUser,
      this.nama,
      this.foto,
      this.expertise,
      this.organisasi,
      this.nomorId,
      this.tempatLahir,
      this.jenisKelamin,
      this.agama,
      this.pangkat,
      this.email,
      this.asalInstansi,
      this.alamatKantor,
      this.noTelp,
      this.nomorKra,
      this.jabatan,
      this.gol,
      this.stringGambar,
      this.gambarBase64});

  DataAlumni.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idSmartbangkom = json['idSmartbangkom'];
    idSipkabangkom = json['idSipkabangkom'];
    idUser = json['idUser'];
    nama = json['nama'];
    foto = json['foto'];
    expertise = json['expertise'];
    organisasi = json['organisasi'];
    nomorId = json['nomorId'];
    tempatLahir = json['tempatLahir'];
    jenisKelamin = json['jenisKelamin'];
    agama = json['agama'];
    pangkat = json['pangkat'];
    email = json['email'];
    asalInstansi = json['asalInstansi'];
    alamatKantor = json['alamatKantor'];
    noTelp = json['noTelp'];
    nomorKra = json['nomorKra'];
    jabatan = json['jabatan'];
    gol = json['gol'];
    stringGambar = json['stringGambar'];
    gambarBase64 = json['gambarBase64;'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['idSmartbangkom'] = this.idSmartbangkom;
    data['idSipkabangkom'] = this.idSipkabangkom;
    data['idUser'] = this.idUser;
    data['nama'] = this.nama;
    data['foto'] = this.foto;
    data['expertise'] = this.expertise;
    data['organisasi'] = this.organisasi;
    data['nomorId'] = this.nomorId;
    data['tempatLahir'] = this.tempatLahir;
    data['jenisKelamin'] = this.jenisKelamin;
    data['agama'] = this.agama;
    data['pangkat'] = this.pangkat;
    data['email'] = this.email;
    data['asalInstansi'] = this.asalInstansi;
    data['alamatKantor'] = this.alamatKantor;
    data['noTelp'] = this.noTelp;
    data['nomorKra'] = this.nomorKra;
    data['jabatan'] = this.jabatan;
    data['gol'] = this.gol;
    data['stringGambar'] = this.stringGambar;
    data['Uint8List gambarBase64;'] = this.gambarBase64;
    return data;
  }
}