import 'package:dataAlumni/bloc/bloc_auth/auth_bloc.dart';
import 'package:dataAlumni/model/cek_token.dart';
import 'package:flutter/material.dart';
import 'package:dataAlumni/util/Style.dart';
import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/Pages/login.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nb_utils/nb_utils.dart' as style;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:upgrader/upgrader.dart';
import 'package:url_launcher/url_launcher.dart';

class OPWalkThroughScreen extends StatefulWidget {
  static String tag = '/OPWalkThroughScreen';

  @override
  _OPWalkThroughScreenState createState() => _OPWalkThroughScreenState();
}

class _OPWalkThroughScreenState extends State<OPWalkThroughScreen> {
  PageController pageController = PageController(initialPage: 0);
  int pageChanged = 0;
  DateTime backbuttonpressedTime;

  String tokens;
  @override
  void initState() {
    super.initState();
    cekstatus().then((value) {});
    Upgrader().clearSavedSettings();
    print("cek dulu");
  }

  Future<bool> cekstatus() async {
    final preferences = await SharedPreferences.getInstance();
    tokens = preferences.getString("token");
    // print("sono");
    // print(tokens);
  }

  @override
  Widget build(BuildContext context) {
    Upgrader().clearSavedSettings();
    final appcastURL =
        'https://gitlab.com/agusbudiantoro/dataalumniflutter/-/raw/master/lib/cekUpdate.xml';
    final cfg = AppcastConfiguration(url: appcastURL, supportedOS: ['android']);
    final linkApk="https://play.google.com/store/apps/details?id=lan.go.id.dataalumni";
    final _loginBlocBloc = AuthBloc();
    Size size = MediaQuery.of(context).size;
    double width = MediaQuery.of(context).size.width;
    List<Widget> buildDotIndicator() {
      List<Widget> list = [];
      for (int i = 0; i <= 2; i++) {
        list.add(i == pageChanged
            ? oPDotIndicator(isActive: true)
            : oPDotIndicator(isActive: false));
      }

      return list;
    }

    return SafeArea(
      child: Scaffold(
          body: UpgradeAlert(
            dialogStyle: UpgradeDialogStyle.cupertino,
        appcastConfig: cfg,
        debugLogging: true,
        onUpdate: () {
          launch('$linkApk');
          // print(Upgrader().isUpdateAvailable().toString());
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                height: double.infinity,
                width: double.infinity,
                child: PageView(
                  onPageChanged: (index) {
                    setState(() {
                      pageChanged = index;
                    });
                  },
                  controller: pageController,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          child: Container(
                            height: context.height() * 0.5,
                            padding: EdgeInsets.only(top: 16),
                            alignment: Alignment.topRight,
                            child: Image(
                              image: AssetImage("images/dataAlumni/share.png"),
                              width: width,
                            ),
                          ),
                          height: size.height * 0.5,
                        ),
                        Positioned(
                          top: size.height / 1.8,
                          left: 24,
                          right: 24,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Share My Activity",
                                  textAlign: TextAlign.center,
                                  style: boldTextStyle(size: 20)),
                              SizedBox(height: 16.0),
                              Text(
                                  "Berbagi informasi seputar pekerjaan, kegiatan sosial dan yang lainnya.",
                                  textAlign: TextAlign.center,
                                  style: secondaryTextStyle(size: 18))
                            ],
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          height: context.height() * 0.5,
                          child: Image(
                            image: AssetImage("images/dataAlumni/net.png"),
                            width: width,
                          ),
                        ),
                        Positioned(
                          top: size.height / 1.8,
                          left: 24,
                          right: 24,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Networking",
                                  textAlign: TextAlign.center,
                                  style: boldTextStyle(size: 20)),
                              SizedBox(height: 16.0),
                              Text(
                                  "Memperluas jejaring kerja, menambah pertemanan dan menjalin koneksi dan kolaborasi.",
                                  maxLines: 3,
                                  textAlign: TextAlign.center,
                                  style: secondaryTextStyle(size: 18))
                            ],
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          height: context.height() * 0.5,
                          child: Image(
                            image: AssetImage("images/dataAlumni/dev.png"),
                            width: width,
                          ),
                        ),
                        Positioned(
                          top: size.height / 1.8,
                          left: 24,
                          right: 24,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text("Profesional Development",
                                  textAlign: TextAlign.center,
                                  style: boldTextStyle(size: 20)),
                              SizedBox(height: 16.0),
                              Text(
                                  "Pengembangan karir dan profesionalitas melalui peningkatan pengetahun dan berbagi pengalaman.",
                                  textAlign: TextAlign.center,
                                  style: secondaryTextStyle(size: 18))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20),
              width: size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  pageChanged == 0
                      ? Expanded(
                          child: SizedBox(),
                          flex: 1,
                        )
                      : Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.only(
                              left: 20,
                            ),
                            child: SliderButton(
                              color: Color(0xFFFF6E18),
                              title: 'Back',
                              onPressed: () {
                                pageController.previousPage(
                                    duration: Duration(milliseconds: 300),
                                    curve: Curves.easeInCirc);
                              },
                            ),
                          ),
                        ),
                  Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: buildDotIndicator(),
                    ),
                  ),
                  pageChanged != 2
                      ? Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.only(
                              right: 20,
                            ),
                            child: SliderButton(
                              color: Color(0xFF343EDB),
                              title: 'Next',
                              onPressed: () {
                                pageController.nextPage(
                                    duration: Duration(milliseconds: 250),
                                    curve: Curves.easeInCirc);
                              },
                            ),
                          ),
                        )
                      : _buttonMain()
                ],
              ),
            )
          ],
        ),
      )),
    );
  }

  _buttonMain() {
    return Expanded(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.only(
            right: 20,
          ),
          child: SliderButton(
            color: Color(0xFF343EDB),
            title: 'Login',
            onPressed: () {
              Navigator.of(context).pop(true);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FoodSignIn(tanda: true),
                ),
              );
            },
          ),
        ));
  }
}
