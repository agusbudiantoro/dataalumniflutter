import 'package:dataAlumni/model/T10Models.dart';
import 'package:dataAlumni/model/models.dart';
import 'package:dataAlumni/util/Images.dart';
import 'package:dataAlumni/util/model/Social_model.dart';

//agenda
List<T2Favourite> getFavourites() {
  List<T2Favourite> list = List<T2Favourite>();
  T2Favourite model1 = T2Favourite();
  model1.name = "Best Jogging tips in the world";
  model1.duration = "5 min ago";
  model1.image = userIcon;

  T2Favourite model2 = T2Favourite();
  model2.name = "Best Yoga guide for better Health in the world";
  model2.duration = "15 min ago";
  model2.image = userIcon;

  T2Favourite model3 = T2Favourite();
  model3.name = "Best Exercise tips in the world";
  model3.duration = "an hour ago";
  model3.image = userIcon;

  T2Favourite model4 = T2Favourite();
  model4.name = "Best Diet tips for the good Health";
  model4.duration = "5 hour ago";
  model4.image = userIcon;

  T2Favourite model5 = T2Favourite();
  model5.name = "Healty food tips in the world";
  model5.duration = "7 hour ago";
  model5.image = userIcon;

  list.add(model1);
  list.add(model2);
  list.add(model3);
  list.add(model4);
  list.add(model5);
  list.add(model1);
  list.add(model2);
  list.add(model3);
  list.add(model4);
  list.add(model5);
  list.add(model1);
  list.add(model2);
  list.add(model3);
  list.add(model4);
  list.add(model5);
  list.add(model1);
  list.add(model2);
  list.add(model3);
  list.add(model4);
  list.add(model5);
  return list;
}

//enda agenda

// alumni
List<T10Product> getProducts() {
  List<T10Product> list = List<T10Product>();
  list.add(T10Product(
      "Nike Shoes", "\$250", "\$350", userIcon, "By Boots Category"));
  list.add(T10Product(
      "Headset", "\$50", "\$100", userIcon, "By Headset Category"));
  list.add(T10Product("Pot", "\$20", "\$30", userIcon, "By Pots Category"));
  list.add(
      T10Product("Watch", "\$250", "\$350", userIcon, "By Watch Category"));
      list.add(
      T10Product("Watch", "\$250", "\$350", userIcon, "By Watch Category"));
  return list;
}

//end alumni
List<SocialUser> addStatusData() {
List<SocialUser> mList = [
  SocialUser(
    name: "Logan Nesser",
    duration: "7.30 PM",
    info: "30 minutes ago",
    image: social_ic_user4,
  ),
  SocialUser(
    name: "Logan Nesser",
    duration: "5.30 PM",
    info: "Today,8:30 PM",
    image: social_ic_user2,
  ),
  SocialUser(
    name: "Logan Nesser",
    duration: "2.30 PM",
    info: "Today,8:30 PM",
    image: social_ic_user3,
  ),
  SocialUser(
    name: "Logan Nesser",
    duration: "7.30 PM",
    info: "30 minutes ago",
    image: social_ic_user4,
  ),
  SocialUser(
    name: "Logan Nesser",
    duration: "5.30 PM",
    info: "Today,8:30 PM",
    image: social_ic_user2,
  ),
  SocialUser(
    name: "Logan Nesser",
    duration: "2.30 PM",
    info: "Today,8:30 PM",
    image: social_ic_user3,
  ),
];
return mList;
}

// end komen
