import 'package:flutter/material.dart';
import 'package:dataAlumni/util/Constant.dart';
import 'package:dataAlumni/util/Colors.dart';

BoxDecoration boxDecorationKomen({double radius = spacing_middle, Color color = Colors.transparent, Color bgColor = t5White, var showShadow = false}) {
  return BoxDecoration(
    color: bgColor,
    boxShadow: showShadow ? [BoxShadow(color: t10_ShadowColor, blurRadius: 10, spreadRadius: 2)] : [BoxShadow(color: Colors.transparent)],
    border: Border.all(color: color),
    borderRadius: BorderRadius.all(Radius.circular(radius)),
  );
}