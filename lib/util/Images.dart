import 'package:dataAlumni/util/Constant.dart';

const food_ic_login = "images/login/food_ic_login.jpg";
const food_ic_google_fill = "images/login/food_ic_google_fill.svg";
const food_ic_fb = "images/login/food_ic_fb.svg";



// bottom navigasi
const t3_ic_home = "images/bottomNavigasi/t3_ic_home.svg";
const t3_ic_msg = "images/bottomNavigasi/t3_ic_msg.svg";
const t3_notification = "images/bottomNavigasi/t3_notification.svg";
const t3_ic_user = "images/bottomNavigasi/t3_ic_user.svg";
const t3_ic_profile = "$BaseUrl/images/bottomNavigasi/t3_ic_profile.jpg";
// end Bottom navigasi

// agenda
const t2_img3 = "$BaseUrl/images/theme2/t2_img3.png";
const t2_img4 = "$BaseUrl/images/theme2/t2_img4.png";
const t2_ic_img1 = "$BaseUrl/images/theme2/theme2_ic_img1.png";
const t2_ic_img2 = "$BaseUrl/images/theme2/theme2_ic_img2.png";

//end agenda

// alumni
const Theme10Pot = "$BaseUrl/images/theme10/t10_pot.jpg";
const t10_headphones = "$BaseUrl/images/theme10/t10_headphones.jpeg";
const t10_ic_shoes = "$BaseUrl/images/theme10/t10_ic_shoes.jpeg";
const t10_watch = "$BaseUrl/images/theme10/t10_watch.jpg";
const t10_profile = "$BaseUrl/images/theme10/t10_profile.jpg";
const t10_profile_1 = "$BaseUrl/images/theme10/t10_profile_1.jpg";
const t10_profile_3 = "$BaseUrl/images/theme10/t10_profile_3.jpg";
const t10_profile_2 = "$BaseUrl/images/theme10/t10_profile_2.jpg";
const t10_slider_1 = "$BaseUrl/images/theme10/t10_slider_1.jpeg";
const t10_profile_5 = "$BaseUrl/images/theme10/t10_profile_5.jpg";
const t10_profile_6 = "$BaseUrl/images/theme10/t10_profile_6.jpg";
const t10_slider_2 = "$BaseUrl/images/theme10/t10_slider_2.jpg";
const t10_slider_3 = "$BaseUrl/images/theme10/t10_slider_3.jpeg";
const t10_shoes = "$BaseUrl/images/theme10/t10_shoes.jpeg";

const t10_ic_otp = "images/theme10/t10_ic_otp.svg";
const t10_ic_search = "images/theme10/t10_ic_search.svg";
//end alumni


//profil
const t5_wallet = "$BaseUrl/images/theme5/t5_wallet.svg";
const t5_analysis = "$BaseUrl/images/theme5/t5_analysis.svg";
const t5_customer_service = "$BaseUrl/images/theme5/t5_customer_service.svg";
const t5_img_settings = "$BaseUrl/images/theme5/t5_settings.svg";
const t5_wallet_2 = "$BaseUrl/images/theme5/t5_wallet_2.svg";
const t5_options = "$BaseUrl/images/theme5/t5_options.svg";
const t5_profile_8 = "$BaseUrl/images/theme5/t5_profile_8.jpeg";
// endprofil

//komen
const social_ic_user4 = "$BaseUrl/images/social/social_ic_user4.png";
const social_ic_user2 = "$BaseUrl/images/social/social_ic_user2.png";
const social_ic_user3 = "$BaseUrl/images/social/social_ic_user3.png";

// end komen

// bottom navigasi menu
const t1_ic_home_image = "images/theme1/t1_ic_home_image.png";
const t1_ic_ring = "images/theme1/t1_ic_ring.png";
const t1_ic_user1 = "$BaseUrl/images/theme1/t1_ic_user1.png";
const t1_ic_user2 = "$BaseUrl/images/theme1/t1_ic_user2.png";
const t1_ic_user3 = "$BaseUrl/images/theme1/t1_ic_user3.png";
const t1_search = "images/theme1/t1_search.svg";
const t1_walk1 = "$BaseUrl/images/theme1/t1_walk1.png";
const t1_walk2 = "$BaseUrl/images/theme1/t1_walk2.png";
const t1_walk3 = "$BaseUrl/images/theme1/t1_walk3.png";
const t1_walk_bottom = "images/theme1/t1_walk_bottom.png";
const t1_walk_top = "images/theme1/t1_walk_top.png";
const t1_menu = "images/theme1/t1_menu.svg";
const t1_ic_bottom_sheet = "images/theme1/t1_ic_bottom_sheet.png";
const t1_ic_dialog = "images/theme1/t1_ic_dialog.png";
const t1_ic_step1 = "images/theme1/t1_ic_step1.png";
const t1_ic_step3 = "images/theme1/t1_ic_step3.png";
const t1_ic_walk2 = "images/theme1/t1_ic_walk2.png";
const t1_ic_walk3 = "images/theme1/t1_ic_walk3.png";
const t1_slider1 = "$BaseUrl/images/theme1/t1_slider1.png";
const t1_slider2 = "$BaseUrl/images/theme1/t1_slider2.png";
const t1_slider3 = "$BaseUrl/images/theme1/t1_slider3.png";
const t1_file = "images/theme1/t1_file.svg";
const t1_images = "images/theme1/t1_images.svg";
const t1_video = "images/theme1/t1_video.svg";
const t1_user = "images/theme1/t1_user.svg";
const t1_notification = "images/theme1/t1_notification.svg";
const t1_settings = "images/theme1/t1_settings.svg";
const t1_home = "images/theme1/t1_home.svg";
const t1_twitter = "images/theme1/t1_twitter.png";
const t1_facebook = "images/theme1/t1_facebook.png";
const t1_instagram = "images/theme1/t1_instagram.png";
const t1_linkedin = "images/theme1/t1_linkedin.png";
const t1_whatsup = "images/theme1/t1_whatsapp.png";
// bottom menu

// profil
const t4_bg = "$BaseUrl/images/theme4/t4_bg.jpeg";
const t4_ic_img1 = "$BaseUrl/images/theme4/t4_ic_img1.jpg";
const t4_ic_img2 = "$BaseUrl/images/theme4/t4_ic_img2.jpg";
const t4_ic_img3 = "$BaseUrl/images/theme4/t4_ic_img3.jpg";
const t4_ic_img4 = "$BaseUrl/images/theme4/t4_ic_img4.jpg";
const t4_ic_img5 = "$BaseUrl/images/theme4/t4_ic_img5.jpg";
const t4_ic_item1 = "$BaseUrl/images/theme4/t4_ic_item1.png";
const t4_ic_item2 = "$BaseUrl/images/theme4/t4_ic_item2.png";
const t4_ic_item3 = "$BaseUrl/images/theme4/t4_ic_item3.png";
const t4_ic_item4 = "$BaseUrl/images/theme4/t4_ic_item4.png";
const t4_ic_item5 = "$BaseUrl/images/theme4/t4_ic_item5.jpg";
const t4_ic_item6 = "$BaseUrl/images/theme4/t4_ic_item6.png";
const t4_ic_item7 = "$BaseUrl/images/theme4/t4_ic_item7.jpg";
const t4_ic_item8 = "$BaseUrl/images/theme4/t4_ic_item8.jpg";
const t4_ic_profile = "$BaseUrl/images/theme4/t4_ic_profile.png";
const t4_ic_profile1 = "$BaseUrl/images/theme4/t4_ic_profile1.jpg";
const t4_icon = "$BaseUrl/images/theme4/t4_icon.png";
const t4_img1 = "$BaseUrl/images/theme4/t4_img1.jpeg";
const t4_img2 = "$BaseUrl/images/theme4/t4_img2.jpeg";
const t4_img4 = "$BaseUrl/images/theme4/t4_img4.jpeg";
const t4_img5 = "$BaseUrl/images/theme4/t4_img5.jpeg";
const t4_img6 = "$BaseUrl/images/theme4/t4_img6.jpeg";
const t4_img8 = "$BaseUrl/images/theme4/t4_img8.jpeg";
const t4_img9 = "$BaseUrl/images/theme4/t4_img9.jpeg";
const t4_img10 = "$BaseUrl/images/theme4/t4_img10.jpeg";
const t4_img_3 = "$BaseUrl/images/theme4/t4_img_3.jpeg";
const t4_iv_img6 = "$BaseUrl/images/theme4/t4_iv_img6.jpg";
const t4_logo = "images/theme4/t4_logo.png";
const t4_profile = "$BaseUrl/images/theme4/t4_profile.png";
const t4_profile_covr_page = "$BaseUrl/images/theme4/t4_profile_covr_page.png";
const t4_walk1 = "$BaseUrl/images/theme4/t4_walk1.png";
const t4_walk2 = "$BaseUrl/images/theme4/t4_walk2.png";
const t4_walk3 = "$BaseUrl/images/theme4/t4_walk3.png";
const t4_walk_bg = "images/theme4/t4_walk_bg.png";
const t4_fb = "images/theme4/t4_fb.svg";
const t4_google = "images/theme4/t4_google.svg";
const t4_bell = "images/theme4/t4_bell.svg";
const t4_file = "images/theme4/t4_file.svg";
const t4_heart = "images/theme4/t4_heart.svg";
const t4_help = "images/theme4/t4_help.svg";
const t4_home = "images/theme4/t4_home.svg";
const t4_logout = "images/theme4/t4_logout.svg";
const t4_playbutton = "images/theme4/t4_playbutton.svg";
const t4_user = "images/theme4/t4_user.svg";
const t4_share = "images/theme4/t4_share.svg";
// end profile

// untuk umum
const userIcon = "images/dataAlumni/unnamed.png";
const iconInstansi = "images/dataAlumni/instansi.png";
// end untuk umum