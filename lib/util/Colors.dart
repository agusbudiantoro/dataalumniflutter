import 'package:flutter/material.dart';
import 'dart:ui';

const opTextPrimaryColor = Color(0xFF000000);
const opTextSecondaryColor = Color(0xFF757575);

const opPrimaryColor = Color(0xFF343EDB);
const opSecondaryColor = Color(0xFF757575);
const opOrangeColor = Color(0xFFFF6E18);
const opBackgroundColor = Color(0xFFFFFFFF);

const food_colorPrimary = Color(0xFF3B8BEA);
const food_colorBlueAccent = Colors.blueAccent;
const food_colorPrimaryDark = Color(0xFF3B8BEA);
const food_colorAccent = Color(0xFF4ec77f);
const food_colorAccent_light = Color(0xFF4ec77f);
const food_textColorPrimary = Color(0xFF333333);
const food_textColorSecondary = Color(0xFF949292);
const food_app_background = Color(0xFFf8f8f8);
const food_view_color = Color(0xFFDADADA);
const food_white = Color(0xFFffffff);
const food_black = Color(0xFF000000);
const food_icon_color = Color(0xFF747474);
const food_form_background = Color(0xFFF6F7F9);
const food_color_Orange = Color(0xFFFF5722);
const food_color_red = Color(0xFFC00404);
const food_color_gray = Color(0xFFFAFAFA);
const food_color_green = Color(0xFF4CAF50);
const food_color_light_orange = Color(0xFFFBFAF0);
const food_color_light_primary = Color(0xFFF6F7FB);
const food_color_yellow = Color(0xFFFFC107);
const food_color_blue_gradient1 = Color(0xFF398CE8);
const food_color_blue_gradient2 = Color(0xFF3F77DE);
const food_color_orange_gradient1 = Color(0xFFFD825D);
const food_color_orange_gradient2 = Color(0xFFFE5052);
const food_color_yellow_gradient1 = Color(0xFFFF895F);
const food_color_yellow_gradient2 = Color(0xFFFE9C3E);
const food_color_green_gradient1 = Color(0xFF02BBC6);
const food_color_green_gradient2 = Color(0xFF84DB74);
const food_colorPrimary_light = Color(0xFFE8E8EC);
const food_ShadowColor = Color(0X95E9EBF0);
const food_ShadowColors = Color(0XFFE2E2E2);
const food_orange_light = Color(0xFFFDF8F2);

// warna Bottom Navigation
const t3_colorPrimary = Color(0XFFfc4a1a);
const t3_colorPrimaryDark = Color(0XFFf7b733);
const t3_colorAccent = Color(0XFFf7b733);

const t3_textColorPrimary = Color(0XFF333333);
const t3_textColorSecondary = Color(0XFF9D9D9D);

const t3_app_background = Color(0XFFf8f8f8);
const t3_view_color = Color(0XFFDADADA);
const t3_gray = Color(0XFFF4F4F4);

const t3_red = Color(0XFFF12727);
const t3_green = Color(0XFF8BC34A);
const t3_edit_background = Color(0XFFF5F4F4);
const t3_light_gray = Color(0XFFCECACA);
const t3_tint = Color(0XFFF4704C);
const t3_colorPrimary_light = Color(0XFFF36F4B);
const t3_orange = Color(0XFFF13E0A);

const t3_white = Color(0XFFffffff);
const t3_black = Color(0XFF000000);
const t3_icon_color = Color(0XFF747474);

const t3_shadow = Color(0X70E2E2E5);
var t3White = materialColor(0XFFFFFFFF);

const t2_colorPrimary = Color(0XFF5959fc);
const t2_colorPrimaryDark = Color(0XFF7900F5);
const t2_colorPrimaryLight = Color(0XFFF2ECFD);
const t2_colorAccent = Color(0XFF7e05fa);
const t2_textColorPrimary = Color(0XFF212121);
const t2_textColorSecondary = Color(0XFF747474);
const t2_app_background = Color(0XFFf8f8f8);
const t2_view_color = Color(0XFFDADADA);
const t2_white = Color(0XFFFFFFFF);
const t2_icon_color = Color(0XFF747474);
const t2_blue = Color(0XFF1C38D3);
const t2_orange = Color(0XFFFF5722);
const t2_background_bottom_navigation = Color(0XFFE9E7FE);
const t2_background_selected = Color(0XFFF3EDFE);
const t2_green = Color(0XFF5CD551);
const t2_red = Color(0XFFFD4D4B);
const t2_card_background = Color(0XFFFaFaFa);
const t2_bg_bottom_sheet = Color(0XFFE8E6FD);
const t2_instagram_pink = Color(0XFFCC2F97);
const t2_linkedin_pink = Color(0XFF0c78b6);
var t2lightStatusBar = materialColor(0XFFEAEAF9);
var t2White = materialColor(0XFFFFFFFF);
var t2TextColorPrimary = materialColor(0XFF212121);

const t10_colorPrimary = Color(0xFF554BDF);
const t10_colorPrimaryDark = Color(0xFF554BDF);
const t10_colorAccent = Color(0xFF554BDF);
const t10_colorPrimary_light = Color(0xFF3E3A5BFB);
const t10_textColorPrimary = Color(0xFF130925);
const t10_textColorSecondary = Color(0xFF888888);
const t10_textColorSecondary_blue = Color(0xFF86859B);
const t10_textColorThird = Color(0xFFBABFB6);
const t10_textColorGrey = Color(0xFFB4BBC2);
const t10_white = Color(0xFFFFFFFF);

const t10_layout_background_white = Color(0xFFF6F7FA);
const t10_view_color = Color(0xFFB4BBC2);
const t10_blue_color = Color(0xFF3A5BFB);
const t10_gradient1 = Color(0xFF3a5af9);
const t10_app_background = Color(0XFFf8f8f8);
const t10_gradient2 = Color(0xFF7449fa);

const t10_ShadowColor = Color(0X95E9EBF0);

const t5ColorPrimary = Color(0XFF5104D7);
const t5ColorPrimaryDark = Color(0XFF325BF0);
const t5ColorAccent = Color(0XFFD81B60);
const t5TextColorPrimary = Color(0XFF130925);
const t5TextColorSecondary = Color(0XFF888888);
const t5TextColorThird = Color(0XFFBABFB6);
const t5TextColorGrey = Color(0XFFB4BBC2);
const t5White = Color(0XFFFFFFFF);
const t5LayoutBackgroundWhite = Color(0XFFF6F7FA);
const t5ViewColor = Color(0XFFB4BBC2);
const t5SkyBlue = Color(0XFF1fc9cd);
const t5DarkNavy = Color(0XFF130925);
const t5Cat1 = Color(0XFF45c7db);
const t5Cat2 = Color(0XFF510AD7);
const t5Cat3 = Color(0XFFe43649);
const t5Cat4 = Color(0XFFf4b428);
const t5Cat5 = Color(0XFF22ce9a);
const t5Cat6 = Color(0XFF203afb);
const t5ShadowColor = Color(0X95E9EBF0);
const t5DarkRed = Color(0XFFF06263);
const t5ColorPrimaryLight = Color(0X505104D7);
const social_colorPrimary = Color(0xFF494FFB);

const shadow_color = Color(0X95E9EBF0);

const t1_colorPrimary = Color(0XFFff8080);
const t1_colorPrimary_light = Color(0XFFFFEEEE);
const t1_colorPrimaryDark = Color(0XFFff8080);
const t1_colorAccent = Color(0XFFff8080);

const t1_textColorPrimary = Color(0XFF333333);
const t1_textColorSecondary = Color(0XFF747474);

const t1_app_background = Color(0XFFf8f8f8);
const t1_view_color = Color(0XFFDADADA);

const t1_sign_in_background = Color(0XFFDADADA);

const t1_white = Color(0XFFffffff);
const t1_icon_color = Color(0XFF747474);
const t1_selected_tab = Color(0XFFFCE9E9);
const t1_red = Color(0XFFF10202);
const t1_blue = Color(0XFF1D36C0);
const t1_edit_text_background = Color(0XFFE8E8E8);
const t1_shadow = Color(0X70E2E2E5);
var t1White = materialColor(0XFFFFFFFF);
var t1TextColorPrimary = materialColor(0XFF212121);
const t1_color_primary_light = Color(0XFFFCE8E8);
const t1_bg_bottom_sheet = Color(0XFFFFF1F1);

const t4_colorPrimary = Color(0XFF4600D9);
const t4_colorPrimaryDark = Color(0XFF4600D9);
const t4_colorAccent = Color(0XFF4600D9);

const t4_textColorPrimary = Color(0XFF333333);
const t4_textColorSecondary = Color(0XFF9D9D9D);

const t4_app_background = Color(0XFFf8f8f8);
const t4_view_color = Color(0XFFDADADA);

const t4_white = Color(0XFFffffff);
const t4_black = Color(0XFF000000);

const t4_icon_color = Color(0XFF747474);
const t4_form_background = Color(0XFFF6F7F9);
const t4_form_facebook = Color(0XFF2F3181);
const t4_form_google = Color(0XFFF13B19);
const t4_green = Color(0XFF0DAF14);
const t4_light = Color(0XFF23DFD5);
const t4_walk = Color(0XFFEDE5FC);

const t4_cat1 = Color(0XFFFF727B);
const t4_cat2 = Color(0XFF439AF8);
const t4_cat3 = Color(0XFF72D4A1);
const t4_cat4 = Color(0XFFFFC358);
const t4_cat5 = Color(0XFFA89DF6);
const t4_shadow_color = Color(0X95E9EBF0);

Map<int, Color> color = {
  50: Color.fromRGBO(136, 14, 79, .1),
  100: Color.fromRGBO(136, 14, 79, .2),
  200: Color.fromRGBO(136, 14, 79, .3),
  300: Color.fromRGBO(136, 14, 79, .4),
  400: Color.fromRGBO(136, 14, 79, .5),
  500: Color.fromRGBO(136, 14, 79, .6),
  600: Color.fromRGBO(136, 14, 79, .7),
  700: Color.fromRGBO(136, 14, 79, .8),
  800: Color.fromRGBO(136, 14, 79, .9),
  900: Color.fromRGBO(136, 14, 79, 1),
};

MaterialColor materialColor(colorHax) {
  return MaterialColor(colorHax, color);
}

MaterialColor colorCustom = MaterialColor(0XFF5959fc, color);
// end warna bottom navigation