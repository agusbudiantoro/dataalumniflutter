// class SocialUser {
//   var name;
//   var info;
//   var duration;
//   var call;
//   var image;
// }
class SocialUser {
  String name;
  String info;
  String duration;
  String call;
  String image;

  SocialUser({this.name, this.info, this.duration, this.call, this.image});
}

class Media {
  var image;

  Media(this.image);
}

class Qr {
  var code;

  Qr(this.code);
}

class Chat {
  var msg;
  var isSender;
  var type;
  var duration;
}
