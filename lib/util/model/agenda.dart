class InfoAgenda {
    List <ModelAgenda> list = [
      ModelAgenda(
        name: "Jadwal",
        gbr: "jadwal"
      ),
      ModelAgenda(
        name: "Pengumuman",
        gbr: "pengumuman"
      ),
    ];
}

class ModelAgenda {
  // String profileImg;
  String name;
  String gbr;
  // String feedImage;

  ModelAgenda({this.name, this.gbr});
}