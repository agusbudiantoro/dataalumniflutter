import 'package:flutter/material.dart';

class InforCard {
    List <ModelInfo> list = [
      ModelInfo(
        name: "Pendidikan"
      ),
      ModelInfo(
        name: "Karir"
      ),
      ModelInfo(
        name: "Organisasi"
      ),
    ];
}

class ModelInfo {
  // String profileImg;
  String name;
  // String feedImage;

  ModelInfo({this.name});
}

class T1Model {
  String name;
  String duration;
  String info;
  String designation;
  String img;

  T1Model(this.name, this.duration, this.info, this.designation, this.img);
}
