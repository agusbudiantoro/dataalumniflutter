// import 'dart:async';
// import 'dart:convert';

// import 'package:dataAlumni/Pages/home.dart';
// import 'package:dataAlumni/bloc/bloc_auth/auth_bloc.dart';
// import 'package:dataAlumni/model/cek_token.dart';
// import 'package:dataAlumni/network/api_provider.dart';
// import 'package:flutter/animation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';

// import 'package:dataAlumni/util/Style.dart';

// import 'package:dataAlumni/StepScreen/Step.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

// class OPSplashScreen extends StatefulWidget {
//   static String tag = '/OPSplashScreen';

//   @override
//   _OPSplashScreenState createState() => _OPSplashScreenState();
// }

// class _OPSplashScreenState extends State<OPSplashScreen>
//     with SingleTickerProviderStateMixin {
//       final _bloc = AuthBloc();
//   String tokens;
//   bool loginStatus;
//   // startTime() async {
//   //   var _duration = Duration(seconds: 2);
//   //   return Timer(_duration, page);
//   // }

//   @override
//   void initState() {
//     super.initState();
//     // startTime();
//   }

//   void navigationPage() {
//     print(loginStatus);
//     Navigator.pop(context);
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//         builder: (context) => OPWalkThroughScreen(),
//       ),
//     );
//   }


//   page(AuthBloc bloc) async {
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     tokens = preferences.getString("token");
//     CekToken loginRequestData =
//         CekToken(token: tokens.toString());
//         print(loginRequestData.token);
//         print("initoken");
//     bloc.add(CekTokenEvent(data: loginRequestData));
//   }

//   Widget build(BuildContext context) {
//     final _bloc = AuthBloc();
//     ApiProvider _apiProvider = ApiProvider();
//     final double width = MediaQuery.of(context).size.width;

//     return Scaffold(
//       body: Container(
//         decoration: BoxDecoration(
//           gradient: LinearGradient(
//             begin: Alignment.topCenter,
//             end: Alignment.bottomCenter,
//             colors: [
//               Color(0xFF73AEF5),
//               Color(0xFF61A4F1),
//               Color(0xFF478DE0),
//               Color(0xFF398AE5),
//             ],
//             stops: [0.1, 0.4, 0.7, 0.9],
//           ),
//         ),
//         width: MediaQuery.of(context).size.width,
//         child: Center(
//           child: Stack(
//             children: [
//               Container(
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 mainAxisSize: MainAxisSize.max,
//                 children: <Widget>[
//                   Image.asset('images/dataAlumni/logo1.png',
//                       width: 120, height: 75, fit: BoxFit.fill),
//                   SizedBox(height: 10),
//                   Text("Data Alumni", style: boldTextStyle(size: 20)),
//                   BlocListener<AuthBloc, AuthState>(
//                     bloc: _bloc,
//                     listener: (context, state) {
//                       if (state is CekTokenError) {
//                         Fluttertoast.showToast(msg: state.errorMessage);
//                         Fluttertoast.showToast(msg: "Login Gagal");
//                         Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => OPWalkThroughScreen()));
//                       }
//                       if (state is CekTokenSukses) {
//                         Fluttertoast.showToast(msg: "Success Login");
//                         Navigator.push(context,
//                             MaterialPageRoute(builder: (context) => Home()));
//                       }
//                       if (state is CekTokenWaiting) {
//                         Fluttertoast.showToast(msg: "Login Waiting");
//                       }
//                     },
//                       child: BlocBuilder<AuthBloc, AuthState>(
//                           builder: (context, state) {
//                             if (state is CekTokenWaiting) {
//                               return CircularProgressIndicator(
//                                 backgroundColor: Colors.white,
//                               );
//                             }
//                             else if (state is CekTokenError) {
//                               return page(_bloc);
//                             }
//                             else if (state is CekTokenSukses) {
//                               return page(_bloc);
//                             }
//                             return page(_bloc);
//                           },
//                         ),
//                     ),
//                 ],
//               ),
//             ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   RaisedButton _buttonLogin(AuthBloc bloc) {
//     return RaisedButton(
//       onPressed: () {
//         CekToken loginRequestData = CekToken(token: tokens.toString());
//     bloc.add(CekTokenEvent(
//       data: loginRequestData,
//     ));
//       },
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(80.0),
//       ),
//       padding: const EdgeInsets.all(0.0),
//       child: Ink(
//         decoration: const BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.all(
//             Radius.circular(80.0),
//           ),
//         ),
//         child: Container(
//           constraints: const BoxConstraints(minWidth: 150.0, minHeight: 36.0),
//           alignment: Alignment.center,
//           child: const Text(
//             'Masuk',
//             textAlign: TextAlign.center,
//             style: TextStyle(
//               color: Colors.black,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
