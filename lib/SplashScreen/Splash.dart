import 'dart:async';
import 'dart:convert';

import 'package:dataAlumni/Pages/home.dart';
import 'package:dataAlumni/Pages/login.dart';
import 'package:dataAlumni/bloc/bloc_auth/auth_bloc.dart';
import 'package:dataAlumni/model/cek_token.dart';
import 'package:dataAlumni/network/api_provider.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:dataAlumni/util/Style.dart';

import 'package:dataAlumni/StepScreen/Step.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OPSplashScreen extends StatefulWidget {
  final bool tandaUpdate;
  static String tag = '/OPSplashScreen';

  OPSplashScreen({this.tandaUpdate});
  @override
  _OPSplashScreenState createState() => _OPSplashScreenState();
}

class _OPSplashScreenState extends State<OPSplashScreen>
    with SingleTickerProviderStateMixin {
  String kode;
  String tokens;
  bool loginStatus;
  bool statusTanda;
  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, statuslogin);
  }

  AuthBloc _bloc = AuthBloc();

  @override
  void initState() {
    super.initState();
    // startTime();
    // statuslogin().then((valu){
    //   print(kode);
    //   print("kode");
    // });
    print("sini tanda");
    print(widget.tandaUpdate);
    getTanda();
    _bloc.add(CekTokenEvent());
  }

  void getTanda() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      statusTanda = prefs.getBool("tanda");
    });
    print("sini woyy");
    print(statusTanda);
  }

  void navigationPage() {
    print(loginStatus);
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => OPWalkThroughScreen(),
      ),
    );
  }

  Future<String> statuslogin() async {
    // ApiProvider _apiProvider = ApiProvider();

    // final preferences = await SharedPreferences.getInstance();
    // tokens = preferences.getString("token");
    // setState(() {
    //   kode = tokens;
    // });
    // print(tokens);
    // if (tokens != null) {
    //   ApiProvider.cekToken(tokens).then((res) {
    //     var isi = res.body;
    //     print(isi);
    //     print("tes");

    //     if (isi == "admin email@gmail.com") {
    //       setState(() {
    //         Navigator.pop(context);
    //         Navigator.push(
    //           context,
    //           MaterialPageRoute(
    //             builder: (context) => Home(),
    //           ),
    //         );
    //       });
    //     } else {
    //       setState(() {
    //         Navigator.pop(context);
    //         Navigator.push(
    //           context,
    //           MaterialPageRoute(
    //             builder: (context) => OPWalkThroughScreen(),
    //           ),
    //         );
    //       });
    //     }
    //     print("status");
    //     print(loginStatus);
    //   });
    // } else {
    //   setState(() {
    //     Navigator.pop(context);
    //     Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //         builder: (context) => OPWalkThroughScreen(),
    //       ),
    //     );
    //   });
    // }
  }

  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: BlocListener<AuthBloc, AuthState>(
      bloc: _bloc,
      listener: (context, state) {
        if (state is CekTokenSukses) {
          // Navigator.pop(context);
          Navigator.of(context).pop(true);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Home(),
            ),
          );
        }
        if (state is CekTokenError) {
          // Navigator.pop(context);
          if (statusTanda == true) {
            Navigator.of(context).pop(true);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FoodSignIn(tanda: true),
              ),
            );
          } else {
            Navigator.of(context).pop(true);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => OPWalkThroughScreen(),
              ),
            );
          }
        }
      },
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Image.asset('images/dataAlumni/logo1.png',
                    width: 120, height: 75, fit: BoxFit.fill),
                SizedBox(height: 10),
                Text("SILANRI",
                    style: boldTextStyle(size: 20, textColor: Colors.white)),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
